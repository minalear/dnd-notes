---
title: "Ideas"
---

## New Campaign Ideas
* Wasteland/Fallout/EYE - Earth was mostly destroyed during World War III with very few nations surviving.  Various nations reestablish themselves and form a large Federation trying to establish a new world order while other groups resist.  Federation employs various devious means to establish control and to conquer the new post-apocalyptic world while others try to survive.
* 


* Call of Cthulhu - players are compelled to investigate a dark cult and figure foil their plans.
* War for Succession - players play the guards to a prince seeking to claim the throne in a death game set by their father.  (HxH)
* Metro
* Dark Souls concept - players play undead seeking a cure for their curse in a foreign land and are being hunted by a holy order of paladins.
  * Zwhaihander as the rules system
  
---
title: "Ideas"
draft: false
---

## World Ideas
* The world is ancient and dying.
    * Apocalypse - the four horsemen ride the land causing the end of the world.  Archaon style apocalpyse like Warhammer. 
    * Cycle - the gods are about to end this version of our world and begin a new one, a never ending cycle since the dawn of time.
        * This can be a dark souls style cycle with light and dark or like Wandersong with the song of creation being a pillar theme.
* High fantasy
    * I want a game where all of the D&D high fantasy concepts are prevalent.  Magic is pretty common and fantastical creatures/races aren't unknown.
* Cyberpunk + Fantasy game
    * Players play real life people in the "real world" but also participate in an online mmo during their freetime.
    * Inevitables are AI employed by the admins to keep the "Integrity" of the game in check.
    * Integrity can be the devolution of the game due to chaos and hackers and can bleed into people's real lives via their implants.

## Campaign Ideas
* A meteor of sorts has landed at the center of a region.  Rumors of bizarre effects caused by it and the idea of treasure cause a race of adventurers to flood the area to seek the fallen star.

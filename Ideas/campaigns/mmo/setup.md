---
title: "Setup"
draft: false
---

## World
The year is 2142 and the world is very bleak.  The sky is permanently black due to a last ditch effort to save the planet from global warming by the Gates Foundation.  Trillions of nanomachines launched into the upper atmosphere that absorb the radiation from the sun's rays and filters it through to the Earth, keeping the planet warm but entirely dark.

The natural world is all but dead with most species of plants and animals becoming extinct.  Humanity survives through sheer will power and ignorance of the disaster that they caused and continue hurdling forward into the future.

Many of the world's richest cities live under domes of artificial light that recreate "the world that was."  Only the most affluent people can afford to live under the dome, leaving the majority of society to live outside of it, pale and afraid.

The city of New Detroit is home to some of the world's hub for technology, being the home of many of the biggest corporations in VR and AR.

## Game
The latest MMO from the game company Inferno is the most anticipated game in ages; ForeverQuest.  Set in the Land of Zygor, three factions fight one another to claim dominion over the world.  But there are also forces at play threatening the peace of Zygor...

The Forerunners are an ancient race of advanced beings that have disappeared eons ago.  Now in the modern ages, their relics are scattered throughout the land and their cities are all but memories.  Not much is known about them but it is known that they were able to unite technology and magic into a single entity allowing for advanced usage.  The combination of Magic and Technology is refered to as Magitech.

### Major Factions
#### Emerald Enclave
* Group that opposes threats to the natural world and helps others sruvive the many perils of the wild.
#### The Vigil
* Alliance of free cities that band together for mutual aid and protection.
#### Marauders

### Minor Factions
#### Holy Order
* Order of paladins and clerics dedicated to the prosthelytizing the glory of the God-King.
#### Syndicate
* Group dedicated to the acquisition of money at all costs through legitimate and illegitimate businesses.
#### Ebon Claw
* Dragon worshipers

### Vilains
* Witch King
* 


#### The Marauders (Barbarians/Warriors)
* A chaotic group of warriors that follow a single warchief into battle.
#### The Syndicate (Traders/Artisans)
* A series of guilds securing territory for maximum profiteering.
#### The Azure Order (Paladins/Knights)
* A noble order of knights that protect and serve, usually heavy handed.
#### The Church (Clerics/Priests)
#### The 

### Map
#### Zones
1. Tundra
1. Wilderness (PvP Area)
1. 


## Ideas
* Androids are integrated into society and treated equally as citizens.  Due to legal battles in the early 2080s, they are created up to a specification to be "near-human" as to not displace humanity in society.  They must eat, sleep, and feel fatigue.  Their are fully self aware and feel/learn/love like humans do.
    * They are basically another form of human life despite being artificial in creation.
* AI in video games and other forms are created up to the legal definition of "self-aware" that give machines rights.  AI in games are hyper realistic, but are not actually self aware and cannot comprehend the "real world."
* The androids help supplement the population of humanity which has drastically fell since Operation Dark Sky.  Humanity now suffers from extreme dropping in fertility world wide.
* A lot of humanity turns towards the burgeoning VR market to escape the desitute of the real world.

* Tempted to make the "real world" a fantasy world with higher beings performing experiments on them, much like Dark City.  The players use VR to unravel the threads of their own reality in real time.
---
title: "Space Combat"
draft: false
---

Space Combat

AC (Armor Class) - how hard it is to hit this target
Init (Initiative) - bonus to the ship to determine the order of combat


Roles
	Helm					- Controls the movement of the ship
	Navigator				- Plots course for navigation; references star charts
	Sensor Operations		- Controls sensor arrays to scan objects; can detect weakness
	Internal Systems		- Power management; Transporting; Deploys repair drones
	Security Oversight		- Deploy combat drones; Deploy security; Internal sensors; Internal containment/forcefields
	Tactical				- Raise/Lower shields; Fire weapons; Tractor beam
	Communications			- Manage communications; Signal interception/jamming
	Sickbay					- Managing wounds of individuals


Notes
	Torpedos typically deal more damage, but can be shot as a reaction by other ships.

Ship Stats
The Noble [Nexus: Express]
	AC			10
	Init 		+1
	Shields 	10
	Recharge	 1
	Hull		20
	Speed		 4
	
	Mk1 Phaser	+2 1d6
	2xTorpedo	+1 1d10
	
The Tirion [Matador: Richmond] (Elf Freighter)
	AC			10
	Init		-1
	Shields		10
	Recharge	 1
	Hull		15
	Speed		 4
	
	E.Phaser	+1 1d6
	Tractor Beam
	
	
Viper [Outerra: Viper]
	AC			14
	Init		+3
	Shields		10
	Recharge	 1
	Hull		10
	Speed		 6
	
	Mk1 Phaser	+2 1d6
	

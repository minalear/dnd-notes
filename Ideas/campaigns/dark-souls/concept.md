---
title: "Concept"
draft: false
---

The world has suffered many a cataclysm, from floods to demon invasions, but the latest incursion may be the last.  The curse of the undead has arrived to the Land of Hylan.  People marked with the sign of the death god Zehir have been cursed to live and die in an endless cycle, slowly losing their sanity and their memory each time.  The lords of Hylan have attempted to subjugate these undead, imprisoning or out right killing many of them, but the curse persists.

Players would play as dejected undead, fleeing from their homelands, coming to the land of Hylan in search of a cure.

In there way are a legion of holy knights dedicated to eradicating the cursed ones from existence for the glory of their god Pelor.

## Phone Notes
* The world is on its last lifeline, being subjugated by a demonic invasion brought by a meteor landing 80 years ago.
* An undead curse has spread through the populace, with their souls being bound to holy places called beacons.
* The undead pilgrims have been told they are chosen ones by the sun god Patyr to fight the endless hordes of demons.
* However, many more believe it is a curse and a blight on humanity, caused by the dark lord Nasca.
* Undead pilgrims seek the Church of the Hallowed Hand hoping to find a cure for their condition.
* The church is in the far northern lands of Hylan where the winters are brutal as well as the populace.
* Undead are also being driven north, hunted down by a band of paladins known as the Scorcher's Frenzy.

* The players begin the gameas part of a wagon train of pilgrims fleeing north when they are accosted by flying demons.
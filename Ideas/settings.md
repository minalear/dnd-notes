---
title: "Ideas"
---

## Steamspire
**New Setting**
A sprawling city built on the backs of colossal, steam-powered beast that roams the land. The city is a marvel of engineering, powered by steam, gears, and clockwork contraptions. The players find themselves in this bustling metropolis, where they must navigate its complex social structure, deal with corrupt officials, and uncover the city's secrets.

## Land of Dreams
**Existing Setting**
A new continent is discovered on Menadi beyond the Abyssal Sea and explorers from all around the world are seeking their fortune there.  However, not everything is what it seems, as the continent is full of ancient horrors that are beyond human imagining.

## Cydonia Reforged
**Existing Setting**
Cydonia rebuilt from the ground up, years into the future where civilization has built itself up to thrive on the desolate world.

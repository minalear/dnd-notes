---
title: "Misc Names"
---

## Seraphon City Names
Itza, the First City
Halanzec, the City of the Sun
Huata, the City of the Sea
Inco

## Seraphon Names
Olco
Tiquatla
Xahuontl
Ayotepec
Aluca
Epec

## Dwarf Names
Inod Sazirasen
Edem Bersibrek
Zon Asmel[ineth]
Sibrek[moldath]
Tirist "Ironhall" Dataniton 

## Goblin Names
Rigur
Rigurd 
Gobta
Gobzo 
Squee
Rathead
Pebble/Boulder
Boot
Domino
Hank
Phil LaMaar
Pinkie 
Binkie
Creeper
Eustace
Teapot
Kettle Belly
Dogshit/Catshit
Gumbo/Dumbo
Gallywix
Butt
Bedpan 
Fecal
Biscuit
Snicker Doodle 
Java
Richard
Devito
Crab/Goyle
Toil
Trouble
Saitama
Cloud
Rotgut
Jaybo
Blister
Gorbum
Lanky/Blanky
Nipnip
Hangnail/Toenail
Fuckpile
Gobber
Stumpy/Dumpy/Bumpy
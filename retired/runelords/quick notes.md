---
title: "Quick Notes"
draft: false
---

## Quick Ideas
* Thelsikar has a journal detailing his experiments.  Various information on the "Everdawn Pool" and how he wishes to find it to use its power to amplify his abilities 
* Red Bishop appears before disaster as a Red Robin and is allied with the ghost of the Chopper
* Forever Man wants to hire Iacco to get Sun Orchid elixir

## Jordan's Contacts
Golemworks - contact with Toth to use their laboratories
Swift Dolphin Warehouse (failed exporter's warehouse; now a hidden drug den) - used to gather more illicit materials for his projects
Boria's (poison seller disguised as a wine shop)
Dreaming Dryad (high-class drug den)

Guild of Apothecaries and Physicians (association of healers and naturalists)
Ravenfoot Corner (small school for alchemists and apothecaries)
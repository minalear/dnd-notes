---
title: "Old Notes"
---

Notes for the previous Rise of the Runelords campaign I ran.

## Setup
Rise of the Runelords takes place on the planet of Olorin.  In the Greater Arc, Olorin is a protected planet, meaning it is kept shielded from interactions from the galactic scale.  The denizens on Olorin do not believe that there are civilizations beyond their planet.

The ultimate crux of the campaign is that the ancient Runelord Karzoug has awoken and is attempting to reclaim his lost, Thassilonian empire.

Date for this campaign is 4719 AR.

## Players
* Seto Kaiba/Koro - Dylan
* Bane/Queen Mal - Keira
* Frank Shaw/Winston Crackbone - Zack
* Merlin - Trevor R.

## Allies
* Blacktooth Tribe (Goblins)
* Cult of the Blood God (Goblins)
* Sandpoint

## Antagonists
* Deus Brando
* Nualia
* Karzoug

## Locations
* Sandpoint
* Magnimar
* Blacktooth Keep

## Ideas
* A Kraken is destroying any ships that try to enter or leave a city's port.

## Summary
### Chapter 1
* Party meets at the Tors to root out Blacktooth
* Seto recruits the goblins, but grants Buttercup the crown when they leave
* During the festival in town, goblins attack, along with Buttercup and the gang
* They swear fealty back to King Fatseto.  Teacup is missing.
* Party figure out Aimeko's brother, Tsuto, is at fault and he is working with Nualia.
* They find him in the Glassworks with his father murdered.
* Merlin is kidnapped by Blacktooth and Thelsikar.
* Party go to Raven's Watch to free Merlin where they defeat the dragon and Thelsikar.
* They establish Blacktooth Keep as their base of operations and more goblins join, namely Scribbleface and Fluteface
* Spies are sent into Thistletop to figure out their plans
* Teacup is recovered, now a Hobgoblin of some power
* Nualia sends an army of goblins to destroy the keep, along with Orik (a traitor) and some bird assassins
* They are defeated and routed back.  Fear is set into the goblins about the new Blacktooth Tribe
* Party attacks Thistletop, defeating the forces there.  They recruit Lyrie, a Thassilon researcher (and fugitive) and Tobias, a magical white 'cat'
* Nualia is driven from Thistletop via teleport and she kidnaps Seto along with her.  Teacup defeats Malfeshnekor and steeps in his blood (later being dubbed the Blood God by his fellow goblins).

### Chapter 2
* Skinsaw murders begin to happen in Sandpoint and the party is tasked with figuring out the culprit
* They meet Koro and recruit him
* Their investigation leads to Aldern Foxglove and him spreading ghoul disease through the countryside
* They go to his abandoned manor to find a tree spirit named Vorel Foxglove and a giant mimic house named Martin.  After a quick fight, the party surenders and makes peace with the duo.  They find that * Aldern went to Magnimar.
* Nualia kept Seto under a spell for two months where they became lovers.  Deus Brando examined him indepth, discovering an ancient sleeping power within him.
* Seto wakes up in a prison and is forced to fight alongside some other people in a pit ran by a Deus Brando.
* Deus takes a liking to some of them and spares their lives, for now.
* Going to the city, the party is welcomed by Ironbriar and shown to their government provided villa for their visit.
* They split up to find clues.  Frank and Koro find Aldern is also sacrificed at his villa and all evidence points to the Brother of the Seven, a fraternity that Ironbriar, Seto's uncle, and Deus are members of.  They come to a house party ran by Gozaburo Kaiba.  They kill Kaiba after a fruitless confrontation with Deus.  They are now taking Ironbriar hostage and bluffing their way out.
* Furby and Bane meet Dinkle Brando and follow him to the Brando Mansion where they are kidnapped by Deus and Binx.  They awake in the prison with Seto and the others.  Nualia promises to break them out at midnight.

## Session Notes
### Chapter 2
* The party leaves the Foxglove Manor to head to Sandpoint, running into Bane's childhood friend Furby, heading to town to visit her mom.  They rest, allowing Merlin to perform some necessary repairs.  They head off to Magnimar.
* Guard stop the party before the enter the gates, stating that any visitors need visas due to a stint of murders happening lately.  Koro tried to pretend to be a liaison, hinting that Frank was a prominent mafia member.  The guard goes inside and rings the alarm.  Armed guards swarm the two, but shortly after are dismissed as Justice Ironbriar.  He welcomes the Heroes of Sandpoint into his city and takes them to a government owned villa where guests stay, while answering any questions they have about the city.
* The party splits up with Koro and Frank going to the Archives to view public records and find leads with Ironbriar, Merlin went to check out the Golemworks, with Bane and Kurby wandering around town looking for clues.
* Frank and Koro, after a run in with a colorful records keeper, find addresses to the Foxglove and Kaiba residences.  Investigating the Foxglove residence, they find the flayed remains of Aldern Foxglove turned ghoul.  They were ambushed by two members of the Skinsaw Cult who had instructions to kill Koro and capture Frank alive.
* Merlin meets with Toth Bhreacher behind the scenes who is amazed at the workmanship.  Toth invites Merlin to a business party at [[Gozaburo Kaiba]]'s estate where he meets [[Deus Brando]].
* Bane and Kurby meet [[Dinkle Brando]] in a bar, where Kurby recognizes his amulet as being of the Brando household.  They all start drinking together, with the girls hoping to get information out of him.  However, it was Dinkle luring them away to the Brando Mansion where they are kidnapped by Deus and Binx.
* Frank and Koro go to the Kaiba Estate to seek out more clues.  There they find the party that Merlin was invited to, along with various members of the wealthy elite.  Gozaburo announces that Deus has put in his bid to be the next Lord-Mayor, saying he's here to boost the economy of Magnimar.  Everyone cheers.
* Bane and Kurby awaken in the prison that Seto and Party B have been kept locked away.  Nualia visits, telling Seto about the [[Cult of the Blood God]] and how Deus wishes to extract both his and Frank's power.  She informs them that she is going to break them out at midnight.  Winston is thrown into her bag as well since he's easy to smuggle out.
* Koro and Frank get introduced to Deus, who already knows everything about them.  After some words, all the guests but Deus, Koro, Frank and Merlin blink out of existence.  Binx emerges from the fountain to size up Merlin.  There Koro attempts to challenge the power of Deus and is shown to be outmatched in every single way.  The two now realize that simply fighting him is not the way to go, after sucking some blood from Koro's neck.
* Deus allows them to leave, blinking the guests back into existence.  Gozaburo takes a bleeding Koro to a back room with a couple servants.  The two capture and torture information out of him about the Brothers of the Seven.  Many of the wealthy elite are members and it is a facade for the Skinsaw Cult.  Deus came claiming to be a messenger of Lamasthu and Norborgor and lead the cult into killing greedy souls to fuel the Runewell.
* Koro and Frank kill Gozaburo and then capture Ironbriar where they make a deal.  Ironbriar will take them to their captured friends if they allow him to live.

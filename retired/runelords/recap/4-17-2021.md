---
title: "4-17-2021"
draft: false
---

* Party wrapped up the Black Tooth cave quest, climbing the wall and handedly taking care of the skeletons
* They approach the nest of the black dragon as it returns, but not stealthily enough to avoid detection
* Priest is immediately knocked unconscious when he rounded the corner by the acid breath
* The dragon became afraid of the dragon bane longsword, but the thick hide prevented Armand from landing a strike before being knocked unconscious
* Bryn was nearly knocked out, but was able to recover Armand's sword and land a decisive blow, putting the fear into the dragon
* Mirai was able to create a duplicate of the sword via an illusion causing the dragon to retreat
* The party recovers a chest stolen by the dragon which appears to have been stolen from the Sona military company that Bryn/Iacco served in

* They vow to track down the dragon and finish it off for good and return to town
* They return to the Rusty Dragon Inn to rest and turn in the quest the following morning.
* They encounter Ameiko getting into an argument with her father and Mirai was able to get more info about her relationship and backstory.
* The party returns to their rooms and proceed to fall asleep.

## Notes
* They returned their mounts to the stable and the owner offered them to continue their lease for free for a week since they paid so handsomely.
* Iacco is interested in testing equipment against dragons
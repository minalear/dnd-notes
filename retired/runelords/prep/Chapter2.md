---
title: "Chapter 2"
draft: false
---

# Chapter 2 Notes
* Bryn will be contacted by Baphomet and offered a deal to give him more power, slowing turning him into a demon.

# Fronts
## Forever Man
Staunton Vhane, using his moniker of the Forever Man, has personally requested the assistance of Iacco.  He has used up the last of his Sun Orchid Elixirs and requires more.  He trusts none of the established alchemists or the back alley drug dealers to make it except for "Wacko Yacko"

The Aspis Consortium has a secret container ship arriving with what Vhane believes to contain the secrets to the Sun Orchid Elixirs.  If the party were to acquire the information, they will be rewarded greatly.  Vhane needs someone he knows he can trust to get the job done and not to ruin any reagents.

On the boat is a young alchemist girl, Elara Tzarreth, the daughter of the famed monk Edric from the Crystal Desert.  She and her brother broke into the citadel and stole an old tablet that contained information of the alchemy process.  Her brother escape but she was captured by authorities and imprisoned within the Thundermaw Spire within Carda.

The Aspis Consortium brokered a deal with the warden, smuggled her out of the region, and took her to a remote laboratory where she can try and recreate the elixir.  Vhane will want the girl and will pay an astronomical amount of money for her.  If they refuse, the full wrath of the Skinsaw Cult will bear down upon the party to retrieve the girl by any means possible.

## Sandpoint Devil
## Armand's Family
## Black Dragon
The dragon has fled east into the Hinterlands and has landed at an abandoned fortress known as Raven's Watch.  Originally used as a prison for local criminals, it was abandoned many years ago when Denton built a jail below the town garrison.  Now the dungeon is the home of a vile priest named Thelsikar, an evil cleric who worships the demon goddess Lamashtu.  Thelsikar is using Raven's Watch to collect a number of monsters and is experimenting upon them via alchemical and surgical means.  He intends to create the perfect monster for the glory of his goddess.

Black Fang has made an alliance with Thelsikar and, in return of elixirs that grants the dragon more power, he will protect the fortress and be a willing minion in Thelsikar's schemes.

## Demon Infestation
Szuriel contacts Bryn offering him power in exchange for his servitude, granting him a number of boons based upon his bringing of war to the world.

Boons
* Tactical Acumen - The quicker reflexes and sharp senses granted by Szuriel grants you a better mastery over battlefield tactics.  Whenever you would gain a bonus on attack rolls or to AC due to battlefield position, such as flanking, higher ground, or cover, you gain an additional +1 insight bonus. This bonus increases by +1 for every five caster levels above 5th you possess (maximum +4).
* You may cast Haste once per day as a swift action as a spell-like ability.

Awaken your blade by driving it into the heart of the dragon that fled your fury.

## Thassilonian Ruins
* The Sunken Queen
* The Pit - massive hole at the center of the Devil's Platter and the rumored home of the Sandpoint Devil.  It interconnects many of the subterranean caverns running through the platter itself.  Explored years ago by Ilsoari Gandethus and his small group of adventurers, he found an ancient stone door marked with the Sihedron Rune.  He was never able to open it and his friends died during the process.  He fled and always regretted the story.  He *had* a key marked with the same rune that is now in the possession of a Mirai.

The Pit leads through various caverns that resemble a lesser Underdark.  It is host to numerous 

# Sona
The city of Sona is going through some political turbulence.  The former Lord-Mayor Haldmeer Grobaras has died under mysterious circumstances (many believing he was assassinated) and emergency elections are being held to find his replacement.  The current acting mayor was Grobaras' assistant, Valanni Krinst, who is doing the best he can to manage this morally bankrupt city.  The Council of Ushers are moving to remove the office entirely, taking the opportunity to enact themselves as an even more powerful part of government.  The two running mayors are Frank Shaw, a local denizen from Denton who found fame for exploring lost ruins many years ago, and Dal Vornado, a ruthless businessman who supplies the armies with much of its munitions.  

Frank wants to pull out of the war, saying the city has wasted so many lives and money on a war that won't even affect us.  Dal wants to continue the war, talking about the pride and loyalty for the alliance, but in reality is just directly benefiting from it.  The denizens see through the facade of Dal and the locals support Frank.

Verrine Caiteil is the current leader of the council and is also wishing to pull out of the war and strip the Lord-Mayor's office of all of its authority.

* Manor given to the party
* 400 gp each


* Thelsikar might have notes on inflicting wounds through magic

* Mirai will contact Dong for help establishing a research base in Denton.
* They will try, but will run into red tape established by the archaeological society in Sona.
* It will be revealed that the Aspis Consortium, tipped off by Lyrie, has bought digging rights for "all known and unknown research sites in Valdovia" for an obscene amount of money
* Thelsikar might be a rogue agent who had stolen information from Conference Z
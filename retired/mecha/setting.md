---
title: "Setting"
draft: false
---

## FTL
* FTL technology is developed allowing the relative ease of space travel between local start systems.
* Various colonies are formed and the reach of humanity broadens to the immediate surrounding sectors.
* Even at Warp 4 (4x speed of light), traveling to Alpha Centauri took about a year of time, resulting in interstellar commerce being not very practical.
* Over time, these colonies became more independent from the nations of earth, many eventually establishing themselves as sovereign nations not bound by earth customs.
* The nations of earth attempt to police the colonies but many attempts became futile due to the vast distances involved.

## United Earth
* In the following decades, the colonies expanded and technology advanced.  A scientist on a research station orbiting Wolf-359 develops a new class of FTL utilizing fixed points called "Jump Gates."  These gates were capable of opening slipstreams of compressed time and space, allowing traversal of great distances within the fraction of the time.
* With the advent of these gates, travel and commerce became commercially viable and travel between earth and the outer colonies increased 1000 fold.
* With easier travel, warfare also became easier.  Independent colonies now began feuding and open conflicts erupted.  The nations of earth tried to exert control back over, which resulting in a conflict that engulfed all of humanity.
* At the end, when all the ashes had settled, a new world government was established; the United Authority of Planets.  The USC (United Space Command) is the primary military agency that secures the UAP's control.
* Over time, the government has become increasingly corrupt with falsified elections and clandestine government agencies keeping control.

## Modern Times
* It has been 900 years since the advent of the jump gate.  The reach of UAP has reached out four thousand light years, creating an expansive and hard to maintain empire.  Fearing a repeat of history, UAP tasks USC and OGR to maintain the fringe colonies, suppressing rebellions and revolts "by any means necessary."
* The central systems are fed propaganda about the reasons for these revolts and the heroism involved in bringing peace to fringe space.  In reality, due to logistics, the fringe colonists are repressed by corrupt governors and uncaring central political bodies.
---
title: "Ideas"
draft: false
---

* Another clone that is on the opposing mech team wants to capture Ryan and use his genes to cure his cellular degeneration.

## Game
* The game starts off with the party joining a Fringe Mecha Unit, a team assigned with patrolling and maintaining colonies in fringe space.
* The ship they're aboard is called a Strike Carrier, a small and fast ship designed to deploy mech fireteams quickly into remote locations without notice.
* Depending on the mission, multiple fireteams can be deployed into Squads and Sections, usually supported by USC ground forces like marines or special forces.  Otherwise, fireteams usually work solo, due to the extreme cost of building and maintaining mechs.

## Organization
* Mech rifle teams are usually accompanied by a ground force of marines.  Together they form a Strike Team.
* Strike Teams may work with other Strike Teams on larger missions that require more coordination.  These missions are usually ran by higher ranking officers.
* A mech rifle team is commanded by a team leader, usually a Corporal.  The Strike Team falls under a ship captain that helms the Strike Carrier.

### Roles
These are the roles that comprise the ship's crew that exclude the rifle teams
* Ship Captain
  * First, second, and third officers
* Chief Engineer
* Chief Medical Officer
* Chief Science Officer
* Chief Tactical Officer

* For most logistics regarding the matter replicator and mech repair, the Chief Engineer and his support crew will be the main point of contact.
* Communications are ran by the Chief Tactical Officer who coordinates Strike Missions.
* A NHP named Data assists from the command center, usually coordinating everything not directly controlled by the Tac Officer.

## Setup
* Someone has been outfitting fringe groups with weapons and arms.  The planet Danalith is one of these rebellious outposts that has taken up arms against the UAP, executing the local governor and establishing their own provincial government.
* Danalith is home to a secret science outpost of OGR who has been researching a top secret project and the PCs are sent to extract the scientist and to put down the uprising.
* The team will be accompanied by an OGR operative who will replace the Tac officer for the mission.
* They have heavy anti-air batteries pointed at the sky awaiting the eventual USC response.
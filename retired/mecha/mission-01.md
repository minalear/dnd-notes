---
title: "Mission 01"
draft: false
---

# Operation Hammer
Operation Hammer is a coordinated effort of three strike teams and a UASC naval frigate to invade the revolting outer colony world of Damascus.  Local guerrilla fighters have seized a local garrison and capital and are holding the sector governor hostage.  Naval intelligence (OGR) have taken command of the operation and have appointed Admiral Hackett to coordinate the ground forces.

The rebels have secured and fortified the city with the arsenal obtained from the garrison.  

## Intelligence
Much of the local garrison joined the revolt, leading to the rebels to quickly secure resources.

## Strike Teams
1. Charlie 
1. Atlas
1. PCs

## Operations
Charlie team will take point an
---
title: "Damascus"
draft: false
---

## Evergreen
  * Landmark Colonial settlement, administered by the NHP **Patience**
  * Established about 50 years ago and is enjoying its second generation of growth
  * Recently came under attack by unseen enemies
## Gray Towns
  * Hundred kilometers to the east and south of Evergreen
  * Various settlements established outside of the permission of Landmark Colonial, "gray areas" of legality
  * Social hubs for the noncitizen residents of Damascus
  * **Liu Maize** and **Merricktown** are the largest of these
  * Primarily agricultural and are very lightly populated
  * Most people here live in small clusters of two to four families working large acres of farm land
## The United Cities
  * Remaining Egregorians on Solo Terra have banded together to form the Damascus United Cities (DUC)
  * Composed of three subterranean cities built into the ruins of old Egregorian hives
  * They depend on the combination of local industry, scavenging, reclamation, and repurposing of the manufacturing equipment and military hardware scattered around the planet's surface.
  * Hivehome boasts a significant metropolitan level of population

# People
  * Patience - Evergreen's Administrator and NPH
  * Brava Hadura - commander of Evergreen's militia
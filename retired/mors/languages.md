---
title: "Languages"
draft: false
---

## Ethnicities
Berber
Phoenician
Egyptian
Greek
Roman
Jewish
Arabic
	Nabataean
	
## Languages
Aramaic - Levant/Nabataean
Arabic - Nabataean/Other Arabs
Armenian - Armenia
Berber - Numidian/North African
Cilician - Persian+Phoenician derivative for Orcish settlers
Egyptian - Egypt
Gaelic - Celts/Gauls
Greek - Greece/Hellinized Nations
Hebrew - Israel/Judah/Levant
Latin - Rome/Roman Provinces/Roman Allies
Lycian - Pergamon/Halicarnassus
Nubian - Southern Egyptian/Kushitic
Persian - Persia/Descendant
Phoenician - Carthage/Phoenician Colonies/Allies
---
title: "Setup"
draft: false
---

It has been one year since the start of the 2nd Punic War and the sacrifices of our valiant heroes in Lower Gaul (France).  Hannibal and his brother have been focused fighting a ground war in Iberia (Spain) defending the important silver mines of Carthage.  Carthage's main force has been fighting a war against Roman aligned Numidians in the south, leaving mainland Europe untouched by the war.

Powerful magical artifacts have been entering the west by smugglers exploiting the wealth of magic in the blasted lands in the east.

# Campaign Goals
* Establish allies in the Levant/Anatolia area (Israel/Turkey)
  * Hasmoneans, Nabataeans, and Armenians hate the Seleucids (former client states).
  * Seleucids wish to reestablish their control of the East and are looking to forge an alliance with Rome to do so.
* Disrupt Roman influences in the East and cut off their supply of magical artifacts
  * Seleucids are courting the Romans who are established at Arados Island
* Stop the Roman fleet lead by Pompeii so Carthage has a chance in the water
  * To get the element of surprise, Hannibal wants to enter Italy from the East, using their allies aid.  Will need the Roman navy to be dealt with.

# Setup
* The players will start the game at Tusk HQ as new recruits being tested by Taika Waititi.
  * Zack's character is a liaison from the Medjay in Egypt and will act as a friendly party with inside information of the area.
    * The Medjay lack the jurisdiction to patrol the Levant and require outside help.
  * Their ultimate goal is to disrupt the artifact smuggling happening in the east and to forge alliances for Carthage.
  * A small group of Myrmidons will test them in combat.
  * As a final test, Rickaeous will emerge with an enlightened star stone hammer to put them through their paces.
* During their tenure on the island, they will learn the basic languages of the Levant (Aramaic) and be given supplies to get their mission started.

# Notes
* The biggest source for smuggled artifacts are the Nabataeans in the east, with two major entry points into the blasted lands being Petra and Damascus.  They're fiercely independent and the government is too corrupt to do anything about the practices.
* The Selucids also have access to the Blasted Lands and are training an advanced group of guides and warriors that are able to delve deeper to retrieve the most valuable pieces.
* Tusk is a very small organization with most of the agents deployed throughout the combat zones to support Carthage.  The agents are few and are stretched thin, which is why this task falls to a new group of recruits.
* The players will have their primary mission as well as their secondary mission; secure allies in the east and to stop the flow of magical artifacts to Rome.
* Their primary mission is to secure allies anyway they can here in and around the Middle East.  Carthage is losing the war and she needs all the help she can get.
  * To do so, the players are presented the current political situation in the Hasmonean Kingdom.
  * **Simon Thassi**, the current High Priest, is nearing the end of his life.  His two sons are poised to succeed him, but are diametrically opposed in ideals.
  * **John Hyrcanus** is Simon's eldest son and wishes to keep the Kingdom independent.  The traditionalists are very anti-magic, stating that all magic should be derived through God.  To obtain independence, he is looking to make an alliance with Rome who are more than eager to do so.
    * John is currently based out of Jericho due to the proximity to Jerusalem.  He lives a modest life among his family in the royal palaces.
  * **Ptolemy, Son of Abubus** and Son-in-Law to Simon, is a Greek who favors Hellenization and wants to return as a client state to the Seleucids for protection.  Feels that the Kingdom cannot sustain itself and is failing to feed its people under the current circumstances.
    * Ptolemy is currently based out of '''Ashkelon''', a port city towards the south near the Nabataean controlled region.
* Their secondary mission is related to a smuggling group known as the Scions of the Seven; referring to the seven holy emanations of the dead god Ahura Mazda.
  * They are a mix of Nabataean natives along with some Romans who are members of the Cult of Mithras.
  * Their latest score; the twin Mainyu mirrors, representing the good and evil spirits created by Ahura Mazda eons ago.

## Fronts
* Hasmonean Kingdom
  * Simon Thassi is close to his death, however, he'll never see it as long as Ptolemy's plots go undisturbed.  Simon and his two will be killed on his orders during a dinner and assassins will fail to kill the third son, John Hyrcanus.
  * Rome is currently sending an envoy to meet with John in Jericho to negotiate a trade deal and military protection for the region.
---
title: "Session 01"
draft: false
---

## Arriving
* Arriving in Apollonia, the players will be directed to seek out M. Night
* He will tell them the full situation of the Hasmonean Kingdom.
* He is currently seeking more information about the Seleucids ands the Nabataeans.
  * Seleucids are a declining empire looking to do anything it takes to reclaim their lost legacy; also the legacy of Alexander.
  * They wish to reconquer much of their lost land, which includes the Hasmoneans.
  * The Nabataeans are staunchly independent, but will surrender facing any extensive military presence in their lands.
* If the players wish to court Ptolemy or John, they will need a recommendation from a noble.
  * The local lord of Apollonia took over the lands seven month ago after the death of his father.
  * He is noble and kind and is looking to help the helpless and hungry in anyway he can.
  * Adam Levine will ask the players to retrieve a lost heirloom of his family in the ruins of Samaria.
    * Samaria was the capital city of Israel 400 years ago that was destroyed by the Assyrian Empire.
    * The heirloom is the Crown of Hoshea, the last king of Israel.
    * Hoshea is descended from the first king of Israel who lead the revolt that split the kingdom into two (Israel and Judah).
    * In reality, Adam is only related by the thinnest of evidence.  He has aspirations of reuniting the region back into the greater Kingdom of Israel.
    * The crown is in the lost necropolis of Samaria which houses the tombs of the kings.
    * One thing to consider about the necropoli in the Levant is that the residual magic left behind the death of Alexander causes a perpetual spread of undeath.
    * The necropoli are managed by a guild called the Grave Diggers, who embalm and entomb new dead and keep the old dead asleep.
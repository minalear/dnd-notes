---
title: "10-16-2021"
draft: false
---

## Recap
The party has successfuly been introduced to the real Stoney.  The party found that he has been brought back to life by his god to take his vengeance on Pompey and free the Mediterranean from the Roman threat.  However, the time he has is short.  The party has continued their plan of entering Set's realm by means of the Barge of Ra, the Mendjet.  To acquire the vessel, the party has arrived in Alexandria seeking answers from the elusive shapeshifting demigod Proteus.  Mito has contacts within the city that can help get them to the island, but from there, the party is on their own.

## Setup
The city of Alexandria is the cultural heart of the Ptolemaic Kingdom, resembling the mending of cultures with the more recent Greek architecture and the older Egyptian structures.  The city is structured under two layers, the inner and outer cities.  The inner city, built upon Mt. Copron, is where the great library and academy of Alexandria lies along with the Royal Palace.  The outer city is where most of the populace resides along with most of the temples, theaters, bath houses, forums, and military structures.

## Alexandria Fronts
* Cleopatra - the daughter of the current Pharaoh is enamored by Caesar.  She secretly runs a cult called the Caesarium who wish to bring Roman interests into the Ptolemaic kingdom by any means necessary.
* Alexander - the undead lord still resides beneath the city, in a constant state of torture, bound by a coffin made of pure gold.  His screams ring out to haunt those unfortunate enough to hear them.
* Proteus - the shapeshifting demigod hides within the island of Pharos, keeping to himself for the most part, but occasionally causing mischief.

## Proteus
Proteus will try to offer the party other options when they with for the barge.  He'll tell them about the agents of Rome currently undermining the city or of an ancient threat beneath the city that wishes to be heard once more.  He can tell Sucari where his father is, tell Muhad how to cleanse his soul of the taint, or give Mito what he desires most; the BOB.
---
title: "9-4-2021"
draft: false
---

# Background
Gnaeus Pompeius Magnus, or Pompey the Great, is the *Son of Poseidon* and has dominion over the Mediterranean with his powerful fleet.  Servant of Rome, he is ruthless with the destruction of her enemies.  He is the chosen of Poseidon, some even claiming him to be a legitimate demigod, and is favored by the old waters.

Stoney is a powerful Pirate Lord in service to a much more ancient primordial being known as Leviathan.  Leviathan is a child of the original gods and has true dominion over the seas, unlike most of the younger, pretender gods.  He remains neutral, but takes a liking to some other entities from time to time.  The corruption can be removed by any antimagic or anticurse effect or by slaying the Nereid.

Pompey, to defeat the Pirates of Tarsis, used his godly connection to trick and corrupt Stoney via a prophet of Poseidon known as Proteus (Old Man of the Sea).  Proteus is a rather unknown child of Poseidon that possesses the ability to change form.

# Setup
The Screaming Eagles, on board a stolen vessel known as the *Inheritor*, escaped the seige of Tarsis by the skin of their teeth and the deaths of many of their brethren.  Only a few remain, far too few to successfully pilot the vessel.  Stoney, being corrupted by the visions of Proteus, has begun sacrificing locals (thinking their Romans) to the statue of Dagon, which he claims to be Leviathan personified by the locals.

The Screaming Eagles are located within a cave near the destroyed town of Ashdod.  Harvey is sent to scour the town for supplies along with Aberdeen and the two Orc crewmates (Krenko and Slythe).  Everyone else is located within the cave entrance with a sleeping Bargs keeping watch.

The initial reactions of the pirates are to be hostile, but can be swayed if they know that the party is anti-Roman.  But they aren't very trusting either.

Stoney keeps to himself in the back of the cave where he communes with "Dagon"  In reality, he's communing with a Sea Hag known 
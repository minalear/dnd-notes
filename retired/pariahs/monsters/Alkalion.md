---
title: "Alaklion"
draft: false
---

# Alkalion (CR 4)
N Medium Aberration
Init +8; Senses Darkvision 60ft; Perception +1

## DEFENSE
AC 16, touch 14, flat-footed 12 (+4 Dex, +2 natural)
hp 39 (6d8+12) // 60 max
Fort +3, Ref +6, Will +6
Resist Acid 10 

## OFFENSE
Speed 40 ft.
Melee 2 claws +5 (1d4+1), bite +3 (1d6+1) 
Space 5 ft.; Reach 10 ft.

## STATISTICS
Str 13, Dex 18, Con 12, Int 2, Wis 12, Cha 12
Base Atk +4; CMB +5; CMD 19 (23 vs. trip)
Skills Climb +10

## SPECIAL ABILITIES
Roar (Ex)
A Alkalion can let out a savage roar.  Any enemy within 60 ft must make a DC 18 Fortitude save or be paralyzed - at the end of each round thereafter, the paralyzed victim can attempt a new Fortitude save to recover from this paralysis.  When a victim recovers from an Alkalion's paralysis, the victim is taggered for 1d6 rounds.
---
title: "Moonlight Lich"
draft: false
---

# Moonlight Lich (CR 4)
NE Medium undead (augmented humanoid)
Init +0; Senses Darkvision 120ft; Perception +20
Aura delusory aura (100 ft.)

## DEFENSE
AC 21, touch 11, flat-footed 21 (+7 armor, +1 deflection, +3 natural)
hp 97 (6d8+12) // 132 max
Fort +7, Ref +3, Will +12
Channel Resistance +4, soul shield, spell storm;
DR 15/bludgeoning and magic;
Immune cold, electricity, undead traits

## OFFENSE
Speed 20 ft.
Melee +1 scythe +15/+10 (2d4+8/19-20/x4)
Special Attacks
  Channel negative energy 9/day (DC 19, 6d6)
  disembodied strike (1d8+5)
  hand of the acolyte (8/day)
  soul lash (DC 19; 5d6)

## Spells
Domain Spell-Like Abilities
  8/day - bleeding touch (5 rounds)
  1/day - dispelling touch

Cleric Spells (CL 11th; concentration +16)
  6th - antilife shell, create undead
  5th - flame strike (DC 20), slay living, symbol of pain (DC 20), unhallow
  

## STATISTICS
Str 13, Dex 18, Con 12, Int 2, Wis 12, Cha 12
Base Atk +4; CMB +5; CMD 19 (23 vs. trip)
Skills Climb +10

## SPECIAL ABILITIES
Roar (Ex)
A Alkalion can let out a savage roar.  Any enemy within 60 ft must make a DC 18 Fortitude save or be paralyzed - at the end of each round thereafter, the paralyzed victim can attempt a new Fortitude save to recover from this paralysis.  When a victim recovers from an Alkalion's paralysis, the victim is taggered for 1d6 rounds.
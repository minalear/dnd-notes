---
title: "Arachnopolis Rex"
draft: false
---

# Arachnopolis Rex (CR 4)
N Medium Aberration
Init +4; Senses Darkvision 60ft; Perception +8

## DEFENSE
AC 19, touch 12, flat-footed 16 (+3 Dex, +6 natural)
hp 39 (6d8+12) // 60 max
Fort +7, Ref +5, Will +2

## OFFENSE
Speed 25 ft., climb 25 ft.
Melee bite +8 (1d8+5 plus grab and poison) 
Space 10 ft.; Reach 10 ft.
Special Attacks Puppet

## STATISTICS
Str 20, Dex 17, Con 15, Int -, Wis 10, Cha 2
Base Atk +4; CMB +10 (+14 grapple); CMD 23 (35 vs. trip)
Skills Acrobatics +11, Climb +21, Perception +8, Stealth +11 (+15 in webs)

## SPECIAL ABILITIES
Arachnopolis (Ex)
For every point of damage the Arachnopolis Rex takes, a small vermin spider is created in an adjacent space (see Spider, Giant Crab).

Puppet Grab (Ex)
While grappled with the Arachnopolis Rex, the spider adds the victim's BAB to its attacks.

Poison (Ex)
Bite—injury; save Fort DC 17; frequency 1/round for 6 rounds; effect 1d4 Constitution damage; cure 2 saves.
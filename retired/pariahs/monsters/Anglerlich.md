---
title: "Anglerlich"
draft: false
---

# Anglerlich (CR 5)
## The Lure
N Medium Humanoid
Init +2; Senses Darkvision 60ft; Perception +2

### DEFENSE
AC 15, touch 12, flat-footed 13 (+2 armor, +2 Dex, +1 shield)
hp 5 (1d10) // 10 max
Fort +2, Ref +2, Will -2;
Undead Immunities (for not being a real creature)

### OFFENSE
Speed 30ft.
Melee rapier +3 (1d6/18-20)
Ranged hand crossbow +3 (1d4/19-20 plus poison)

### STATISTICS
Str 11, Dex 15, Con 10, Int 10, Wis 9, Cha 10
Base Atk +1; CMB +1; CMD 13
Skills Perception +2, Stealth +2
Languages any


## The Fish
N Large Aberration
Init +7; Senses Darkvision 60ft; Perception +6

### DEFENSE
AC 17, touch 12, flat-footed 14 (+3 Dex, +5 natural, -1 size)
hp 51 (6d10+18) // 78 max
Fort +8, Ref +8, Will +3
Defensive Abilities ethereal jaunt

### OFFENSE
Speed 40 ft., fly 30 ft (perfect)
Melee bite +10 (2d6+7 and grab) 
Space 10 ft.; Reach 5 ft.
Special Attacks etheral ambush

### STATISTICS
Str 20, Dex 17, Con 16, Int 7, Wis 13, Cha 10
Base Atk +6; CMB +12 (+16 grapple); CMD 25 (37 vs trip)
Skills Perception +6, Stealth +7

### SPECIAL ABILITIES
Ethereal Ambush (Ex)
A Fish that attacks foes on the Material Plane in a surprise round can take a full round of actions if it begins the combat by phasing into the Material Plane from the Ethereal Plane.

Ethereal Jaunt (Su)
A Fish can shift from the Ethereal Plane to the Material Plane as a free action, and shift back against as a move action (or as part of a move action).  The ability is otherwise identical to ethereal jaunt.

Swallow Whole (Ex)
The Fish who has successfully grabbed an opponent can attempt a new combat maneuver check to swollow the victim.
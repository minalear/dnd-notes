---
title: "2022-01-30 (Recap)"
draft: false
---

# Recap
## Knotmen and Friends
 * Theo rejoins the party, miraculously alive after their previous combat encounter
 * The party travels along the river until they come across the reported Knotmen village
 * The Knotmen are digging at a relic hidden in the wall, thinking it is the Deep Carbon Observatory
 * Party ambushes them and swiftly defeat them, but at the cost of their slaves killing themselves
 * The only slave that lived was one with a destroyed leg
 * Theo, with the help of Sorin, were able to remove his knot and fix his leg, allowing him to be free
 * The party name him Will for he was never given a name
 * Will informs the party of some of the dangers ahead and the fact that there are two routes they could take
  * One route leads down and towards the Dvargir
  * One route leads up and connects with an Aelf-Adal village that the Knotmen traded with

## Relics of the Past
  * The party explores the shaft that the Knotmen were digging at
  * They discover part of a vessel from another world, full of otherworldy technology and cultural artifacts
  * They find evidence of advanced armor and weapons (namely guns) from this other world
  * They also find a large animated skeleton who befriends Sorin, recognizing the soul of his friend inside the other
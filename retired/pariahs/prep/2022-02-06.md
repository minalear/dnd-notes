---
title: "2022-02-06"
draft: false
---

# Roadmap
  * The party is 500 miles beneath the surface and they are seeking the quickest way out
  * There are a number of routes, each with their own sets of challenges.

## Route \#1 - Direct
  * The party can just climb straight up and out which will take seven ingame months to achieve
  * The party will run into random encounters with various beasts and various challenges that neither guide can predict
  * Requires traveling through the capital city of the Aelf-Adal; Riven

## Route \#2 - Deep Carbon Observatory
  * Joining the race to find the DCO
  * Among other things, the DCO has a matter transporter that can teleport everyone to the surface
  * Will involve traveling through the capital city of the Knotmen; Foreclose

## Other Options
  * The players can attempt to pursue other options to reach the surface

## Driving Forces
  * The biggest driving force for the PCs is escaping the Underdark
  * Theo wants to spread his religion and obtain power
  * James still wants to murder his grandfather and separate Jinzo from his body
  * Nalla wants to remove the curse on her people, the waking nightmares that drive her people mad
    * Nightmares perpetuated by the living world above
    * She also wants to recover the memories of her father
  * Ron wishes to exercise evil from all corners of the world

# Setup
## Nalla and the Adal
  * Party will learn she and her people are outcasts from the city of Riven
  * The city is a place of nightmares, literally created from the fears and dreams of the surface
  * As outcasts, they are not affected by the waking nightmares as much, but they do still feel it 
  * In reality, Riven lies on the border of material and dream, being partly in the Realm of Leng, where Lloth's other loyal subjects reside; the Denizens of Leng
  * The Denizens of Leng act as the superior race residing over all of Adal kind in service to their queen.
  * The Adal are dedicated to their queen as well, but distrust the Denizens.

## Going Forward
  * Nalla will commune with the Sight Seer of the village, her mother, who has also forgotten the father
  * The Sightseer will instruct the party to travel to the "Glade of Eternal Moonlight" to find the Oracle
    * This is a very dangerous journey
    * The glade is home to the Mond
  * If the party decides to go for the Civilopede instead, it will take them through another (albeit similar) path
  * The route both crosses Lake Hathir which is full of scissorfish and a few mantis shrimp

## Fossil Vampires
  * The Fossil Vampires has been tracking the party since they left the body of the Gnonmen guard for it to feast upon
  * They have been cleaning up the kills in their wake and is hoping to score from their exploits
  * Since they've stopped at the village, the vampires have been growing restless and are wanting to strike out in frustration, hoping to take Will

## Gilgamesh
  * Three souls of ancient warriors embody this behemoth who wanders the Veins hoping to accomplish their lost goals
    * Elindis Matriel, Taika Waititi, and Gilgamesh
  * The statblock for Gilgamesh uses a Rock Troll, but uses a large sword rather than 2 claws (3d6+7)
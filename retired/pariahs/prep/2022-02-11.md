---
title: "2022-02-11"
draft: false
---

## PCs
  * Sorin
  * James
  * Theo

## Companions
  * Ron - Trilobite Paladin seeking to rid the world of evil, one evil-doer at a time
  * Nalla - Adal huntress seeking to restore the memories of her father by defeating the Spectre of Brocken

## Setup
  * The party is currently dragging the dead body of Gilgamesh towards Lake Hrathir
  * They will appear on a remote shore of the massive lake and will use the body to distract the local scissorfish to allow them to swim
  * Their goal is to reach the Glade of Eternal Moonlight and commune with the Oracle to find answers
  * Within the Glade lies the Moonlight Lich, a weaver of nightmares and creator of monsters, using the various moonlit waters as his medium

## Encounters
### Lake Hrathir
  * Lake Hrathir is a massive subterranean lake with miles of shoreline
  * Various parts of the shore are remote, but others are settled by various Underdark civilizations

### Moonlight Lich
  * The Moonlight Lich is a connoisseur of fine arts and craft, making macabre displays of all the poor creatures that have wandered into its path
  * It has established itself here in the Glade a number of years ago, cutting off the Underdark from accessing the Oracle
  * Seraphiel isn't bothered by its prescence, as long as it didn't bring harm to Metatron
  * Any kill the Lich secures will bind the soul of the slain into a horrific statue, capturing the moments of their demise

### Oracle
  * The Oracle is a passive neutral agent acting within the Underdark
  * He calls noone enemy and the only friend he has is his loyal guardian, but even he treats with an element of indiference
  * He will grant anyone group that visits him three questions
    * And does not appreciate when people try to game the system
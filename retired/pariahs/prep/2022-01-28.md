---
title: "2022-01-28"
draft: false
---

## PCs
  * Sorin (Ryan) - Drow gunslinger and the inventor of guns on Edur, took his weaponry from the Aspis Consortium, afraid they would use it for terror
  * James (Zack) - rogue guy who stabs things a bit too often
  * Theo (Dustin) - maddness freak who does whatever, supposedly spreading a religion

## Setup
  * The party is currently infiltrating a Gnonmen village after murdering the guard outside
    * Guard didn't want to fight, was attempting to scare away the three strangers who wanted to enter the village where his wife and kids were
    * Being in the Underdark, the literal most hostile place on the planet, leads to people not being very trusting
    * Killed after not cooperating immediately or trusting the guy who wanted to touch him for no reason
  * High chance the Gnonmen village will be inconsolable after this
    * Will demand a life to replace the one taken, most likely taking the baby into their care to be raised by them
    * Will demand the party to leave and never to return
    * Will defend with their lives to kill the party if pushed to it
  * If placated, the village will reveal to the party a passage way forward towards whatever their destination may be
    * The party wishes to leave towards the surface, so the common sense direction is up; through miles of river and waterfalls

## Gnonmen Village
  * Ran by three elders who take the protection of their people very seriously
    * Nogal, Hect, and Pot
  * Sustained by a burgeoning mushroom farm worked by the various people and guarded by the more mighty

### Treasures
  * 2x Fungal Lamps - glows in a soft, steady green glow.  If the lamp is damaged, the lamp will colonize any open wounds on the holder.
  * Container of 5 Luminal Cigars - the smoke from these glow like a gentle dawn, each can be used for 30 minutes

## Tachyon Troll Trader
  * Filbert is a Tachyon Troll, but he doesn't let that stop him from being the best darn salesman in the Veins
    * Items for sale: Ring of Delayed Doom, Ring of Foe Focus, Mother Deck (appears as a Deck of Many Things), and a Rod of Wonder
  * Filbert appears to be brutally harmed from a recent fight, but any questioning about it he denies anything (because he likes the party)
  * He will recognize Sorin (or at least another soul embodying his body), claiming they were together in a room at once (alluding to Sona on Cydonia with Ezrael)
    * First hint that maybe there's more to the party's appearance here than meets the eye
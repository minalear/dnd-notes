---
title: "4-23-2021"
draft: false
---

## PCs
* **Edric Tzarreth** - friends are dead or gone, is seeking for away to get home and check on his family.
* **Tyrus Steelrock** - ship wrecked on the island and is looking for ways to escape the island.
* **Jessica** - Member of the Grimm Troupe, seeking to escape the island and her old life.
* **Zack** - kidnapped by the dragon cult for some reason and is looking to escape.

## Start
The party will be resting in their makeshift shelter.  Andross has taken it upon himself to make various reinforcements, creating a makeshift wall and beds for the various survivors.  A few more people, unknown to the party, have made their way here.  Tror comes in carrying a few kobolds who were scouting out the area and takes it upon himself to keep the camp secret with various traps and tracking.

## Scenes
* **Camp**
    - Ding gives the party 
* **Forest Trek**
* **Excavation**
    - The cult is currently excavating an ancient Jaghut tomb of the warrior **Gothos** in search of one of the binding stones of the **Ur-Dragon**.
    - The excavation is ran by a Dragon Priest named **Balthazar**, a big meaty evocation wizard representing the Red Aspect.
    - Balthazar has a Young Red Dragon and a number of wrymlings patrolling the area and a number of minor cultists, half-dragons, and warriors protecting the dig itself.

## NPCs
- **Ding** - weakened from his disease and without his medicine, Ding's status will deteriorate fairly quickly without assistance.  His main goal is to support the party with knowledge and support.
    - Ding will give the party two cyphers, beads that can summon air elementals as per the Conjure Elemental spell.
- **Caikocri** - wounded fighting the kobolds and will be supporting Ding during time, helping establishing a base camp and supporting the party.
- **Dalia** - tiefling assistant to Caikocri, taking it upon herself to cook and maintain the camp for all the survivors.
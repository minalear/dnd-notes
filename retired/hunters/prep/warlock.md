---
title: "Warlock"
draft: false
---

## Spellcasting Ability
Charisma is your spellcasting ability for your warlock spells, so you use your Charisma whenever a spell refers to your spellcasting ability. In addition, you use your Charisma modifier when setting the saving throw DC for a warlock spell you cast and when making an attack roll with one.

**Spell save DC** = 8 + your proficiency bonus + your Charisma modifier

**Spell attack modifier** = your proficiency bonus + your Charisma modifier

## Patron
### Hexblade's Curse
Starting at 1st level, you gain the ability to place a baleful curse on someone. As a bonus action, choose one creature you can see within 30 feet of you. The target is cursed for 1 minute. The curse ends early if the target dies, you die, or you are incapacitated. Until the curse ends, you gain the following benefits:

  You gain a bonus to damage rolls against the cursed target. The bonus equals your proficiency bonus.
  Any attack roll you make against the cursed target is a critical hit on a roll of 19 or 20 on the d20.
  If the cursed target dies, you regain hit points equal to your warlock level + your Charisma modifier (minimum of 1 hit point).

You can't use this feature again until you finish a short or long rest.

### Hex Warrior
At 1st level, you acquire the training necessary to effectively arm yourself for battle. You gain proficiency with medium armor, shields, and martial weapons.

The influence of your patron also allows you to mystically channel your will through a particular weapon. Whenever you finish a long rest, you can touch one weapon that you are proficient with and that lacks the two-handed property. When you attack with that weapon, you can use your Charisma modifier, instead of Strength or Dexterity, for the attack and damage rolls. This benefit lasts until you finish a long rest. If you later gain the Pact of the Blade feature, this benefit extends to every pact weapon you conjure with that feature, no matter the weapon's type.

## Pact Boon
* **Pact of the Chain** - You learn the _find familiar_ spell and can cast it as a ritual. The spell doesn't count against your number of spells known.

  When you cast the spell, you can choose one of the normal forms for your familiar or one of the following special forms: _imp, pseudodragon, quasit, or sprite_.

  Additionally, when you take the Attack action, you can forgo one of your own attacks to allow your familiar to use its reaction to make one attack of its own.

## Eldritch Invocations
* **Agonizing Blast** - When you cast _eldritch blast_, add your Charisma modifier to the damage it deals on a hit.
* **Investment of the Chain Master** - When you cast _find familiar_, you infuse the summoned familiar with a measure of your eldritch power, granting the creature the following benefits:

    The familiar gains either a flying speed or a swimming speed (your choice) of 40 feet.
    As a bonus action, you can command the familiar to take the Attack action.
    The familiar's weapon attacks are considered magical for the purpose of overcoming immunity and resistance to nonmagical attacks.
    If the familiar forces a creature to make a saving throw, it uses your spell save DC.
    When the familiar takes damage, you can use your reaction to grant it resistance against that damage.
* **Devil's Sight** - You can see normally in darkness, both magical and nonmagical, to a distance of 120 feet.
* **Eldritch Mind** - You have advantage on Constitution saving throws that you make to maintain your concentration on a spell.
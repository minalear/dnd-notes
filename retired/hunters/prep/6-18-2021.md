---
title: "6-18-2021"
draft: false
---

## PCs
* **Edric** - friends are dead or gone, is seeking for away to get home and check on his family.
* **Tyrus** - ship wrecked on the island and is looking for ways to escape the island.
* **Jessica** - Member of the Grimm Troupe, seeking to escape the island and her old life.
* **Killian** - kidnapped by the dragon cult for some reason and is looking to escape.

## Start
The party just proved to Icarium that they are worthy of taking the Ruin Stone from the tomb and he bestowed various gifts upon the party.

## Scenes
* **Tomb**
  - Icarium is willing to leave the tomb, but the defensive wards keeping him in place must be removed which will anger Black Magga into attacking the party.
  - Otherwise he will remain for all eternity until the world returns to Mother Night.
* **Lake + Lake Shore**
  - If the players leave the lake without engaging with Black Magga, she does not interact with them again until agitated.  The murlocs leave the players be, but will celebrate them if the players intend to interact with them.
  - Red Eyes exist throughout the island near the lake as a remnant of Icarium's creation.  He has various bodies and protects the woods from assailants.
* **Camp**
  - Ding will examine the Ruin Stone and tell the party it is a powerful, ancient relic of a bygone era that is a fragment of a greater power.
  - **Ruin Stone** - once per day, as a bonus action, the stone is able to undo a single deed that the user has performed.  The greater the deed, the greater the risk of it backfiring.
    - Reroll ability check - d20 check
    - Reroll saving throw - d12 check
    - Undo turn - d8
    On any natural one with these checks, a randomly chosen party member must make a DC 20 Con save or suffer 10d6+40 necrotic damage.  If the character dies, the crumbles to dust and can only be restored to life by means of a true resurrection or wish spell.

    The stone is capable of undoing greater effects as well, but at greater costs due to scope.  Effects are determined by the DM.
* 
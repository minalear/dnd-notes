---
title: "Dragon Cult"
draft: false
---

## Background
The dragon cult is headed by Liliana Highmore, an ancient Azlanti human who has obtained immortality eons ago by being fleshwarped into a five headed dragonoid.  She is referred to as **Tiamat** by the dragons as being *one of many*.

The cult's ultimate goal is the resurrection of **The Ur-Dragon**, a powerful dragon ancient from the dawn of time and father to all dragons.  He was sealed away by the Storm Giants during the early war of the ancients using a powerful runic seal that were split into shards and hidden away into tombs of heroes.

Today, the cult has found four of the five shards which are necessary for releasing the planar binding of The Ur-Dragon.  If he is released, his essence will combine with the body of the Scion, a dragon specifically born to become a vessel for the ancient.

## Hierarchy
1. Liliana - undisputed leader of the cult, united the disorganized cult years ago and has made them a force to be reckoned with.
2. Wyrmspeaker - five wyrmspeakers representing each chromatic color of dragon
    - Balthazar - Red wyrmspeaker (human wizard) - oversees archaeological digs
    - Raubon - Black wyrmspeaker (human knight) - oversees prisoner camp
    - Animo - Green wyrmspeaker (elf arcanist) - oversees research at library
    - Bruse - Blue wyrmspeaker (half-dragon warrior) - oversees external coordination with other cells
    - Papa - White wyrmspeaker (gnome artificer) - oversees security
3. Dragonsoul - lieutenants of a wyrmspeaker, uses dragon orbs associated with their color.
4. Dragonfang - underlings to dragonsouls
5. Dragonwing - first position of leadership in the cult, very brutal to achieve this rank
6. Dragonclaw - the lowest rank of the cult, who perform the bulk work for the organization.
7. Initiate - trainees dedicating months to years to achieve the rank of dragonclaw by being indentured servants.  When they pass their trials, they dedicate themselves to one of the dragon aspects.  Many do not survive their trials.

* Wearers of Purple - special rank given to members of local sects.  They act as the defacto leader of a local sect when not coordinating with the central sect.  All wyrmspeakers are wearers of purple as well as some lower ranking members.
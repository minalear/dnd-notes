---
title: "Story"
---
This is the canonical story of the Crescent Wayfairers after the end of the player campaign.

## Invasion of Winter
* Party leaves Dragon Island
* Return to Ondale to find a terrible blizzard has encompassed the region
* Turns out the blizzard has taken over the entire southern hemisphere
* Herald of the Ice King's invasion
* They invade the crystal palace and defeat the Ice King
* Party travels to the south pole, with the help of the orc clan, and defeat Anduril, the goddess of winter
* Party becomes two-star hunters

## Hunter Exam
* Mike Manifold asks party to be examiners at the next Hunter Exam
* Party helps thwart a coup planned by Israel Cons during the exam
* Mugol becomes a Hunter

## Dreamland Expeditions
* Party joins the Zodiacs, a special board of hunters serving the chairman of the Hunter Association
* Mike plans on leading an expedition deeper into the Dreamlands, something his forebearers refused to do
* The party meets several dreamland denizens
	* Storm Giants of Maothas
	* Celephais and the Dream Emperor

## Hellgate Disaster
* Party helps defeat a secret cabal seeking to reopen the Hellgate
* They were assisted by the paladin High Lord Boltar
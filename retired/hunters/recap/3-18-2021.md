---
title: "3-18-2021"
draft: false
---

Party was interrogated by the ship's captain about breaking into the assassin's room.  The party were left off easy due to favorable testimonies by Guseva and Michael Scott.  That same evening, while guarding Sergei at the ball, Jeremy made several business connections before the Grimm Troupe attacked.  The battle was hard fought with many allies falling.  Towards the end of the encounter after Grimm fled, dragons attacked the ship forcing it to crash.  Jeremy heroically sacrificed himself to save the party guests by distracting a dragon that landed at the party.  At the very end, the dragon burst through the dome and Michael Scott appeared to help try and hold it off.

---
title: "Setup"
---

## Concept
* Party will begin in the village of Tembley within the Barrowdun
* Game will start as a slice of life of village life where the players will participate in various village activities and meet the locals
  * Ideally someone from the party will be from the village, otherwise they would need good reasons to be here

## Players
* Ryan will be playing a Priest of Mithras
  * He finished his education in the Monastery of Saint Caelin and was assigned to the temple at Tembley
  * He will meet Father Hawthorn who has been tending to the temple and the people for the past seven years
* 
---
title: "Setup"
---

## Players
* Karrak Aldune (Dustin) - retired military pilot turned to the private sector, working for Rogier
* Rogier Van der Meer (Ryan) - pan-human psychic investigator looking for his first case
* Evelyn Krass (Courtney) - private security guard sent to pick up TREVOR for the Galos job
* EVA (Trevor) - android investigator who lost their human partner trying to get back into the game
* Hector Schmidt (Zack) - dejected medic fleeing a dark past seeking refuge in the fringe

## Alduin Station
* Party will begin at the Alduin Station on January 24, 3118
  * Ryan and Dustin have arrived on a whim as they travel the Orion Arm looking for something to investigate
  * Courtney and Trevor have arrived to meet up; Trevor was hired to investigate Argos' death and Courtney is here as the rendezvous
  * Zack has been here hiding out from the mafia, running a black market clinic

### Introductions
* Ryan and Dustin will exit the warp and arrive before the station and begin docking procedures
  * Introduce the station and some of the attractions
  * Guards will pick them for random search
* Trevor arrives, being hired by a Captain Gregor Kozlov on behalf of Larry Fitzgerald on Galos Prime
  * Is to meet here to be picked up by one of Kozlov's agents
* Courtney will enter the station and head to her designated rendezvous point with Trevor's character
  * Hired by Captain Gregor Kozlov as part of his private security group which is part of the Altruis Station
    * Doesn't trust sector security and prefers to hand pick his agents
  * Ryan's character will conveniently be in the scene when they discuss the investigation
  * Up to Ryan how to proceed, either talking with them directly or following them sneakily with their ship
  * Courtney will have a warp capable police cruiser on loan from Altruis
* Zack will be heading home from getting groceries where he'll run into Jaxx
  * Police are raiding his apartment looking for Zack
  * Jaxx assumes either Dr Reyes ratted him out or some dirty cops are trying to drag him back to the mafia

### Galos Prime
* Regardless of how everyone gets here, Galos Prime will allow the players to dock in the Central Port
* They'll be greeted by Larry Fitzgerald and given the rundown of the situation
* Proceed to Chapter-01
  
## Notes
* Courtney didn't have much to do with the Strix investigation
  * Knows that the Altruis Security, the sector security force that handles everything in Aldania, ruled the death as a suicide
  * The investigation was handled by Officer Breda, a meat head officer who lacks general finesse when it comes to operations
  * Courtney's Captain is Cpt Fulmer and is okay, prefers to protect the interests of the people of Altruis over everyone else in Aldania
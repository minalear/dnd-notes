---
title: "Chapter-00"
---

* Rogier runs into and subsequently kidnaps Schmidt and takes him aboard the Agent of Inquiry after overhearing the job between EVA and Evelyn
* They follow them to a Motel 6 where they proceed to introduce themselves and get the cops called on them
* Party kidnaps EVA and Evelyn while in the process of disarming cop and destroying his vehicle
* Party proceeds to Aldania where Captain Kozlov notices Evelyn is not returning with his loaned car
* Tells them to proceed to Galos Prime and that he signed them off from checking with customs
* Party arrives in orbit of Galos Prime
---
title: "Setup"
---

> Galos Prime, the thirteenth colony established by the Marlowe Corporation and site of the final battle of the Interplanetary Wars, now a backwater agri-world with nothing more to its name than a half forgotten legacy and corn.
>
> The planetary governor, Argos Strix, was found dead in his office by his secretary, Mina Thomas, three weeks ago.
>
> Sector security and OGR have written off the death, claiming it to be death by suicide.  However, interim governor and Chief of Police, Larry Fitzgerald, believes Argos was murdered and has hired a third party investigator to look into it.
>
> &mdash; Intro to *Space Columbo*

## Galos Prime
Game begins with the party traveling to Galos Prime, a backwater agri-world found on the fringe of UAP controlled space.  It was one of the colonies founded before the discovery of Hyperlanes, having been established by a traditional colony ship launched by the Marlowe Corporation.  The colony collapsed during the "Two Hundred Year Silence" with barely anyone of the original colony surviving.  When the Interplanetary Wars broke out, Galos Prime was conquered by the Warlord Abram Don Lorn.  It was here where Lorn made his final stand against the forces of Earth and perished.

Since then, Galos Prime has been reinvigorated, having been reestablished as an independent charter colony under the OrionTec banner (a subsidiary of Marlowe Corporation).  It was designated as an agriculture site and has since been terraformed into a more suitable climate for growing crops.

---
title: "Ideas"
---

* Galos Prime resides within the Aldania Sector
* UAP is structured into sectors, systems, and then planets
* Active research into Hyperlanes are underway in Aldania, hoping to unlock a new Rush beyond the fringe

* Hacker is an avid "damage" player, people who play a banned card game in dangerous scenarios
  * Could be found playing in an orbital that is due to detonation

## ChatGPT Notes

Sure, I'd be happy to help you come up with the reason why the governor was murdered and some clues for your players. Here are a few ideas:

Reason for the Murder:

* The governor discovered evidence of illegal activity by the Smith-Shimano corporation, and was planning to blow the whistle on them. The corporation, fearing exposure, hired an assassin to eliminate the governor before he could go public.
* The governor had uncovered evidence of a conspiracy involving several prominent figures in the government and the corporate sector, and was planning to expose them. Those involved in the conspiracy hired someone to kill the governor to prevent him from going public.
* The governor was secretly working on a revolutionary new technology that would have made traditional agriculture obsolete. Those who stood to lose financially from the development of this technology hired an assassin to eliminate the governor before he could share his ideas with others.
* The governor was involved in a romantic relationship with someone who was not well-liked by the local community. When this relationship was discovered, the person's angry family members hired an assassin to kill the governor in retaliation.

Clues for the Players:

* A hidden compartment is found in the governor's office containing documents related to the illegal activities of the Smith-Shimano corporation.
* The governor's personal computer contains encrypted files that only someone with engineering skills, like the governor himself, would be able to decipher.
* A witness comes forward, claiming to have seen a suspicious individual lurking around the governor's estate on the night of the murder.
* A set of blueprints for a revolutionary new agricultural technology are found hidden in the governor's private lab, suggesting that someone may have been trying to steal or destroy them.
* The governor's autopsy reveals that he had ingested a lethal dose of poison, which could have only been administered through his food or drink. The players must investigate the kitchen and food supply chain to determine where the poison came from.
* The players discover that the governor's personal bodyguard, who was supposed to be on duty the night of the murder, was absent from his post for several hours. They must investigate where the bodyguard was and what he was doing during that time.

---

* The Central Market: A bustling hub of commerce where farmers and merchants come to sell their wares. The market is a vibrant mix of colors and smells, with vendors hawking everything from fresh produce to handcrafted tools.
* The Hydroponics Farm: One of the largest hydroponic facilities on the planet, this farm produces crops year-round using nutrient-rich water instead of soil. The facility is a marvel of engineering, with rows upon rows of plants growing under bright, artificial lights.
* The Solar Array: Located on the outskirts of the city, this massive solar array provides power to the entire planet. The array is made up of thousands of solar panels that stretch out as far as the eye can see.
* The Water Treatment Plant: The water treatment plant is responsible for purifying and distributing the planet's water supply. The facility is heavily guarded, as it is a prime target for sabotage.
* The Orbital Dockyard: The dockyard is a massive space station that orbits Argos Prime. It serves as a hub for interstellar trade and travel, as well as a staging ground for military operations.
* The Mining Outpost: Located in the nearby hills, this mining outpost is responsible for extracting valuable minerals from the planet's crust. The outpost is a self-contained facility, with its own living quarters, food supply, and medical facilities.
* The Agricultural Research Facility: This facility is dedicated to developing new strains of crops that are more resistant to disease and pests. The facility is heavily guarded, as many of the experiments conducted here are considered top-secret.
* The Medical Center: The medical center is the largest hospital on the planet, equipped with state-of-the-art technology and staffed by some of the best doctors and nurses in the galaxy.
* The Space Elevator: The space elevator is a towering structure that extends into orbit. It is used to transport people and cargo to and from the orbital dockyard.
* The Recycling Plant: This plant is responsible for recycling and repurposing waste materials from across the planet. It is a key component of the planet's sustainability efforts, and is highly regarded by environmentalists throughout the galaxy.

---

I like the idea of the bodyguard being absent for many hours.  Would you help me come up with why he was missing and why he would want to hide it?

---

Certainly! Here are a few possible reasons why the bodyguard was absent for several hours and why he might want to hide it:

* The bodyguard was secretly in a relationship with someone, and they had planned to meet on the night of the murder. However, when the bodyguard arrived at the designated location, he found that his lover had been killed. The bodyguard panicked and fled the scene, fearing that he would be blamed for the murder.
* The bodyguard was in financial trouble and had agreed to help someone rob the governor's estate in exchange for a cut of the profits. However, the robbery went wrong, and the governor was killed in the chaos. The bodyguard fled the scene to avoid being caught and implicated in the crime.
* The bodyguard was contacted by someone who claimed to have information about a threat to the governor's life, and was instructed to meet them at a remote location to receive this information. However, when the bodyguard arrived, he was ambushed by the assassin who had been hired to kill the governor. The bodyguard narrowly escaped with his life and fled the scene, but has been too afraid to come forward with what he knows.
* The bodyguard had been secretly investigating the governor's personal life, suspecting that he was involved in something shady. On the night of the murder, the bodyguard received a tip that the governor was planning to meet with a dangerous individual. The bodyguard followed the governor to the meeting place, but was spotted and forced to flee. He has been too afraid to come forward with what he knows, as he fears for his own safety.
* The bodyguard had received a distress signal from someone on the planet, and had left his post to investigate. When he arrived at the location of the distress signal, he found that it was a trap, and was ambushed by the assassin who had been hired to kill the governor. The bodyguard narrowly escaped with his life and fled the scene, but has been too afraid to come forward with what he knows.

---

The machine intelligence that created the Orion Arm is actually a group of post-human beings who have transcended their physical bodies and now exist as pure energy. These beings have taken on the role of "gods" to humanity, and have created the Orion Arm as a kind of paradise for their followers. The post-human beings have the power to manipulate reality itself, and can create any kind of environment or experience they desire. However, there are dark forces at work within this post-human society, and some believe that the paradise they have created is actually a prison.

Sure, here are some xeno-sounding names for the ancient species that transcended their physical bodies:

* Xal'mor
* Zyrk'thul
* Vyl'kara
* Keth'razi
* Qu'linar
* Zeph'yris
* Azur'eth
* Ty'shaara
* Syr'vani

---

* The black body entity could be a singularity, a region of space where the laws of physics break down and time and space become warped. It could be used to create a new type of propulsion system that allows ships to travel faster than the speed of light, making Smith-Shimano the dominant force in interstellar travel.
* Alternatively, the black body entity could be a source of immense energy, capable of powering entire star systems. Smith-Shimano could use this energy to fuel their operations, making them one of the wealthiest and most powerful corporations in the galaxy.
* The entity could also be a gateway to another dimension or universe, allowing Smith-Shimano to access technology and resources that are otherwise impossible to obtain. This could give them an enormous advantage over their competitors and even the A'kara.
* However, the entity could also be incredibly dangerous, perhaps even sentient and malevolent. It could be a remnant of a long-dead civilization that was destroyed by its own technology, or a weapon created by an ancient and powerful alien race. Smith-Shimano's attempts to harness its power could have unintended and catastrophic consequences, unleashing an unimaginable horror on the galaxy.

As for how this could be related to the murder of Governor Argos, perhaps he stumbled upon evidence of Smith-Shimano's activities and was planning to expose them, making him a liability that needed to be eliminated. Or maybe he discovered something about the true nature of the black body entity that made him a threat to Smith-Shimano's plans. The players could uncover clues that lead them closer to the truth, putting them in danger from the corporation's agents who will stop at nothing to keep their secrets safe.

--- 

* Lena Reyes: The chief of security on Galos Prime. She was in charge of protecting Governor Argos, and may have valuable information about any security breaches or suspicious activity leading up to his death.
* Dr. Erika Zhang: A scientist specializing in alien biology and xenobiology. She may have insight into any unusual substances found in the governor's body or on the crime scene that could be related to the murder.
* Captain Gregor Kozlov: The captain of the Aldania Sector's spaceport. He may have information about any unusual arrivals or departures around the time of the murder.
* Nadirah Al-Farouk: The owner of a popular local restaurant. She may have seen or heard something that could be relevant to the case, as her establishment is known for attracting a diverse clientele.
* Jensen Novak: A private investigator who has been investigating the governor's death on behalf of the governor's family. He may have information that the players don't, but could be convinced to work together with them.
* Mikhail Petrov: A wealthy industrialist with a large presence in the Aldania Sector. He may have connections or information that could be useful in the investigation, but could also have his own motivations for getting involved.
* Ambassador Tarek al-Saqqaf: The ambassador from the neighboring star system. He may have information about any tensions or conflicts between Galos Prime and their neighbors that could be relevant to the investigation.

These are just a few ideas to get started, but feel free to add, modify, or remove NPCs as you see fit for your campaign.

---

Lene Reyes is a middle-aged woman of average height and build. Her skin is a deep brown, and her hair is thick, black, and worn in tight braids that fall to her shoulders. Her dark eyes are sharp and intelligent, and she has a no-nonsense demeanor that suggests she's not one to suffer fools. She wears simple, utilitarian clothing in muted earth tones, and carries herself with a sense of purpose and authority.

Will you give me the physical description of Dr Erika Zhang?

Dr. Erika Zhang is a middle-aged pan-human of Han Chinese descent. She has short, straight black hair that she often wears in a bun. Her face is angular with high cheekbones and a narrow jawline, giving her an intense and serious look. She has dark brown eyes that seem to miss nothing and are always observing her surroundings. Her skin is smooth and fair, but with a slight yellowish hue that suggests she spends a lot of time indoors. She is of average height and build, with a lean and wiry frame that suggests she keeps herself in good physical shape. She wears utilitarian clothing, favoring jumpsuits and lab coats over more fashionable attire. Overall, she gives off an impression of competence and efficiency, with a no-nonsense attitude that commands respect.

What kind of pan-human is Erika Zhang?

Dr. Erika Zhang is a pan-human with genetic modifications that give her enhanced cognitive abilities, such as faster information processing and memory recall. She is part of the "Mentats" pan-human group, which has been genetically engineered to excel in mental tasks and are highly sought after for their intellectual abilities.

Will you give me the physical description of Captain Gregor Kozlov?

Captain Gregor Kozlov is a tall and muscular man, standing at 6'5" with broad shoulders and a barrel chest. He has a square jawline and sharp cheekbones, with short-cropped blond hair and piercing blue eyes. He has a few scars on his face, a remnant of his years serving in the military. His voice is deep and commanding, and he carries himself with an air of confidence and authority. He wears his navy blue uniform with pride and always has a holstered laser pistol at his side.
trevor.k.fisher@gmail.com
Will you give me the physical descriptions of Nadirah Al-Farouk, Jensen Novak, Mikhail Petrov, and Ambassador Terek al-Saqqaf?

Certainly, here are the physical descriptions of the four NPCs:

Nadirah Al-Farouk: A tall and imposing woman of Arabian descent with piercing green eyes and long black hair that she wears in a tight braid. She has a muscular build and moves with a controlled grace that speaks of years of martial training. She wears traditional Arabian clothing, but with the addition of a high-tech combat vest.

Jensen Novak: A stocky and heavily-muscled man with a rugged, weather-beaten face and short-cropped hair. He has a distinctive scar above his left eyebrow and a thick beard that covers most of his face. He wears a battered leather jacket and carries a large revolver at his hip.

Mikhail Petrov: A tall and lean man with a shaved head and sharp, angular features. He has piercing blue eyes and a cool, detached manner that can be off-putting to some. He wears a form-fitting black jumpsuit that accentuates his athletic build and carries a sleek, high-tech assault rifle.

Ambassador Terek al-Saqqaf: A regal and dignified man of Middle Eastern descent with a neatly trimmed beard and piercing brown eyes. He wears traditional Arabian robes of the finest silk and carries himself with a refined grace that suggests a lifetime of diplomatic training. Despite his elegant appearance, there is a steely determination in his gaze that suggests he is not to be underestimated.

---

Minerva's Den was originally founded as a mining station in the early days of the Orion Arm settlement. However, as the minerals started to run out, the station was gradually abandoned and eventually became a haven for outlaws and other criminals. Over time, the various factions and organizations on Minerva's Den started to consolidate their power and influence, forming a loose but powerful criminal network that controls the station and the surrounding system.

One of the most powerful figures on Minerva's Den is a notorious crime boss named "The Spider," who is known for his ruthless tactics and cunning intelligence. He controls a vast criminal empire that spans the entire sector, and his agents and enforcers can be found on almost every planet and station in the area.

Another influential figure on Minerva's Den is a charismatic smuggler named Jaxen Sol, who runs a popular bar and nightclub on the station. Sol is known for his charm and his ability to make deals with almost anyone, and he has a reputation for being a master of disguise and subterfuge.

Other important figures on Minerva's Den include the heads of various criminal organizations, such as the Brotherhood of Shadows (a group of skilled assassins), the Crimson Hand (a gang of cybernetically-enhanced thieves), and the Black Suns (a powerful mercenary group).

Despite the station's reputation as a haven for criminals and outlaws, there are some who see it as a place of opportunity and freedom. Many independent traders and entrepreneurs have set up shop on Minerva's Den, taking advantage of the lack of regulation and the abundance of willing customers.

However, life on Minerva's Den is not without its dangers. The station is constantly under threat from rival criminal factions and law enforcement agencies, and violence and betrayal are common occurrences. Those who dare to challenge the power of The Spider or his allies often end up dead or worse.

Some possible locations on the planet Minerva's Den could include:

* The Veil: This is the main attraction on the planet, a sprawling entertainment complex that includes bars, restaurants, casinos, and dance floors. It's always packed with people, and many of the galaxy's most notorious criminals and outlaws can be found here.
* The Bazaar: This is an open-air market that operates on the outskirts of The Club. Here, vendors sell all manner of goods, from weapons and illegal drugs to exotic pets and rare artifacts.
* The Docks: This is the spaceport where ships arrive and depart from the planet. It's a chaotic and dangerous place, as many of the ships that dock here are involved in illegal activities.
* The Red Light District: This is an area of the planet where prostitution and other illegal activities are rampant. It's a dangerous place to be, but it's also where many criminals go to blow off steam and indulge their vices.
* The Slums: This is a run-down, impoverished area of the planet where the poorest inhabitants of Minerva's Den live. It's a dangerous place to be, as crime and violence are rampant here.
* The Black Market: This is an underground marketplace where all sorts of illegal goods and services are bought and sold. It's a dangerous place to be, but it's also where many criminals go to acquire the tools they need to carry out their operations.
* The Pit - A seedy, underground fight club where anything goes.
* The Hive - A cluster of illegal cybernetic clinics and chop-shops.
* The Red Star - A casino that caters to the wealthy elite.
* The Last Stand - A bar where the toughest and most dangerous criminals go to unwind.
* The Labyrinth - A maze-like network of alleyways and abandoned buildings, used by smugglers to move contraband in and out of the Den.
* The Black Market - A sprawling marketplace where anything and everything can be bought or sold.
* The Crypt - A series of catacombs beneath the city, rumored to be haunted by the ghosts of those who died in the Den's early days.
* The Coliseum - An arena where gladiatorial combat is held for the entertainment of the Den's denizens.
* The Velvet Room - An exclusive, members-only lounge where the wealthiest and most influential individuals in the Den gather to discuss business.
* The Dead Zone - A section of the city that has been abandoned and sealed off due to dangerous levels of radiation.

---

Sure, here are some possible individuals you might find around Minerva's Den:

* Zara "The Blade" Tariq - a notorious pirate captain with a reputation for ruthlessness and cunning.
* Dr. Kaela Rallis - a brilliant scientist specializing in cybernetic implants, rumored to be conducting secret experiments on unwilling test subjects.
* Luka "The Viper" Kovac - a master thief and skilled assassin, known for his love of danger and his talent for escaping impossible situations.
* Captain Suleiman al-Nasser - a former UAP naval officer turned privateer, who now commands a small fleet of ships that prey on UAP merchant vessels.
* Xander "The Rat" Rodriguez - a scrappy street urchin who grew up on the mean streets of Minerva's Den and has since made a name for himself as a skilled pickpocket and smuggler.
* "Mad" Margo DuPont - a notorious arms dealer with a fiery temper and a love of explosives.
* Solon "The Oracle" Panopoulos - a mysterious figure who claims to be able to see the future, and has built a small cult of followers who believe in his powers.
* "Blackjack" Jackson - a charismatic gambler and con artist who always seems to be one step ahead of the game.
* Tamara "The Spider" Sato - a skilled hacker and information broker who knows everyone and everything in Minerva's Den.
* Captain Alistair McRae - a grizzled old space dog who has seen it all and done it all, and now spends his days spinning tall tales at the local spaceport bar.

---

* A disagreement over the allocation of resources or territory, leading to tensions between member species of the Avaraxian Alliance. This could be especially volatile if there are limited resources or planets that are considered highly valuable.
* A disagreement over the handling of a potentially dangerous alien species, such as the Ferelian Swarm we discussed earlier. Some members of the alliance may want to eradicate the species, while others may argue for preservation or study.
* A division between member species based on their level of technological advancement. Some members may feel that others are lagging behind, or that they are being left behind by the more advanced species. This could lead to feelings of resentment and conflict.
* A dispute over the leadership or governing structure of the Avaraxian Alliance itself. Some member species may feel that they are not being represented fairly, or that certain species hold too much power.

These are just a few potential issues that could lead to a political crisis within the Avaraxian Alliance. Depending on the specifics of the conflict, it could lead to anything from minor disagreements to all-out war between member species. The resolution of the crisis could require diplomacy, compromise, and potentially even a restructuring of the alliance's governing structure.

--- 

There exists a rare and mysterious substance in the galaxy that is said to have reality-bending properties. It is so rare and dangerous that it is strictly prohibited to possess or use, but rumors persist of smugglers and criminal organizations trafficking it on the black market. The substance could be a valuable plot point for quests or campaigns, as characters may be tasked with tracking down its source or preventing its use for nefarious purposes.

The resource could be a rare and highly coveted type of energy that is only found within the remnants of supernovas. It could be a form of exotic matter or energy that has unique properties and is able to power advanced technology and enable faster-than-light travel. The Dravar could have an advantage in mining this resource due to their specialized knowledge and equipment.

However, the discovery of this resource could also lead to conflict and tension between different factions in the galaxy, as everyone seeks to control it for their own purposes. It could become a valuable bargaining chip in political negotiations, or even the cause of full-scale interstellar war. The Avaraxian Alliance may have to intervene to prevent the situation from escalating out of control and ensure that the resource is used for the benefit of all civilizations in the galaxy.

How about "Novium" as a name for this substance? It's derived from "nova," which is related to supernovas, and it has a futuristic-sounding ring to it.

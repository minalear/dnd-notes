---
title: "Death of Strix"
---












## Death of Argos Strix
* Argos Strix was a former starship engineer working for the Smith-Shimano Corporation and designed the Horizon-class line of freighters.
* He was laid off during some corporate restructuring and ended up taking some data files along with him.
* Some of these data files happened to contain top secret notes on a project currently being worked on by SSC called the Excession; a research project into a mysterious black body entity that they are keeping a secret from UAP authorities.
* Galos Prime is facing financial hardships with the planet barely producing enough revenue to pay workers, let alone maintain equipment and infrastructure.
* Argos, going through old data tapes discovers the notes on the Excession and attempts to sell these to a rival corporation, partly for revenge and partly to try and help his planet.
* SSC cannot sue Argos to keep these secrets due to the nature of the program so they hire an assassin to take him out instead, framed to look like suicide.
* The assassin hired is named Vega and is known for using remote killing techniques by hijacking target's brainwaves and inducing strong suicidal desires within them.
* Vega utilized some communications arrays located planetside that are used to coordinate autonomous farming vehicles and uses it to attack Argos.
* These readings were noticed by local technicians but were disregarded as some form of atmospheric noise which is not uncommon.







* Players will arrive in Galos Prime and meet with interim planetary governor Larry Fitzgerald.
* They are here to investigate the supposed murder of Argos Strix, though most of the populace believes he died of natural causes.
* There will be a few different locations the players can investigate to seek out clues; namely Argos's office, the morgue, Argos's villa
* Notable NPCs; Argos's secretary (Mina Thomas), Argos's wife (MG-86), Larry Fitzgerald, Yan (interim chief of police), the mortician

## Death
* Argos was found with a revolver and a blast to his head, presumed suicide
* Sector security investigators from Ajax and the sector OGR office ruled the death officially as suicide
  * The jurisdiction for cases of this level fall to sector security rather than local police forces
  * The sector investigator is a Nilgobian known as Ferg
  * The OGR agent was a pan-human known as Lucius Eltrix
    * It is later revealed that Lucius was an accomplice to hide evidence, working alongside Vega who was an old friend.
    * Lucius was extorted into helping as he owed Vegas several favors for hiding his substance abuse problems from OGR leadership

## Argos's Office
* Argos's Office is located in the planetary office campus, a series of small buildings that make up most of the planetary bureaucracy and manages interplanetary affairs for the local populace.

### List of Clues
* Argos's computer is encrypted and locked behind a password.  Neither the police or the OGR scanned the computer for any details
  * Once decrypted, most of the files on his computer are related to work; emails between sector officials and prominent local citizens
  * In the trashcan of his email can be found an email from Argos's old boss from SSC extending him an offer to return to work (Argos declines) and then a follow up email asking for 
* Argos has a book on starship engineering that has a note from his professor from an engineering school on Terra Nova; Professor's name is Melanius Bogart
* Many pictures show him with locals, being happy and supportive, as well as his wife and co-workers
* The pistol is an older model Novax gun (NVL-77) that is no longer in production; Argos was gifted it by his dad when he graduated university
  * This gun is considered "dumb" as it has no logic modules and cannot be remotely controlled, relying on manual operation from the user

## Morgue
* Currently, Argos's body is kept in cold storage waiting for the private investigation
* MG is upset as this is delaying his burial and she wishes for him to be put to rest
* The mortician is a pan-human named 

### List of Clues
---
title: "Setup"
---

## Players
* Karrak Aldune (Dustin) - retired military pilot turned to the private sector, working for Rogier
* Rogier Van der Meer (Ryan) - pan-human psychic investigator looking for his first case
* Evelyn Krass  (Courtney) - private security guard sent to pick up TREVOR for the Galos job
* EVA (Trevor) - android investigator who lost their human partner trying to get back into the game
* Hector Schmidt (Zack) - dejected medic fleeing a dark past seeking refuge in the fringe

## Galos Prime
* Small, backwater, agricultural world that feeds the Aldania Sector
* Planetary governor, Argos Strix, was found dead in his office on January 10, 3118
* Players arrive on February 9, 3118
* Larry will assign Officer Kimball Bryant to assist the party while he goes off to finish paperwork

### Larry Fitzgerald // Interim Governor and Chief of Security
* Loyal to a fault and proud of his security forces
* Suspicion could be thrown on him from the outset with him not willing to answer many questions
  * Will just be busy trying to coordinate the momentous task of running a global agricultural trade network

### Argos Strix // Planetary Governor
* Found dead on the morning of January 10, 3118 by secretary Mina Thomas
* Death by self inflicted gunshot wound
  * Revolver was a collectable antique that was awarded to Argos after 20 years working for SSC
* Cause of death was confirmed by local coroner Dr. Erika Zhang
* Sector security handled investigation, lead by Detective Breda, who ruled the death a suicide after about an hour.
* Local OGR office refused to do a further investigation
* Larry pushed for a private investigator and kept Strix's body on ice, much to the displeasure of MG-86
  * Hired EVA off the Omninet, hoping she could find answers to Strix's death
* Argos Strix has been staying late due to the harvest season to finish paperwork

### Strix's Office
* Strix's office is pretty basic with some furniture and decorations
* Personal computer has some encrypted records that are inaccessible without the password
  * Locked behind advanced cryptography; cannot be brute forced
    * Any attempts to brute force the password results in unrecoverable data deletion by the security system
    * Limited to 10 attempts
  * The password is "PerAsperaAdAstra"
    * Reference to the latin phrase his professor wrote on a personalized note given to Strix after graduating
    * Means "through hardships to the stars"
* Bookshelf contains a number of books related to engineering, agriculture, and other hobbies of Strix
  * A keen eye can notice a piece of paper sticking out of an old engineering textbook
  * Note is from Melanius Bogart, Strix's professor and friend from the Stellaris Institute

### Jensen Novak // OGR Investigator
* Jensen Novak is an OGR investigator who suspects foul play with the death of Strix
  * Assumes that OGR declined to investigate due to being involved with the murder somehow
  * Knows that remote killing is possible though requires the skill of an exceptional assassin
* Jensen can be first encountered leaving the office of Erika Zhang as the party arrives
  * Will not stop to interact, maybe a quick hello, before entering a personal hovercar
  * Zhang will refuse to elaborate if questions about Novak
    * Will lie about him either being a client or former professional acquaintance
  * The two would have worked together at Dristmak where Novak would bring her bodies to examine
* Jensen is a skilled OGR operative and will notice most attempts to tail him
* Is currently staying at an apartment complex on the edge of town
* Is equipped with a personal field generator and a laspistol

### Erika Zhang // Coroner's Office
* Erika is a no nonsense coroner who is straight to the point with questioning
* Strix's murder was caused by the revolver; a .357 magnum
* Only a small amount of alcohol was present in his system but no other chemicals
  * Strix was known to be a light drinker, nothing new
* Due to the lack of tools available to Zhang, she cannot perform any advanced post-mortem psychological scans and OGR denied her request to lease their equipment
* The body is currently in cold storage in the back of the office
  * Players could potentially take the body for more advanced scans if they can come up with a good plan
  * OGR has a remote station on Gelida in the Enius system where they store some equipment that the players could potentially break into to get
* If the players are able to get advanced scans done, it will reveal a sudden spike of anxiety one hour before death; otherwise Strix is shown to be calm and collected

### Nadirah Al-Farouk // Technician's Office
* Nadirah is a hard working individual responsible for maintaining the control network in Central City and through the various control hubs across Galos Prime
  * This control network is used to coordinate the vast fleet of automated farming vehicles planetwide
* Vega used this communications array to broadcast his kill signal
* Nadirah noticed this disturbance the following morning but didn't put the pieces together and wrote it off as atmospheric interference
* Broadcast can be traced to a phased telecom array in orbit around Galos Prime where Vega hijacked into

### Captain Gregor Kozlov // Port Captain
* Retired captain, now runs Altruis Station and oversees the traffic coming in and out of the sector
* Knows Courtney and will be happy to help with the investigation
* Will report that only one unchecked vehicle passed through the sector around the date of the murder
  * Unchecked vehicles are normal, as they are usually tied to UAP business
  * Requires a ranking officer's signature but this vehicle's signer has been redacted; usually pointing to some OGR business

### Lucius Eltrix // OGR Operative
* Operative who is in charge of the OGR branch in Aldania
* Declined to investigate Strix's death due to knowing Vega was involved
  * Vega has severe dirt over Lucius from his OGR days
* Assisted Vega, allowing him to get through customs unchecked
* When the players have reached a crux in the investigation pointing to OGR involvement; Lucius will flee for Minerva's Den
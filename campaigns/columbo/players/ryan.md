---
title: "Ryan"
---

## Basic Ideas 

* The family's name is Manderville, and they made their fortune through various industries such as finance, entertainment, and real estate.
* The Mandervilles are a very old and established family, tracing their roots back to the early days of space colonization.
* The family's matriarch, Cassandra Manderville, is a legendary figure in the world of high-stakes gambling. She founded the family's flagship casino, the **Starlight Lounge**, and is known for her uncanny ability to predict the outcome of any game of chance.
* The Mandervilles are also patrons of the arts, and own one of the largest private collections of classic literature, music, and fine art in the galaxy.
* The family's orbital, known as the **Golden Saucer**, is a marvel of engineering and architecture, with sleek modernist design and all the amenities of a luxury resort.
  * It is one of the largest and most luxurious in the galaxy. It is home to numerous casinos, hotels, and entertainment venues, as well as a thriving community of residents and employees.
* Despite their wealth and power, the Mandervilles are not universally beloved. They have been accused of using their influence to manipulate markets and governments, and rumors persist of shady dealings and unsavory alliances. Nonetheless, they remain a force to be reckoned with in the galaxy.
* Purple eyes

### Family

* **Cassandra Manderville**: Matriarch of the family and the grandmother of RYAN. Has a reputation for being both charming and ruthless and manages most of the financial aspects of the family.
* **Julian Manderville**: Father of RYAN and founder of Manderville Industries, a highly successful corporation involved in the development of advanced medical technologies.
* **Victoria Manderville**: Julian's wife and a prominent philanthropist. She is known for her generous donations to various charitable causes.
* **Alexander Manderville**: The eldest son of Julian and Victoria. He is the CEO of Manderville Industries and a brilliant scientist in his own right.
* **Olivia Manderville**: RYAN's younger sister and a highly skilled hacker. She is often called upon to handle the family's cyber security needs.
* **William Manderville**: RYAN's older brother and a successful real estate developer. He is known for his extravagant lifestyle and his love of fast cars and expensive yachts.

Some unique details about the Manderville family include:

* Julian Manderville is a celebrated figure in the medical community for his groundbreaking work in developing advanced prosthetics and life extension technologies. He has won numerous awards and accolades for his work.
* Alexander Manderville is widely regarded as a genius in the fields of robotics and artificial intelligence. He is the mastermind behind many of the cutting-edge medical technologies developed by Manderville Industries.
* Olivia Manderville is a skilled hacker who has been recruited by various government agencies and corporations for her expertise. She is known for her rebellious streak and her tendency to push boundaries.
* William Manderville is a notorious playboy who has been married multiple times and has a reputation for his wild parties and extravagant spending.
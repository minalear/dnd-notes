---
title: "Timeline"
---

## 470 CE
### January
* 3rd - Harith's prison boat is destroyed off the coast of Viland and swims to the mainland
* 4th - Party investigates Hunters of Fox Reach and tracks down Nakai
  * They defeat the hunters who are revealed to be werewolves
* 5th - They follow Nakai arrive at the Quetza dig site where they help defeat the Relic Priest Lord Kakmuzo
  * Harith dies
  * Zeltzin dies
* 7th - Nakai leaves for Itza and takes Jatziri with him
  * Fox Reach is destroyed by Warrior of Xah En
  * Qiric is rescued by Lil Germo and Big Rico
* 8th - Party leaves for the Lyceum to seek information on secrets found in Quetza
* 20th - Party arrives at the Lyceum and begins researching Quetza and the Sekar
* 21st - Mirai replaces Qiric in the party
* 22nd - Mirai reveals the existence of the Sekar Whispering Way to Lord Krakdimoxtal, sparking an international incident and potential invasion of Seraphon into Viland
* 23rd - Party leaves on the Rosy Red Ferdinand
* 24th - Nerdo and Geeko flee into the night with Vega, leaving behind a dead captain and a sabotaged boat
  * Screaming Eagles encounter the party and come to an agreement
* 25th - Mirai impersonates Captain Brodius in a game of Damage in Corizon del Mar
  * Screaming Eagles drop party off near Fox Reach
  * Party examines the ruins of the church in Fox Reach and meet Fleek
* 26th - Party leaves for Whitham
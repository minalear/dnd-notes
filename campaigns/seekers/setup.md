---
title: "Setup"
---

# Setup
* One player will be aboard a small, rickety scooner on its way to Whitham, a northern port town in Veryn.
* During the voyage, the boat will be destroyed by a wandering Nakai, a massive white humanoid lizard that has been making his way towards the Storval Plateau from Aurios.
* The player will wash ashore near the small town of Fox Reach where they'll meet Qiric, a wandering gleaner on the look for Nakai.


---
title: "Ideas"
---

## Fronts
Some ideas for fronts that could be major motivations for arcs in the campaign:

### Major Fronts
* Warriors of Xah En
  * Celestial bodies could be aligning once more allowing portals to open onto Menadi from Xah En
  * These powerful warriors may start causing havoc, attempting to trigger another cataclysmic event that would separate the worlds again
  * A sympathetic villain reminiscent of the Ascians who only wish to restore their world
* Razmir and his Seekers
  * Razmir is seeking the Ruin Stone (or some equivalent artifact) within the bowls of the Lyceum
  * He may not know of its location yet, but could play a major role with the party if they become intertwined with the school
  * Could be a good excuse for the party to become higher ranking gleaners and access more top secret information that the school has under lock and key
* Race for the Relic
  * Some mystical object crashes into the hinterlands of Viland and several factions begin a race to retrieve it
* Dream Summoners
  * A cult that uses the nightmares of innocents to summon devistating monsters and gods, all pulled from the realm of Dreams
* Huani was captured and tortured by the Sekari, using her connection to the webway to empower their own empire
  * The Lost City of Chetzen lies within the Stagwood where Huani remains
  * Currently being experimented on by Conference S, the modern incarnation of the Strix Academy

### Minor Fronts
* Qiric, the Gathlain, may be the target of poachers wishing to sell him to unethical alchemists
* Regis Valter, of the noble Valter family, is looking to sponsor an adventuring group, mostly because he wishes to use his family's money for good

## Random Ideas
* Fungal blooms start to spred from a large woods in Viland, threatening the return of the ancient fungal threat.
  * Consumes all life and produces funganoid knights to defend its spread
* Fassbender is a old wizard who researches simalacrum and needs resources for his studies
  * An old Sikar Imperium mage's sanctum has a magical mirror that creates simulacrum of those it views

## Nakai
* Seeking a lost city of the Seraphon, deep within the forests of Viland
* Crossed the ocean from Aurios and has been spotted during the crossing
* Gleaners have been dispatched to seek him and determine what he's up to and to capture him if needed

## The Lodge
* Most of the old hunters are secretly werewolves, given the powers of the lycanthrope by the werewolf spirit Loper
* Loper is the ghost of the founder of the Lodge, Eustace Helsh, and the father of the current leader of the Lodge, Terrance Helsh
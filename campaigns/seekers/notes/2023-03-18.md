---
title: "2023-03-18"
---

* Party found the Stellarium and marked the 10 locations of the Attunement Nexus
  * They also noted the four "active" ones
  * They are unsure what they mean or what they point to
  * One clue they have is the 8 different skies representing the 8 different worlds being pulled into Menadi
* Party found the desiccated head of Lord Cualli (Eagle Lord's) son Xinoch
  * Spirit of Xinoch helped Zeltzin fight Kakmuzo later
* Party stumbled upon Nakai and found their way to Lord Kakmuzo's chamber
  * Kakmuzo revived himself by killing the fourteen archaeologists that stumbled upon his chamber
  * Upon his revival, he cast the whole Temple back into a nightmare realm
* During the fight, Kakmuzo blasted the party with various spells
  * A powerful blast of ice killed both Zeltzin and Jatziri, but Zeltzin used his fate point to protect Jatziri
  * A circle of death killed Aella, Qiric, and Harith
    * Aella used her fate point to save herself while Starkward used his to save Qiric
    * Qiric used his fate point to save his bees and flower
  * The fight was ended by killing the fourteen archaeologist that were empowering Kakmuzo
* Nakai had Qiric do the rights of hood to send Zeltzin off to the afterlife as a proud warrior
* Nakai broke the pedestal and saved Huani
  * Huani revived the fallen Jatziri and bestowed upon her the powers of the Coatl before leaving to rejoin the Webway
* Nakai left with the party following
* They never found the other people trapped within the temple and just returned home to Fox Reach
* Starkward took Harith's body and buried him somewhere in the Stagwood while Qiric carried some mementos, namely the hat
* After a few days when they neared Fox Reach, they noticed massive smoke clouds errupting into the air and the sounds of a massive beast nearby
  * Qiric shot up into the air to investigate and noticed a dragon had slammed into and destroyed the town's temple
  * The dragon itself was dead, having landed and destroyed half the town (including the Tipsy Gnome)
  * He noticed the rider slaying individuals with ease before fleeing the town
* Qiric followed the rider outside of town and noticed he was wounded
  * Qiric attempted to drop molotov cocktails on top of him but missed
  * In return, the rider shot a fireball, effectively killing Qiric who fell out of the air unconscious
  * Qiric's flower return the life essence it had to save him from dying in the snow and now he must race against time before the flower perishes
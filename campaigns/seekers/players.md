---
title: "Players"
draft: false
---

# Players
* Ryan - reincarnated wood elf
  * When they die, nature selects a select few to be reincarnated into a plant-like form, beginning as a flower
  * Ryan and GRIL were outcasts from their society, being obsessed with alchemy and machines (considered taboo)
  * They died under mysterious circumstances
  * Ryan was given to the Lyceum as a flower, which is considered taboo for wood elves
  * She is still in flower form and he does not know why
  * Lyceum decides to release him since he is a fully sapient being and he enrolls in the school and became a gleaner
  * Special occupation that students can take to help gather information and stuff for the school
  * Has a bunch of friends in school
  * Takes first task to fetch a rogue kroxigor roaming the coast, afraid that the locals will just kill him
  * Seeking help with the task, caretaker given supplies and gold to recruit "helpers"
  * Caretaker is "Gram Gram" Zakiri, a kenku former gleaner who turned researcher, took in Ryan as a young person.
    * Member of the Council
* Dustin - Priest
  * Bloodline blessed by a god they do not worship, instead they follow the Church of another god
  * Always felt like an outsider


---
title: "Captain Brodius Pompeius"
---

CR 8

Male human rogue  
CE Medium humanoid (human)  
**Init** +4; **Senses** Perception +11

### DEFENSE

**AC** 24, touch 15 (+4 armor, +4 natural, +1 deflection, +5 Dex)  
**hp** 104 (8d8+40)  
**Fort** +7, **Ref** +11, **Will** +3  
**Defensive Abilities** evasion, improved uncanny dodge, trap sense +2

### OFFENSE

**Speed** 30 ft.  
**Melee** keen mwk rapier +15/+10 (1d6+4 + 1d8 fire/16–20)  
**Ranged** 2x pistol +13 (1d8 + 1d8 fire/×4)  
**Special Attacks** sneak attack +4d6, grit (2 points, Gunslinger’s Dodge)  
**Rogue Spell-Like Abilities** (CL 8th, concentration +10)

**3/day**&mdash;light  
**2/day**&mdash;[vanish](https://www.d20pfsrd.com/magic/all-spells/v/vanish/)

### STATISTICS

**Str** 18, **Dex** 20, **Con** 18, **Int** 14, **Wis** 10, **Cha** 12  
**Base Atk** +9; **CMB** +15; **CMD** 26  
**Feats** Amateur Gunslinger, Combat Expertise, Exotic Weapon Proficiency (firearms), Improved Feint, Weapon Finesse (rapier), Weapon Focus (rapier)  
**Skills** Acrobatics +13, Bluff +12, Climb +9, Perception +11, Profession (Sailor) +8, Stealth +13 (+17 in dim or no light), Swim +8;  
**Racial Modifiers** +4 to Stealth in dim or no light.  
**Languages** Common, Latin, Greek, Infernal  
**SQ** rogue talents (finesse rogue, major magic, minor magic, weapon training), trapfinding +4  

### Notes
* Improved Uncanny Dodge makes him immune to flanking
* Evasion prevents all damage when a reflex saving throw would half damage from an effect
* Uses his bluff to feint to get sneak attack using his move action
  * DC is 10 + BAB + Wis Mod or 10 + Sense Motive
* Uses acrobatics to move through threatened squares to get into melee combat
  * Five foot steps to unleash pistol attacks when he reloads
* Will use Combat Expertise against tough opponents, preferring a defensive posture
  * -2 to melee attacks, +2 to AC
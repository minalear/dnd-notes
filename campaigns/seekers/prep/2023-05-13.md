---
title: "2023-05-13"
---

## The Quest
* The party will be sent off with supplies provided by the Lyceum, typically what they give Gleaner crews on more important missions
  * Box containing several potions
    * 11x Healing Potion (Light) - 1d8+1
    * 5x Healing Potion (Moderate) - 2d8+2
    * 1x Bull's Strength (5 min)
    * 1x Cat's Grace (5 min)
    * 1x Fox's Cunning (5 min)
  * 2x scrolls of Voluminous Vocabulary (8 hours)
  * Wand of Magic Missile (50 charges)

## The Hellfire Captain Brodius Pompeius
* Dread Pirate Hellfire Captain Brodius Pompeius Heracles Titanicus of the Screaming Eagles, Bane of Caesar, Chosen of Archdevil Barbatos, Scourge of the Ruby Sea, and Lord of the Fifth is his full title
* After the death of Brodius, he was cast into the Fifth Level of Hell where he founded a new pirate crew and conquered the River Styx, claiming the title of Lord of the Fifth
* Despite his playful demeanor, he is a ruthless captain, willing to make an example of anyone to ensure the cooperation of others.  He adopted this style this while fighting for his life in Hell.

### Fleet
* He sails the Infernal Legion, a ship from hell that smells of sulfur and burns with eternal hellfire
* He also sails with two other ships; the Lady's Martyr and the Bateau Rapide that he has captured from recent exploits
* It is common to see a Screaming Eagles boat dock at Stagwood Port, though the Infernal Legion has apparently never been seen near land
* They also have tamed an adolescent kraken which Brodius has named "Big Ricky"

### Crew
* The crew of the Screaming Eagles is mostly human, orc, and maormer, though there are a few exceptions
  * Nicolas Lachange - an Adal exile that acts as a hidden blade for Brodius
  * TikTik and Uther - dwarf cannoneer whose companion, a deformed troll named TikTik, has a cannon strapped to his back
  * Quill and Quoll - twin kobold engineers who are inseparable
  * Fred - a Bearded Devil bound to the service of Brodius.  Due to the laws of Hell, he is the only entity that is fiercely loyal to Brodius.
* Brodius himself is a deadly fighter, the best fighter of the whole crew.  With his Arms of Diavolo ability, he can extend two fiery arms and perform attacks with them.
  * He wields four pistols and two swords in combat, usually preferring an opening barrage from the pistols followed up by sword attacks while he reloads with his extra arms
  * His equipment deals additional hellfire damage due to be being blessed by Barbatos, the Archdevil of the First Level of Hell

### Geeko and Nerdo
* Geeko and Nerdo were former crew members of the Screaming Eagles when they captured the Rosy Red Ferdinand
* In the dead of night, Geeko, Nerdo, and a handful of other mutineers stole the boat and sailed away before Brodius could catch them
* They have been hiding out in the Lyceum for over a year now, posing as sons of an Aragornian explorer, hoping to evade Brodius
* Brodius, through the power of his Devil's Book, knows their location at all times

### Encounter
* The party will encounter the Screaming Eagles on their voyage to Fox Reach
* Geeko and Nerdo are hoping to use their lookalikes, Rico and Germo, to fool Brodius and allowing them to escape
* Brodius is no fool and won't fall for the ploy, but still wants the ship back and does not care about the status of the crew
* Geeko and Nerdo may attempt to flee before the encounter, most likely in the dead of night
* The Screaming Eagles, between the three ships, boasts a fighting crew of 40 as well as Big Ricky that can be used to immobilize ships

## Fleek the Fabriki
* Fleek is 357 cycles old and is wishing to see the world
  * Freaked out that his mother has asked if he planned on building her any grandchildren
* Fleek is seeking places that his grandfather has described, namely these ancient Sekar ruins
* Fleek will have information on both Fox Reach's temple (known as Thelmare in ancient times) and Xalnathul
  * His grandfather, Marsupial, constructed these ages ago and would tell Fleek tall tales of these great cities built into the skies, massive beasts of burden hauling the material, etc.
  * Marsupial is clinically insane and doesn't actually recall much about these cities, just exagerated images exist
  * He will have information on how to gain acess to Xalnathul as it was a secret port city of the Sekar uses to spy

## Mushroom
* Over the next few days, the mushroom Germo and Rico have adopted will mature, first growing eyes, and then baby talk
* It'll progress into a child that can walk around and it will be about the size of a quarter

## Stats
* [Maormer](https://www.d20pfsrd.com/bestiary/unique-monsters/cr-7/rhun-gha-1/) CR 6
* [Maormer](https://www.d20pfsrd.com/bestiary/unique-monsters/cr-5/ship-scavenger/) CR 5
* [Pirate](https://www.d20pfsrd.com/bestiary/unique-monsters/cr-4/sightless-sea-sailor/) CR 4
* [Fred - Bearded Devil](https://www.d20pfsrd.com/bestiary/monster-listings/outsiders/devil/bearded/)
* [Nicolas LaChange - Dark Slayer](https://www.d20pfsrd.com/bestiary/unique-monsters/cr-15/zelfane-vexidyre/)
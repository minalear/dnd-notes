---
title: "2022-11-12"
---

## Characters
* Aella
* Harith
* Hurdir "Starkward"
* Qiric
* [Zeltzin-Xochit](https://docs.google.com/document/d/1bmcHtYUyMe1JcvYw22MngKdJNBvh1DlG4eGhtlBYmdU/edit)
  * Jatziri

## Setup
* Players are sneaking into the Temple of Hood within the ruins of Quetza by disguising Qiric as Markus Wulfheart
* He hopes to convince the site administrator that he is here to slay the beast that is plaguing the digsite
* Nakai was able to get access to the temple by the ancient Webway

## Quetza, the Lost Temple-City
* The temple has somehow been "reactivated" since the incident, with many passageways and rooms changing and defense mechanisms activating
  * The temple to Hood has been massively expanded from what is normal, possibly indicating the city being dedicated to the Lord of Death
  * A lot of the iconography to Hood has been changed to some unknown, faceless figure depicting arising from some void
    * This figure is the Demiurge, an entity from another world pure negative force
    * Lord Kakmuzo began worshiping it to ascend to become a Relic Priest 

### Arvo Mark, Site Administrator
* Agent of the Consortium, very stern but willing to make a deal to ensure his operations proceed smoothly
* Possesses an Idol of the Spider Goddess that grants him immortality and a small reservoir of divine magics
  * Found during an excavation of an old elven battleground, thought to be an idol of an Adal General

### Dungeon Mechanics
* Based on this [5e Dungeon](https://5e.tools/adventure.html#tftyp-thsot,-1,running%20the%20adventure,0) module.
* 

### Items of Interest//Loot
* 500gp, 2512sp
* gold and silver chess set (1000sp)
* rod of metamagic, reach (lesser)
* [Ring of Chameleon Power](https://www.d20pfsrd.com/magic-items/rings/ring-of-chameleon-power/)
* Torch of Sidon (modified Archon's Torch) - golden torch inscribed with Seraphon runes
  * Casts light as a normal torch for up to five minutes
  * Gives the user the benefit of [Archon's Aura](https://www.d20pfsrd.com/magic/all-spells/a/archon-s-aura) for the duration
  * Has three charges
  * Markings on the torch show signs of attempted modification, but to no avail
* Astralabe - item used to orient with the Webway, the mystical realm connecting the materium with the realm of light
  * Obvious signs of modification to align with the realm of shadow
  * Shadow Path used to bypass the natural laws of life and death and to allow seraphon casters to use necromancy
  * This Astralabe was used to help create the Whispering Way
* Inscribed Jade Spearhead - cursed item
  * Grants the wielder +2 Natural Armor + DR 5/adamantine
  * When attached to a weapon, the weapon is treated as a +1 vicious weapon of that type
  * Unbeknownst to the wielder, the crafter of the spearhead can watch the user; Lord Kakmuzo

### Nakai and Huani
* After rescuing Huani, Nakai will return home from whence he came; through the ocean
* Huani will bestow her power upon Jatziri and will rejoin the lifeforce within the Webway

## Returning to Fox Reach
* The party will return to town to find part of the city in flames from a very recent dragon attack
* They will find the body of the drake crashed in the center of town which destroyed the old temple
* Markus Wulfheart is wounded, having taken down the drake, and most of his rangers slain by the dragon rider
* The rider is escaping into the nearby forest, heavily wounded, but still incredibly dangerous
* The [Dragon Rider](https://www.d20pfsrd.com/bestiary/npc-s/npcs-cr-11/wil-telaxxis-human-fighter-8-hellknight-4/) is a Warrior of Xah En, sent from their dimension to break the bonds that are dragging the worlds together
* The rider will attempt to leave through a rift; any players who cross the barrier will need to make a new character
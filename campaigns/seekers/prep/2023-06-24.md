---
title: "2023-06-24"
---

## Pre-Journey
* Gilbert will present Miss Aella with a gift, as a token of his appreciation for being such a kind boss
  * [Monk's Robe](https://www.d20pfsrd.com/magic-items/wondrous-items/r-z/robe-monk-s)

## Journey to Xalnathul
* The journey to Xalnathul with the Deepwater Salvage Company is straightforward with no real altercations
* The party will receive a missive from Gilbert informing them of Rico and Aella's wanted status
  * The missive will mention that a "small, sickly boy matching Germo's description was captured attempting to cross the border"
  * Gilbert has locked Aella's assets up in the courts, so she has no fear of losing them, but he can't do much about her wanted status
* Journey takes two days to reach the location with the guidance of the Lantern
* At the location, there are several rocky outcroppings indicating the previous location of the city
  * These outcroppings form a rough circle around where the city sunk to the bottom
* It would take several hours for the submersible, the *Size Isn't Everything*, to reach the bottom of the Ruby Sea
  * Despite many creaks and moans from the hull, it holds up to the pressure
* When they reach the sea bottom, they notice that the location where the city should be actually descends further into a fault
  * This fault line doesn't line up with current geographical information, so this must have been the cause of the city sinking
  * During this time, they'll be accosted by Cephalid warships.
    * They're not outwardly hostile, but they will attempt to detain the submersible
    * The pilot will be able to successfully navigate away (or if a player is piloting, they would need to make checks)

## Xalnathul
* Descending into the rift will reveal the vibrant light of the city far beneath
* The city is encased in a protective bubble emitted from the Eye, so the submersible will need to park at the edge of the city
* The city center is the only thing that is relatively the same.  The rest of the city has long since degraded and fell to ruin.

### Outer City
* The Outer City that encircles the City Center is composed of crumbling ruins and crawling with mindless Naga that attack anything on sight
  * They are frequently living in small bands and routinely attack and attempt to kill these other bands
  * Over the eons, these bands have established territories that they rarely leave
* Navigating through the Outer City requires individuals to navigate through crumbling structures and through destroyed streets.  There are no clear paths through and only those that have lived here for a long time know how to navigate it effectively.
* The Outer City is also very dark with little to no natural illumination.
  * The party can see the light from the center city, but they are too far away to be illuminated by the central tower and its lights

### Inner City
* The Inner city is actually maintained by the surviving denizens of Xalnathul, with lanterns illuminating street corners
* There are only a few dozen people left within the city and they would all be horrified of anyone walking through, so they would avoid the players at all costs
* Guards may accost them, but they most likely would flee and warn Vex and the Council
  
### Vexenith
* Vexenith is the de facto ruler of the city after he locked Queen Nara away within the tower
* He would want to kill the party and capture their method for traveling to the surface
* Additionally, he is also in communications with the Cephalid Emperor Aboshan
  * They wish to make an alliance, trading powerful relics for the ability to leave the city and return to the surface
  * They no longer are under the impression that the Sekar remain on the surface and wish to join the Underwater Empire in their plans for surface domination
  * The powers held within the Great Library of Zalor could be the ticket Aboshan needs to be able to travel to the surface safely

### Queen Nara
* Once when the players get close enough to the central tower, Queen Nara may attempt to communicate them telepathically
* She will beg for their help in freeing her as she is locked away in her tower
* She wishes to return to the surface and to leave the depths behind
* She scorns her council but loves her people and will want to find a way to rescue them and to establish a new life for them on the surface
  * In reality, without the Eye of Zoggatha, she would die without its life sustaining energies

### Great Library of Zalor
* The Great Library is home to Zalor the Wise and his underlings.
  * He is currently the only one left alive, as his sect was dedicated to discovering a method of reversing the genetic mutations their people have endured
  * If they are to meet Zalor, he is now a humbly looking frog person with gnarly teeth
* The library is full of magical artifacts, though most require to be a highborn Sekar to use

#### Treasures of the Great Library
* **Serpent Staff of Wisdom** - grants the user a +4 enhancement bonus to their Wisdom
* **Scale of Zoggatha** - large, iridescent scale said to be from their ancient god.
  * Can be used as a shield and when used as a spell focus, increases any spell DC by +2, checks to overcome SR by +2, and concentration checks by +2
  * Can be modified by a skilled artificer into a ring, arm band, or amulet
* **Fangs of Coiled Fury** - Said to be the fangs of a legendary Coatl.  
  * Their strikes causes a spiritual serpent to strike in addition to their own.
  * Fists become a Wounding +2 weapon
* **Scepter of Dominion** - https://www.d20pfsrd.com/magic-items/staves/staff-of-enchantment/
* **Serpent Crested Crown** - +4 enhancement bonus to Charisma, can cast Dominate Animal (serpents) at will
* **Naga-Scale Bindi** - iridescent scale of the first priestess of Zoggatha who was slain by her god and her body granted supernatural magics
  * [Naga-Scale Bindi](https://www.d20pfsrd.com/magic-items/wondrous-items/m-p/naga-scale-bindi)

## Notes
* The Eye sustains the inhabitants of the city, preventing them from dying, but they still suffer from hunger and mental decay
  * Only the most resolute have survived thus far, namely Queen Nara, her council, and several individuals of the Library of Zalor.

## Encounters
* The party will be encountering rabid Naga, the Naga nobility, and the Gith.
  * The Gith are here attacking the central tower of Xalnathul that houses both the Eye of Zoggatha and the Attunement Nexus.
  * They will be on foot, guarded by two dragons; one scouting the Outer City and one assaulting the central tower

### Outer City
* Dragon of Xah En - [Dragon Statblock](https://www.d20pfsrd.com/bestiary/monster-listings/dragons/dragon/chromatic-blue/blue-dragon-young-adult)
  * Much like the blue dragons of Menadi, these dragons much prefer to surprise attack their prey and fight dirty
  * The dragon will attempt to assassinate one of the party and fly off with their body if any of them are out in the open
  * It will attack using the cover of darkness that the outer city provides
  * If it studies the party long enough, it can use voices it has heard and mimic those and attempt to lure out others.
  * It will also attempt to sabotage the party by destroying any water or potions they are carrying using it's frost breath.
* Rabid Naga - these attack in bands of 3 to 5, but only when they believe they have the upper hand
  * [Naga Statblock](https://www.d20pfsrd.com/bestiary/monster-listings/monstrous-humanoids/sahuagin)

### Central City
* Naga Guards - similar to the rabid naga, they can be found in groups of anywhere from 5 to 10, but they typically possess casters as well to support their units
* Gith - the Gith will recognize the party (Rico) and assume they are here to stop them from destroying the nexus (and destroying their world)
  * [Gith Statblock](https://www.d20pfsrd.com/bestiary/npc-s/npcs-cr-11/wil-telaxxis-human-fighter-8-hellknight-4/)
  * If the party communicates that they are here to help destroy the nexus, they will assume it is some trick and be against allying with them
  * The Gith are immune to magics native to Menadi due to them being outsiders (and the opposite is true too)
  * However, any effects that can produce a tangible physical effect, like a fireball, will still negatively effect them
* Vex - [Vex Statblock](https://www.d20pfsrd.com/bestiary/monster-listings/monstrous-humanoids/deep-one-elder-2/)
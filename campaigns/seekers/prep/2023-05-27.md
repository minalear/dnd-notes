---
title: "2023-05-27"
---

## Setup
* Players are current in a hostile negotiation with Brodius and the Screaming Eagles
* Brodius is intrigued with Mirai's transformation abilities and is thinking how he can exploit that to fuck over the other pirate lords
* He will have Mirai take his place in a game of Damage at Corazon del Mar

## Damage
* Damage is a type of card game the pirates of Menadi love to play
* Typically it's played in areas of extreme danger; amidst a hurricane, during the siege of a city, edge of a wildfire, etc.
* Money and live tokens are bet until a player runs out of betting pieces
  * Live tokens are either willing participants or hostages
  * 10,000 crowns are worth one live token
  * Players can bet their own lives as two tokens
  * When a live token is lost, they are killed and the winning player is granted 10,000 crowns from the game master
* Game Master of Corazon del Mar is an ancient Drakthul named Maelstromos
  
## Corazon del Mar
* An ancient cavern found in the isle of Alvernia, located between Anteras and Saricur
* It is a place of great evil and is infamous within Maormer society
* It is said that once every year, the lives of 10 young sea elves must be sacrificed to the entity within Corazon del Mar or a year of bad storms will wreak the coasts, costing them many more lives
* The stories are true for within is an entity of ancient power called Maelstromos
* In recent years, pirates have picked up the yearly tradition of sacrifice, but instead play a game of damage
  * Maelstromos allows this as they typically bring much more sacrifices and he enjoys the torment the sacrifices are subjected to during the game
  
## Maelstromos
* Maelstromos is a Drakthul, a race that was created when the Aelf-Adal's darkness was severed from Lloth by an ancient hero
  * They are the embodiment of Lloth's anger and they wish is to bring suffering to mortals
  * With Lloth's final defeat and banishment within a Azath Houth, they have become thralls of Akam
* Maelstromos was once named Kelad, but was wounded in a battle centuries ago that have left him beaten and broken
* He now lives within Corazon del Mar, slowly healing himself by tributes of souls from the local Moarmer population
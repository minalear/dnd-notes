---
title: "Fabriki"
---

The Fabriki are an artificial race of humanoids native to the small region known as Tagris on the continent of Viland. They were created centuries ago by the fallen Sekar Imperium to be a slave race of dedicated workers. 

### Society

Fabriki are highly social creatures, forming tight communities throughout the rugged hills of Tagris. They live within massive silos they construct from the same materials their bodies are made from, unique substances found only in the local region. Very few Fabriki leave their homes, but occasionally a free spirit will explore outside the realm of Tagris and become an adventurer. These Fabriki are considered exiles and are not welcome back to their home silos. 

**Typical Names**: Dolphin, Marsupial, Grand-Stand, Tower, Siege, Roll-Around.

### Biology

Fabriki are biologically immortal, with many being centuries old and able to recall memories of the days of the Imperium. However, due to their fragile psyches and eccentric personalities, not much important information is usually gleaned from these memories outside of fantastical stories.  In addition, Fabriki are sexless, typically choosing whatever pronouns "sound the most interesting."

### Reproduction

The Fabriki reproduce unlike any other race on Menadi. The first day of every year of the Fabriki calendar, communities will get together to create new bodies made of clay and hardened sky metal. Once when the sun reaches its zenith, life is born into these forms, creating a new generation of Fabriki. 

### Standard Racial Traits

- **Ability Score Modifiers**: Fabriki tend to be fairly smart and quick, but lack much common sense. They gain +2 Intelligence, +2 Dexterity, and -4 Wisdom.
- **Size**: Fabriki are Medium creatures and thus receive no bonuses or penalties due to their size.
- **Type**: Fabriki are considered both humanoid and construct.
- **Base Speed**: Fabriki have a speed of 30 feet.
- **Languages**: Fabriki begin play speaking Common and Fabric.

### Defense Racial Traits

- **Elemental Vulnerability (Acid)**: Fabriki are vulnerable to acid attacks (50% additional damage from acid energy attacks).
- **Hardy**: Fabriki gain an additional 20 hit points due to their artificial nature.

### Feat and Skill Racial Traits

- **Craftsman**: Fabriki gain a +2 racial bonus on all Craft or Profession checks to construct objects out of metal, stone, or clay.
- **Skill Bonus**: Fabriki gain a +2 racial bonus on Knowledge (History) checks.

### Senses Racial Traits

- **Darkvision**: Fabriki can see perfectly in the dark up to 60 feet.
- **Low-light Vision**: Fabriki can see twice as far in low-light conditions.

### Other Racial Traits

- **Constructed**: Fabriki lack a Constitution Score.  Any DCs or other statistics that rely on Constitution treat the Fabriki as having a score of 10.
  - They are immune to any effect that requires a Fortitude save (unless the effect also works on inanimate objects or is harmless)
  - They are immune to all mind-affecting effects
  - They cannot heal damage on their own and are immune to any effect that channels positive or negative energy.
  - They are not subject to ability damage, ability drain, fatigue, exhaustion, energy drain, or nonlethal damage.
  - They are immediately destroyed when reduced to 0 hit points or fewer.
  - They cannot be raised or resurrected.
  - They do not breathe, eat, or sleep, unless they want to gain some beneficial effect from one of these activities. This means that a Fabriki can drink potions to benefit from their effects and can sleep in order to regain spells, but neither of these activities is required to survive or stay in good health.
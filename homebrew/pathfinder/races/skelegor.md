---
title: "Skelegor"
draft: false
---

# Skelegor
Type - Aberration - 3 RP
  60 ft darkvision
Size - Medium - 0 RP
Base Speed - Normal Speed - 0 RP
  30 ft base speed
Ability Score Modifier - Flexible - 2 RP
  +2 to two ability scores
Language Quality - Standard - 0 RP
  Racial and Common

Undead Resistance - 1 RP
  +2 bonus on saving throws against disease and mind-affecting effects
Unnatural - 2 RP
  -4 Charisma-based skill checks to affect animals
  +4 dodge AC against animals
  Animals starting attitude towards members one step worse
Skeletal Damage Reduction - 2 RP
  Gain DR 5/bludgeoning

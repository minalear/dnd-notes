---
title: "Slann"
draft: false
---

# Slann (22 RP)
Monstrous Humanoid (3 RP)
Large (7 RP)
Slow Speed (-1 RP)
Advanced (4 RP) +4 Intelligence, +2 Wisdom, +2 Charisma, -2 Dexterity
Standard Language (0 RP) Common, Saurian, Slannish

Slann Magic (Elven Magic) (3 RP)
Levitate, at-will (4 RP)
Terrifying Croak (2 RP)
---
title: "Saurus"
draft: false
---

# Saurus (10 RP)
Monstrous Humanoid : 3 RP
Medium : 0 RP
Normal speed : 0RP
Standard ability : 0 RP (+2 Strength, +2 Wisdom, -2 Charisma)
Standard Language : 0 RP (Common, Saurian)

Darkvision 60 ft

Racial Abilities
Bite (1 RP)
Natural Armor (2 RP)
Flexible Bonus Feat (4 RP)
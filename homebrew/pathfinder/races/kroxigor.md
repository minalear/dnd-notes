---
title: "Kroxigor"
---

## Kroxigor (20 RP)
Monstrous Humanoid : 3 RP
Large : 7 RP
Slow speed : -1 RP
Standard ability : 0 RP (+2 Constitution, +2 Wisdom, -2 Intelligence)
Standard Language : 0 RP (Common, Saurian)

Darkvision 60 ft

Racial Abilities
Bite x2 (2 RP)
Frenzy (2 RP)
Natural Armor (2 RP)
Improved Natural Armor (1 RP)
Scent (4 RP)
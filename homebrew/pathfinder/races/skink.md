---
title: "Skink"
---

## Skink (11 RP)
Monstrous Humanoid (3 RP)
Small (0 RP)
Fast Speed (1 RP)

Flexible (2 RP)
Standard Language (0 RP) Common, Saurian

Craftsman (1 RP)
Emissary (1 RP)
Skink Magic (Gnome Magic (2 RP))
Bite (1 RP)

## Chameleon Skink (14 RP)
Monstrous Humanoid (3 RP)
Small (0 RP)
Fast Speed (1 RP)

Flexible (2 RP)
Standard Language (0 RP) Common, Saurian

Sneaky (5 RP)
Skink Magic (Gnome Magic (2 RP))
Bite (1 RP)
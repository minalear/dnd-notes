---
title: "Warlock"
draft: false
---

# Warlock
Alignment: Any
Hit Die: d8
Starting Wealth: 3d6 x 10 gp (average 105 gp)

## Class Skills
  Craft (Int), Fly (Dex), Deception (Cha), Knowledge (Arcana), Knowledge (History), Knowldge (Religion), Knowledge (Nature), Knowledge (Planes), Profession (Wis), Spellcraft (Int), and Use Magic Device (Cha)

Skill Ranks per Level
2 + Int Modifier

Level   BAB     Fort    Ref   Will    Special
1st     +0      +0      +0    +2      Pact Magic, Cantrips, Eschew Materials, Otherworldly Patron
2nd     +1      +0      +0    +3      
3rd     +1      +1      +1    +3      
4th     +2      +1      +1    +4      
5th     +2      +1      +1    +4      
6th     +3      +2      +2    +5      
7th     +3      +2      +2    +5      
8th     +4      +2      +2    +6      
9th     +4      +3      +3    +6      
10th    +5      +3      +3    +7      
11th    +5      +3      +3    +7      
12th    +6/+1   +4      +4    +8      
13th    +6/+1   +4      +4    +8      
14th    +7/+2   +4      +4    +9      
15th    +7/+2   +5      +5    +9      
16th    +8/+3   +5      +5    +10     
17th    +8/+3   +5      +5    +10     
18th    +9/+4   +6      +6    +11     
19th    +9/+4   +6      +6    +11     
20th    +10/+5  +6      +6    +12     

Level   Cantrips    Spells    Slots   Slot Level    Invocations
1st     2           2         1       1st           0
2nd     2           3         2       1st           2
3rd     2           4         2       2nd           2
4th     3           5         2       2nd           2
5th     3           6         2       3rd           3
6th     3           7         2       3rd           3
7th     3           8         2       4th           4
8th     3           9         2       4th           4
9th     3           10        2       5th           5
10th    4           10        2       5th           5
11th    4           11        3       5th           5
12th    4           11        3       5th           6
13th    4           12        3       5th           6
14th    4           12        3       5th           6
15th    4           13        3       6th           7
16th    4           13        3       6th           7
17th    4           14        4       6th           7
18th    4           14        4       6th           8
19th    4           15        4       6th           8
20th    4           15        4       6th           8

## Class Features
The following are the class features of the warlock.

### Weapon and Armor Proficiency
Warlocks are proficient with all simple weapons.  They are not proficient with any type of armor or shield.  Armor interferes with a warlock's gestures, which can cause their spells with somatic components to fail.

### Spells
A warlock casts arcane spells drawn from the witch spell list.  They can cast any spell they know without preparing it ahead of time.  To learn or cast a spell, a warlock must have a Charisma score equal to at least 10 + the spell level.  The Difficulty Class for a saving throw against a warlock's spell is 10 + the warlock's Charisma modifier + 1/2 the warlock's caster level.

A warlock's selection of spells is extremely limited.  A warlock begins play knowing two 0-level spells and two 1st-level spells of their choice.  At each new warlock level, they gain one or more new spells, as indicated on the Table:Warlock Spells Known (these spells are not modified by their Charisma score, the numbers on the table are fixed).  These new spells can be common spells chosen from the witch spell list, or they can be unusual spells that the warlock has gained some understanding of through study.

Upon reaching 3rd level, and every odd numbered warlock level after that (5th, 7th, and so on), a warlock can choose to learn a new spell in place of one they already know.  In effect, the warlock loses the old spell in exchange for the new one.  The new spell's level can be of any level that the warlock can effectively cast, dictated by the Table:Warlock Spells Known table.

Unlike a wizard or a cleric, a warlock need not prepare their spells in advance.  They can cast any spell they know at any time, assuming they have not yet used up their spells per day.
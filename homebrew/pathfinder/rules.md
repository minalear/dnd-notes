---
title: "Rules"
---

# Rule Changes
* Full night's rest gives you level + CON modifier health (CON cannot reduce the number if negative)
* Every character gets max HP at level up (monsters are at max hp)
* Passive perception is 10 + Perception and you are assumed to be taking a 10 unless otherwise stated.
* You cannot rest to regain spells or hp more frequently than once every 24 hours.
* Mundane ammunition count does not need to be tracked.

## Character Creation
* Players use standard array for stats: 15, 14, 13, 12, 10, 8
* All non-int classes are bumped to a base 4+int for skill points
* For the first three sessions you are given free range to change your character's feats/skills/spells/etc, in case something isn't working.
* Alignment restrictions on classes are disregarded
* Free point into Profession skill at character creation for relevant background information.
* Rogues get full BAB advancement (similar to fighter)
  * Does not apply to Unchained Rogue

## Fate Points
* Every PC begins the game with a single fate point. They can be spent on the following:
  * Instantly succeed on a roll
  * Instantly becoming stable at 0 hp
* Fate Points can only be used for *that* PC.

## Spell Changes
* Casters don't get bonus spells-per-day from their ability scores
* Find Traps and Knock are no longer on any spell lists
* Cure spells (and potions) heal for max outside of combat

## Feat Changes
* Reduced need for feature requirements.  If a feature you want requires a feature you don't want/need, ask the DM for approval.
* Leadership feat is removed

## Death and Resurrection
* Raising creatures from the dead is difficult and as such there are few people in the world able to do it.
  * Raise Dead, Resurrection, and any other spells that raise characters from the dead must be run by the DM before selection.

## Revised Falling Rules
 * 1d6 per 10 feet fell as per normal
 * Anything over 30 feet will result in a "Hard Fall"
 * Hard Falls require a DC 20 Fortitude save or PCs take full damage
 * Damage is capped at 500 ft, or 50d6

## Minor Rules
* Intelligent Undead are subject to mind affecting spells
* Mounts and familiars are not harmed unless you use them in combat (or do something really stupid with them)

# Friday Game

# Saturday Game
* At character creation, roll 1d6, or 1d12 if you have a charisma over 12. The number will determine people you "know" personally. Upon meeting an NPC you can declare you know them, using one of the numbers, you are then in charge of making up this person and how you know them.
---
title: "Castle Griffinsteed"
---

## Intro
**One Shot Introduction**
Today is a very special day.
Every 25 years, the barbarian village of Elvenwood celebrates what they call the "Warrior's Harvest."
A fruitful, wonderful celebration of honor where the finest soldier of the village is chosen to venture out far away from camp and potentially to never return.

**Post Tournament**
You are tasked with finding out what happened with your old family's house.  Castle Griffinsteed.  Long since abandoned and corrupted by dark magic.  Uncover the secrets of your old family name, and your adventure begins.

**Castle Gates**
You consider what more will lie ahead.  What treachery.  What deceit will lie ahead of you in Castle Griffinsteed.  Where you stand right now, no man has stood.  No man enters, no man returns.  When you step in, you are there forever, or so your grandmother has told you in her old stories.

A castle in ruin, once great standing tall is a former, fraction of what it used to be.  But this is not the test.  The test is below in the catacombs (players hit checkpoint here).  One can only imagine the secrets and the horrible dungeons that lie ahead of you.  You must press on and must find out what happened to your house.

**Old Family's Prisons**
You can tell it's a prison, there are bars on the wall.

**Rocket Launcher**
You remember your old grandmother saying "You'll never know when you need a rocket launcher."

**Magic Land**
What the fuck is this?  This is under Castle Griffinsteed?  A whole land of strang things, creatures, and wildlife and vegetation you have never seen before.  You must keep your wits about you if you are to uncover the demonic presence underneath your old family's house.

**Another Dungeon**
It's a zombie game now.  Every game needs to have zombies in 2015.  You feel you're not far away from the final secret.

**18xGrandfather**
That's it, the zombies are no longer pursuing.  Have you made it to the final room?  The final door?  Will you uncover the secrets of your family name?  What is in here?  Who is this?

## Info
* Castle Griffinsteed is about 200 feet away from Elvenwood.
* The castle has been seized many times by many different forces.
* When the players find their guns, they learn they can "Aim Down Sights" like their old grandmother once taught them for a +1 attack bonus.  She would be so proud of you if she knew you were using it now.

## Potential Scenes
* Warrior's Harvest - there is a small tournament to determine the strongest soldier in the village and who will leave the village, potentially to never return.
* Broken Down Caravan - there is a scary animal inside.
* Castle Outpost - bandits are waiting for the players there.  They're just idiots.
* Castle Gates - just talk about the castle
* Catacombs - two paths, one left and one right.  The right path has coins.
    - Right path leads to Father's dungeon.
    - Left path leads to Old Family's Prisons.
* Father's Dungeon - Wizards.  Evil wizards.  They may be conjuring up a demon.
    - Loot: Shotgun
* Old Family's Prisons - leads straight to the the supply room.  Upstairs lead to the dungeon.
     - Loot: healing potions, Uzi, Magnum, Flail
* Dungeon - the military are here.  They use Uzis.  Coins.  One dude with rocket launcher.  Leads into a carved out cave (perfect oval).
* Cave Systems - Seems to have been dug out rather recently.  Has stairs that players can't walk down unless they make a DC 25 acrobatics check or walk around.
    - Mold and mildew caking the walls causes nausea on a failed DC 12 Con save during their time in the caves.
    Lesser Demon Al'gorro, the third lieutenant of the underworld.  You don't stand a warrior's chance.  (Vulnerable to rocket launcher damage).
    - After defeating Al'gorro, wall opens up to reveal magic land.
* Magic Land - filled with rabbits that just follow and creep the players out.  They don't aggro until they find the sacrificial home.  They use lazer beams.
    - There is a shop, spend the coins collected to buy health potions, a mushroom, radar gun, and a crystal.
    - Leads into another dungeon.
* Another Dungeon - filled with zombies, rabbits, soldiers, and Al'gurru, the giant demon stuck in the wall for all eternity.  Exploding barrels.
    - Al'gurru starts moaning really loudly when he begins casting a very large spell.
* 18xGrandfather - great times 18 great grandfather, he will teach you all the secrets and the only person to ever make it through the dungeon.
    - Choice between coming with grandpa or two months supply of chips.  It's a lot of chips.

    "Welcome *player name* it is I, your great times eighteen great grandfather, I can teach you all the secrets for you are the only person to ever make it through the dungeon.  So your choice is this, come with me or take this at least two months supply of chips.  The choice is yours.  I stand you will make the correct one.  What do you want to know about your father?  What your grandfather?"
---
title: "USC Visitor"
---

## USC Visitor

The USC Visitor, a Rangefinder-class Corvette, acting as the carrier vessel for a UAP Far-Field Team.  The USC Visitor currently has a crew of 150 with a contingent of researchers, diplomats, and a number of support crew and is acting as a patrol boat and limit contact vessel.

The ship currently has two Osprey-class dropships used to ferry cargo and personnel to and from the ship.  The Ospreys lack any traditional armaments, but the ship's armorer does have a set of gatling guns that can be mounted within the dropship's doors.

The ship's armaments include; 1xMAC, a point-defense laser array, and two torpedo bays which is often use to launch Javelin-class torpedos or sensor arrays.

The ship's Curator is named Bob Saget and is a pretty dry AI assistant.

![USC Visitor](https://dm.minalear.com/one-shots/star-trek/images/usc_visitor.png)

## Sensors 

The ship also has a standard array of sensors allowing the ship to scan celestial bodies from 2 LY away.
* Long-Range Sensors allow for low resolution scans of objects up to 5 LY away.  These sensors typically cannot pick up objects as small as ships who are not projecting a warpspace beacon, though are more than enough to determine general information from various celestial bodies like stars, planets, moons, and asteroids.
* Lateral Sensors allow for high resolution scans of nearby celestial objects and ships.  These are powerful enough to scan objects as small as a car within a solar system; typically 200,000AU

![Osprey Dropship](https://dm.minalear.com/one-shots/star-trek/images/osprey_dropship.png)

## Crew Roles
* Captain
  * First, Second, Third Officers
  * 10x yeomen
* Chief Science Officer
  * 10x researchers
* Chief Tactical Officer
  * 10x Security Detail
* Chief Medical Officer
  * 5x medical staff
* Chief Engineer
  * 10x engineers
* Armorer
  * 3x journeymen
* OGR Officer
* 1xRifle Teams
  * Corporal, Sergeant, 5xPrivates
  * 2 LANCER suits; One utility, One combat

## Crew Names

### Humans
* Vivian Gardner
* Kirby Wood
* Gayle Collins
* Max Weaver
* Ellis Gibson
* Quinn Barick
* Everett Alder
* Fenris Bentzen
* Marten Smith
* Wynston Barick
* Nikos Rocheford
* Urien Dering
* Valen Catlow
* Derrik Harcrow
* Fafnir Arleth

### Other
* Aaman Daro
* Mari Jaka
* Bretek Zaro
* Aamin Dejor
* Corbin Nile
* Ar'Mek
* T'Kata
* Morka
* Kora
* Keebho
* N'Kala
* N'Mara
* R'Grali
* Vekma
* Rali
* Teg
* Khag
* Colv
* Gavr
* Yog
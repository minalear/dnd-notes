---
title: "Great Hedgehog Adventure"
---

* party starts off in an inn
* they are seasoned adventurers
* a middle aged sonic the hedgehog slowly rolls in to get drunk
* party needs to help him find his lost friends from a battle with Egg that scattered them through the various dimensions
* every time the party saves a character, they get turned into a chaos emerald and sucked into a small portal
* sonic claims that Egg is stealing them for some nefarious purpose

* knuckles is in hell, having being the only one that actually died, and is currently being punished for his sins
  * knuckles is being forced to punch the devil's ballsack repeatedly for all of time
* tails was dropped into a victorian gothic horror plane where he took up experimenting on the local populace
  * werewolves that fly like him and zombies that had their bodies combined together
* amy has been kidnapped and forced to work at the "House of Suck n Jive" where she works as a stripper/prostitute
  * she really enjoys working there and everyone treats her with respect
  * really doesn't want to leave
* shadow is currently working a dead end job at a mcdonald's where every machine is broke and there is a constant line of karens yelling at him
* sonic doesn't want to save big the cat from the realm of eternal nightmares

* the final confrontation is with Egg, a seemingly normal household egg, surrounded by the chaos emeralds that hold the souls of sonic's friends
  * he poses no threat and can easily be squished
  * the pc that squishes the egg will wake up the next night in their room, completely surrounded by inumerable eggs
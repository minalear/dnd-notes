---
title: "One Shot Rules"
---

* Level Four
* No feats/no variant human
* No multiclassing
* Use standard array for stats; 15, 14, 13, 12, 10, 8
* Begin with class starting equipment
  * Equipment will be provided in the adventure
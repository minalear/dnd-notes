---
title: "Timeline"
---

## Classical Era
* Founding of the First Empire on Emos
  * First Empire is united through conquering various human tribes south of the Emosian Spine
  * Gave way to the modern incarnations of Mercia and Etrusca
  * Rampant corruption and political infighting lead to the Empire losing influence
  * Never technically fell, but has since lost its control over its elector states who have seceded
  * Many current nations were former electoral states of the First Empire
  * Capital region is Odias
  * Mercia has absorbed many former First Empire nations and has taken the mantle of "Empire of Man"

## Common Era
* 1 CE - Crowning of the First Phoenix King of Mercia
* 108 CE - Etrusca invaded Viland
  * 113 CE - Invasion concludes with the fall of Astor
* 383 CE - Fall of the Etrusca
  * Red Revolution engulfs the northern regions of Viland, forcing the empire to retreat
  * Valtara breaks away from Veryn during the reconstruction, refusing to accept the return of the king
---
title: "Todo"
---

* Detail the city of Whitham
* List the various nobility and major political players for the various regions (Bannorn, Margrave, Whitham, Manhing).
* Devise various local plots near Whitham that the party will focus on during the initial stages of the campaign
* Rethink the native race of Xah En
* Name the ocean (Sea of Ladesca) as well as the trading league that charters the Aspis Trading Company (Mercia, Akros, Limnos, Meletis, Volos, and Arta)
* Name the specific school body that manages the gleaners
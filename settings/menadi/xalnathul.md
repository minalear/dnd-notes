---
title: "Xalnathul"
---

## History
* Xalnathul was a Sekar city located on an artificial island at the heart of the Ruby Sea
* It was built in secret, protected by powerful wards, allowing only the Sekar to locate it and enter its ports
  * Only a specially crafted magic device, known as a Seeking Lantern, could locate the city
* It was a major travel hub for many of the Sekar ships navigating the Ruby Sea and was the base of their primary navy
* At the city's heart was a relic called the Eye of Zoggatha, an artifact that kept the city hidden and protected by unwanted eyes
  * The Eye was also a powerful weapon, capable of destroying invading fleets from miles away.
* Xalnathul sank during the final era of the Imperium during the calamity that caused the rest of the Imperium to decline
  * The calamity, referred to as Arda's Reckoning, caused 80% of the Sekar populace to disappear overnight
  * The Eye of Zoggatha attempted to protect the city from this event but the wards became overpowered and caused the city to sink beneath the waves
  * The citizens of Xalnathul were kept alive by the Eye and granted a limited form of immortality, where they cannot die naturally, but they can mentally devolve
  * They also slowly warped into the Naga, a race of subaquatic Sekar, though they lack the ability to breathe underwater
* In the modern era, they are currently in communication with the Cephalid Empire and their Emperor Aboshan
  * The Emperor would like to trade the denizens of Xalnathul the ability to live and breathe underwater and to join the empire in exchange for the city and the artifacts found within
  * Absohan's desire is to conquer the surface world, but they currently lack the ability to survive in lower depths, let alone on the surface
  * He believes an alliance with the Naga and access to their technology is an important step in this process.
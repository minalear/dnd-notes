---
title: "Old Timeline"
---

## Prehistory
### Age of Decay - ~10,000 years
* Convergence
* Primal beings come into existence on Menadi
* Elder Gods arrive

### Age of Renewal ~5,000 years
* Elder races are created from memories of old
* Ur-Dragon's spectral wings births dragons into the world
* Demiurge arrives and creates Ozon from Arda
* God Wars
* Elder Gods leave Menadi
* Humanity and dwarves emerge

### Age of Uncertainty ~5,000 years
* Minor gods begin to emerge
* Humanity spreads throughout the world
  * Nordic raiders spread via the seas and settle in new lands
* Dwarves arrive from Underdark
* Exiles of Quetza become the Sekar
  * They conquer and enslave the humans of Viland
  * They find Zoggattha and the Imperium is born
* Fabriki are made as a superior slave race to humans
  * Humanity living beneath Sekar rule is thrown into squalor and left to die
* Lloth arrives and creates the Adal
* Everwar
* Light Elves split into modern elves
  * Heart Trees are grown around the world to establish wood elf groves
* Human kingdom in Andalusia is founded (Hathor)
  * Pharaohs become liches post-entombment and rule massive subterranean empires
  * These Pharaohs are protected by the Hand of Akkad, a secret sect of magi and rangers
* Zoggatha returns to slumber and the Sekar fall into ruins after revolts

## Heroic Age
* Shoanti of Viland settle into various tribes
  * Bannorn settle in the lower lands

## Classical Age
* Etruscan Empire begins conquering new territory
  * Conquers southern Emos and northern Viland

## Modern Age
* Etruscan Empire is overthrown in Viland
* 3CE Talidor and the Lyceum is founded in Daikon










## Prehistory
### Age of Decay
??? - ???
: Eight worlds collide into one resulting in the death of billions and the formation of Menadi; event is referred to as the Convergence
: Surface of Menadi is in ruin, covered in the remains of fallen civilizations and corpses
: Fungal beings appear and begin consuming corpse and ruin alike, flourishing on the decay
: Other primal beings emerge during this time; namely the Archaeans, Substratals, Trilobites
---
### Arrival of the Elder Gods
??? - ???
: The seven Elder Gods awake, bringing their elements to the world and breathing new life into the now barren world
: Trilobites are drawn to Sidon as the embodiment of Light as the Substratals begin to worship Arda as the embodiment of the Earth
: War is waged against the fungaloids, resulting in them being driven deep beneath the world
: Using the memories of the fallen, Ira begins shaping the elder races; the Elves, Slann, Deep Janeen, etc.
: Ancient civilizations are built and the Elder Gods lead the world into an age of peace and harmony
---
### God Wars
??? - ???
: An entity beyond this world, known only as the Demiurge, arrives on Menadi and begins corrupting Arda
: Arda's mind fractures and the being known as Ozon is born; an embodiment of madness and destruction
: Orcs, trolls, goblins, and gnolls and various other dark races emerge from the destruction
: War for the survival of Mendai begins with the elder races pitted against the spawn of Ozon
: Ira is killed in the conflict and her body becoming the Paths
: The world is split from the calamity forming the three modern continents; Emos, Aurios, and Viland
: God Wars end with the imprisonment of Ozon and the banishment of Akam and Saba
: Near death, the remaining Elder Gods return to their Elder Warrens to recover and vowing to protect Menadi by removing their divine powers from it
---

: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id aliis narrare gestiant? At hoc in eo M. 
: Compensabatur, inquit, cum summis doloribus laetitia. Quid est enim aliud esse versutum? Certe non potest. 
: Poterat autem inpune; Bonum liberi: misera orbitas. Polycratem Samium felicem appellabant.
---
1067
: Memini vero, inquam; Ita multa dicunt, quae vix intellegam. Istam voluptatem perpetuam quis potest praestare 
: sapienti? Terram, mihi crede, ea lanx et maria deprimet.
---
1072
: Duo Reges: constructio interrete. Minime vero, inquit ille, consentit. Equidem e Cn. Sin laboramus, quis est, 
: qui alienae modum statuat industriae?
---
1089
: Aperiendum est igitur, quid sit voluptas; Hoc non est positum in nostra actione. Certe, nisi voluptatem tanti 
: aestimaretis. Itaque haec cum illis est dissensio, cum Peripateticis nulla sane.

## First Era
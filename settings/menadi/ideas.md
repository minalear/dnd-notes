---
title: "Ideas"
---

## Misc World Ideas
* Dismembered God - a god that was taken and dismembered into many parts
  * Cults are dedicated to "finding the missing pieces" and putting him back together to make him whole
* Vega, Thrice Executed
  * Mass murderer who has been captured and executed on three separate occasions, but was found to have "returned" from death each time
  * He is now held deep within the bowels of the Lyceum
* Many gods from the previous worlds lost their divine sparks which were lost within the Underdark
  * Many powerful entities value these souls and use them to increase their power
  * Jeremy's soul was taken by a devil known as Maloch
* Great warriors of a civilization, after their death, are grafted into magical suits of armor
  * It is so they can serve their society forever after
  * Main general could be one of these magical knights
* Black Knives imbued with the magic of death are the only things capable of killing divinity
* Patron Saints of various disciplines
  * Saint of Beer Brewing - convinced his monastery to drink beer instead of water during a plague which saved them
  * Gun Saint - story of Sorin
* Ikariam, the Unchained Fury
  * Soul of Ikarium from Edur reemerges on Menadi and begins wreaking havoc
  * His mind is fractured and is eternally fighting to free his father from the Azath House
* Clan of Bloodragers that utilize emotions to cast a special kind of magic
* Life & Death
  * when a mortal dies, their soul is drawn to Hood's Realm of Tyr Mari
  * the soul's memories and life experiences are cleansed within the Soul Forges until they are stripped of everything and cleaned once more
  * the exception of this cycle are the elves, whose souls are so intimately entwined with other warrens, that they travel there instead
    * Elven souls empower and imbue the warrens they travel to with their life experiences
    * This is the reason that elvish magic and society is so focused on their ancestors
    * Occasionally, these souls will leave the warren and are reborn into new forms on Menadi (Gathlain for example)
  * Souls are also often hijacked by other extradimensional beings, most notably devils from Hell, who stoke the fires of their war machines with the souls of the damned
  * Hood considers any circumvention of the natural order of life and death to be the greatest injustice in all of existence and actively works to undermine others who wish to avoid the cycle of life and death
* A Order of Knights who bear medallions containing the souls of the very first knights of that order
  * there's only a set amount, with trainees being raised when a knight perishes
  * could be a fallen order from an ancient time with the medallions being lost in their old fortress
* Tower of Prophecy
	* Ancient ziggurat that has the spirit of an forgotten oracle
	* Bestows blessings, curses, and can tell ones' future    
* Andula Wastes
  * Blasted hellscape of stone and fierce winds, barely any vegetation remains
  * Initially destroyed by an uncontrolled spell that destroyed the once lush gorge (spell of a Sekari?)
  * Forms a rift between two regions and is the site of many bloodied battles (Veryn and Shoanti?)
  * Said to be haunted by the ghosts of those soldiers slain
  * Very dangerous to travel
* Dark Woods
  * Haunted woods leading up to the Andula Wastes, said to be inhabited by werewolves and other nasty beings
  * The woods were protected by a cabal of witches from the destruction that ruined the wastes
  * It is said the mage that destroyed the wastes was once the husband of the lead witch
* Samsarian Sword Lords
  * Some of the world's most prestigious swordsmen
  * Dedicate their lives to the art of sword dueling
  * Many become sellswords while others maintain a pacifist life in their monastery in their homeland
* Rogue Monastery - secretive group of rogues that follow a small cabal of sorcerers
  * Protect some ancient evil/secret
  * Highly skilled rangers, thieves, and assassins
* Andarin Sages - powerful mages and scholars
  * They keep knowledge away from the masses, believing that only the elite should possess such powerful magics
  * Talidor was a former Andarin, believing knowledge should be available for all
  * One mage is entombed within an ancient structure, personally containing a demon
  * The Sages employ an army of soulbounded magical armor to act as anti-magic paladins
  * From the country of Andor
* Shardminds are the High Elf reincarnation versions of Gythlain
* Temple-Cities of the Seraphon form a global network of Leylines that allow Slann/Seraphon to channel magic from the Elder Warrens
  * Slann mage priests are the ones that maintain this leyline network
  * Was granted by the Old Gods
* The neutrality between the smaller trade republics and Mercia comes from the trading charters that would effectively block any warring nation from international trade
  * Mercia relies on the high bandwidth trade from the ATC and can't effectively conquer the smaller nations (yet)
* 

## Races
### Birdsss
* Race of bird warriors in the western continent that the Mercians are constantly dealing with
* Named the Vaalkerians, they are a flightless bird species with proud traditions

## Razmir
* Razmir gained his power by wishing upon the Starstone.  Since it behaves similar to Nanika, the next wisher will have to pay a terrible price for it.
  * Razmir was a research student, then going by the name of Lewis Lanceburg, when he encountered the artifact.
  * Going against regulations, he wished for the powers that eventually allowed him to conquer Razmiran.
  * Alternate idea is that he is seeking the Starstone wishing to actually become divine and that his current powers are his own.

## Characters
* Agrippa da Rippa
* Funky Student

* Hjelmdall
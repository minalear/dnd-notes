---
title: "Religion"
---

## Religions by Region
### Viland

* Etruscan Pantheon is still recognized by many locations in the northern regions of Viland, though their worship is seeing itself on the way out
  * Geodona is a Theocracy that recognized the Etruscan Pantheon as its state religion and the only true religion
  * Isger is also a bastion of Twin Flame worship
  * Pantheon is composed primarily of the Twin Flamed God and several minor deities
* After the fall of the Etruscan Empire, the regions located Caelondia and Daikon brought back their older deities that were banned during the occupation
  * These deities are descended from ancient Shoanti gods
* Telmar worship two gods primarily, an ancient god of death and fate
* Kirkland residents revere a "deity of the pit" that is said to be the origin of their curse and they try to appease it to alleviate their suffering


* Most deities of Viland extend from the worship of the gods established by the Etruscan Empire or the ones established after their fall
* Geodona still worship the Etruscan Pantheon and insist that is as far as their allegiances go.  This still rubs their neighbors the wrong way.
* Daikon and Veryn establish the worship of their old gods
### Emos
### Aurios

## Notes
* Etrusca worships the three fire gods and refers to their religion as the Holy Flames
  * The three flames represent different aspects of fire; warmth, destruction, and reconstruction
  * Geodana is a church ran state that still practices the Holy Flames
  * 
* Mercia establishes the Church of Saros, the God of the Sun
  * The declare that Saros is the one true god and others are false pretender gods needing to be snuffed out
  * This stigma has lead the rest of the world that follow Saros to be ashamed/hide their faith
* Both Saros and the Holy Flames both evolved from the ancient worship of the Old God Sidon
  * Aspects of her still reside within the realm of light and fire, which is where these entities manifest

## Ideas
* Arkhan, the God of Magic, limits the amount of magical energies allowed to be channeled through warrens by traditional means
  * Wizard spells are contracts with Arkhan to access this power and are well regulated
  * Sorcerers and divine casters bypass this by accessing their magic through gates or granted by their own deities
  * Arkhan sees himself as bringing magic to the common masses and no longer limited to those born with gifts or granted by fickle gods
  * Rogue Monastery sees Arkhan as a god of ultimate evil and should be destroyed, disallowing the use of magic by the common masses
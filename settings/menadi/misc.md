---
title: "Misc"
draft: false
---

## World Features
* Vast desert with the coast ruled by crusader type foreign lords
	* Interior still controlled by nomadic natives
	* They revere the ancient empire
	* Undead pharaohs live deep beneath the desert in lost catacombs, fighting each other in eternal warfare
* Empire spreading through the lands by using advanced military strategies
	* Utilizes skilled assassins to initiate their sieges
* Dark Elves/High Elves/Wood Elves
	* Witch King
* Ancient saurus warriors that live deep in the jungle
* Under Empire similar to the Underdark
	* Skaven

## Other

### Active Attunement Nexus Locations
1. The Nexus of Crimson Skies - Storval - Located on a high mountaintop, this Attunement Nexus is surrounded by a perpetual storm that turns the skies crimson. Lightning frequently strikes the mirrored towers, making this location a dangerous one.
2. The Nexus of Fungal Caverns - Myrwood - This Attunement Nexus is located within a labyrinthine network of caverns, where the air is thick with spores and the ground is covered in a layer of spongy fungal growth. The caverns are home to a variety of mushroom creatures, some of which are friendly to visitors, while others are fiercely territorial.
3. The Nexus of Sunken City - Ruby Sea - This Attunement Nexus is located beneath the waves of the ocean, within the ruins of a sunken city. The towers are encrusted with coral and surrounded by schools of bioluminescent fish. The Nexus is guarded by aquatic creatures.
4. The Nexus of Blackwood Forest - Fox Reach - This Attunement Nexus is located deep within a dark and twisted forest. The towers are carved from blackwood trees that grow unnaturally straight and tall. The Nexus is guarded by a tribe of shadow creatures that worship the Sekar.

### Deactivated Attunement Nexus
1. The Nexus of the Badlands - Caelondia - This Attunement Nexus is located in a vast desert where the wind creates a constant howling sound, as if the dunes themselves are whispering secrets. The Nexus towers are partially buried in the sand, making them difficult to find.
1. The Nexus of the Frozen Peak - Kirkland - This Attunement Nexus is located at the top of a treacherous mountain, where the air is thin and the snow never melts. The towers are made of ice and constantly emit a chilling aura. The Nexus is guarded by ice elementals.
1. The Nexus of the Frozen Dreams - Telmar - Located in a frigid tundra, this Attunement Nexus is surrounded by a field of ice sculptures that seem to come to life at night. The Nexus itself is said to be able to bring about powerful winter storms.
1. The Nexus of Endless Sands - Bassham - Located in a vast desert, this Attunement Nexus is housed in a massive sandstone tower that reaches high into the sky. The shifting sands around it are said to conceal ancient treasures.
1. The Nexus of Whispering Caverns - Sumera - Located deep underground, this Attunement Nexus is hidden within a labyrinth of winding tunnels and caverns. The mirrored towers are carved from glowing crystals that emit a soft, eerie light.
1. The Nexus of Howling Winds - Andula Wastes - Located on a high plateau, this Attunement Nexus is constantly battered by fierce winds that howl through the mountain passes. The mirrored towers are made of a dark, stormy metal that seems to absorb the light.

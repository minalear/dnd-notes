---
title: "History"
draft: false
---

# Menadi Story
## Prehistory
* Aftermath of the Convergence, world is destroyed and rotting
* Sentient fungal life emerges, begins to break down the death and decay, recyling matter to be used once again
* Trilobites exist, lost and without hope, before the arrival of the Elder Gods.
* Elder Gods awaken, trilobites worship Sidon, she brings war to the fungal people and drive them beneath the world

### Elder Races
* Elder Gods craft new races in their vision.
* Ira creates the Slann and Sidon creates the Elves.
* In the north, the Seraphon races flourish, with the Slann mastering powerful magics and eventually teaching the elves the power of warrens
* Light Elf civilization flourishes in Aurios, mastering magic from Tyr Voma

### God Wars
* Arda becomes corrupted by the Demiurge, becomes Ozon; sparks the God Wars
* Ozon, Akam, and Saba fight against the rest with Hood remaining neutral
* Elves, Trilobites, Seraphon, and other elder races fight on the side of light
* Ozon is imprisoned beneath the earth in Tyr Thevalo; Akam's body is destroyed along with his divinity in Tyr Melici (Underdark); and Saba is exiled to Tyr Veno
* Remaining elder gods leave Menadi to their respective warrens, vowing to keep true divinity from walking the lands ever again
* Trilobites vow to keep their goddesses' mission alive and plunge into the Underdark to fight the forces of evil and darkness
* With the disappearance of the elder gods, and their divine prescence no longer supressing Menadi, minor gods began to appear throughout the world

## Classical Era
* Sekari discover Zoggatha and found the Sekar Imperium, marking the beginning of written history
* Portals appear in the frigid north from another world (Tyr Thevalo), marking the arrival of humankind
  * Humans were created by the twisted dreams of Ozon from their memories of the world that was
  * Humans spread through the lands, many are later captured and enslaved by the Sekar Imperium
* Dwarves awaken in the Underdark, having hearing the "Toll of the Hammer" which they attribute to their god Andross awaking them
  * First Kingdom of the Dwarves was founded in Shanatar
* Adal are created by a reawoken Lloth and march upon the surface war, sparking the Aelven Wars

### Aelven Wars
* One of the most destructive wars the planet has ever seen, with many Light Elven cities wiped from the surface of the world
* Light Elves, alongside allied dwarves and humans, are barely able to fight back the Adal, causing them to retreat into the Underdark
* Their hatred for the surface races boils as Lloth recupperates and prepares for a second, and final, assault on the surface
* Due to the destruction and shock from the war, the Light Elves lose their connection to Tyr Voma and effectively split into various races
  * The Elven race now pulls their magic from a path, having lost their connection to the elder warrens.
  * The high elves develop a new kind of magic, hoping to reconnect to Tyr Voma, by utilizing magical crystals
  * Several elves felt this was blasphemy and the elven race splits into the modern day incarnations

## Middle Ages
* Zoggatha returns to his slumber and soon after the Sekar Imperium falls to various revolts, freeing the humans of Viland
* Shoanti tribes form throughout the Storval Plateau and the northern coast
* Over the next few hundreds years, the various human tribes develop into kingdoms, with the most powerful being Veryn
  * Teryns are various lords that hold tribal lands throughout the northern forests of Viland
  * They were united by the first King of Veryn, cementing the northern Viland humans into Fuedalism rather than Barbarism
* Veryn fails to conquer the Storval Plateau and perishes in the conflict, leading to animosity between the two groups of humans
* Weakened from their failed war, the Etruscan Empire from across the sea makes land and wipes up the broken kingdoms, cementing it as a world empire

### High Kingdoms
* Evil Kingdom that bred Orcs as warriors for slaughter and conquest
* Neutral Kingdom that made the Hivers by genetically modifying Antlings with lost technology found within the Underdark
* Dwarves migrate, leaving Old Shanatar, for the surface mountains in Emos and Aurios

## Modern Times
* Merican Empire is founded by the First Phoenix King
* Mercian Empire expands, felling old kingdoms and conquering much of the central landmass in Emos
* Etrusca falls and Viland is freed by various rebelling factions working together

### Industrial Era
* Aspis Trading Company becomes a major player in world economics, being financially backed by the Mercians
* Science and engineering evolves and steam powered machines become popular, revolutionizing many industries, and causing mass poverty and manual labor is transformed by these new machines
* Early forms of firearms are developed and deployed, revolutionizing the battlefield







# OLD Quick History
1. Convergence happens, destroying five worlds and forcing them into one; Menadi
1. Death and decay cover the world
1. A progenitor race creates the fungonids, a race of fungus like entities that feed off the death
1. The Elder Gods arrive into the world, having been slumbering for a thousand years
1. Dismayed at the state of the world, they create the elder races; Trilobytes, Seraphon, and Elves to combat the fungi
1. The fungi are driven beneath the world, into the Underdark, where the horror lies
1. Arda becomes corrupted and becomes Ozon, triggers a war with the other elder gods
1. Ozon creates the "dark" races like orcs, goblins, trolls



# OLD History of Edur
## Convergence
* A cataclysmic event causes many parallel universes to converge into one, causing massive amounts of destruction in all of them.
* Many souls of these worlds were pulled into the Well of Eternity, leading them to be rebirthed into the new world.
  * Various gods and deities of these worlds had their divine sparks ripped from them, causing their souls to return to normal as well.
  * Many of these souls have imprinted "memories" of their past lives that occasionaly resurface during their lives.
* Mother Dark was destroyed leading to an influx of Void Prescence into Edur.
  * Edur is the world where all other worlds overlapped.
  * Many locations on Edur were buried and fell into the Underdark.
* The converged world came to be known as Menadi which came to house many elements from the previous worlds.

### Survivors
* Lloth survived by being protected by the nightmare of Leng and was able to found her empire once more deep within the Underdark and creating the Aelf-Adal from the souls of the Drow.
* Ramos' soul hijacked the soul of another, a pretend god named Razmir, and reappeared in Edur with memories of both him and the false prophet.
* Nurgal was protected in the Abyss, the only plane that was not affected by the Calamity.  He emerged to reclaim his rightful title as God of the Sun.
* The adventurers that triggered the Calamity emerged thousands of years later in the Underdark among the Aelf-Adal.
* The Hunter Association, having been headquartered in the Dreamlands, was able to survive the Calamity.  Many of the members left but a few remained, operating in the shadows.
* The leadership of the Aspis Consortium also survived by being in the dream and they were able to found their company once more in the new world.

## Aelven Wars
* Early in the history of Edur, the Aelf-Adal attacked the surface world at the behest of Lloth, their Queen of Nightmares.  
* Only by the collective efforts of the Seraphon and Mer were the dark elves able to be driven off, back to their blackened cities deep beneath the surface.
* The nightmares of this war still reside in the memories of the longer lived races, such as the High Elves.
* These nightmares give fuel and fire to the hatred of the Aelf-Adal who wish to return to conquer the surface.

## Arrival of Man
* Man arrived in the continent of Emos from the frigid Nordlands and began settling the lands there.
  * They met a clan of wood elves who befriended the new people.
* The Nordlings that arrived in Aurios were quickly dispatched and killed by the Seraphon native to the northern jungles there.
* The Nordlings were fleeing the grip of the Iron King, an evil lich that resides at the most northern tip of the world.
  * The Iron King's terrible fortress appears as a black iron spire jutting out of the frigid wastes
  * The Iron King is said to be the first being who awoke in the new world, some evil from the World That Was.

## Emergence of the Dwarves
* The dwarven people were born of rock from the upper reaches of the Underdark, being freed from a curse long forgotten.
  * They arrived on the surface near the time human arrives as well
  * Elves began associating the two new races as being one and the same, leading to them being collectively called "man"
  * Dwarves attribute their creation to their patron deity Andross
* The Dvargir (formerly the Duergar that followed Assog the Defiler), no longer consider themselves kin to dwarves, but something completely new.
  * Ravenous beings dedicated to work and the production of more work.  Working towards this unified future, nothing else matters, no man nor god can sway them from their self appointed destiny.
  * The Dvargir work to dig deeper and deeper into Edur for something they can't quite recall.  Their dig is called the "Ascent"
  * They are working to be working.  Work is the plan.  Work is the point.

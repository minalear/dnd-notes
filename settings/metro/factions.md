---
title: "Factions"
---

## Major Factions
* The Administration - American government lead by a Ronald Reagan AI seeking to assert its influence over the metro through police action.
  * Currently headquartered in the old US Embassy and have control over the Napier, Orchard Boulevard, and Orchard stations.
  * At war with the Fourth Reich and have a tenuous alliance with the Commonwealth
* Fourth Reich - splinter American faction that exists in the northern fringes of the metro
  * Neo-nazis that wish to rule the metro and purge the inhabitants of those they deem impure
* Commonwealth - series of united stations along the East West Line that maintains a strong trading network
  * Largest faction and has a tight control over their resources
  * Heavily militarized but is generally apathetic to conflicts occurring elsewhere in the metro
* Iron Union - has complete dominion over the western portion of the metro, due to their choke hold over the Jurong East station
  * They are incredibly paranoid of foreigners and visitors, having strict control of movement in and out of their territory
  * The western part of the island has vast quantities of resources, making the Iron Union a target by many other factions
* Red Vanguard - communist faction that has a mix of Chinese and Stalinist inspiration
  * Bitter enemies with the American and Nazi factions and despises both the Commonwealth and the Iron Union
  * Treaties with the Commonwealth to ensure trade between the factions

## Minor Factions
* Syndicate - Black market trading network that smuggles goods in and out of the Commonwealth
  * Typically charge exorbitant prices for luxury items for those denizens that live outside of the Commonwealth
* University - group dedicated to preserving knowledge and technology within the metro
  * Established beneath the National University of Singapore
  * Well respected by most factions and are typically seen as a neutral party to most conflicts
* Bukit Heads - militarized fanatics hailing from Bukit Batok//Bukit Gombak
  * They wear buckets on their head to hide their mutations and are very capable warriors
  * Typically act as private military for factions within the Metro, but will "heed the call of the hill" if their home is under threat
  * Must survive a year on the surface in the Bukit Panjang Camp during their training
* Scientologists

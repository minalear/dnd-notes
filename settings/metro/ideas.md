---
title: "Ideas"
---

* Currency should be 5.56mm cartridges due to the most popular weaponry being the SAR 21 and the BR18.
  * Both the SAR 21 and BR18 were manufactured in Singapore and are the most prevalent weapons in most caches
  * Both guns use the same magazine

## Singapore Island
* Jurong Lake has massively flooded due to sea levels rising.
  * This has caused parts of Jurong and Tengah to become flooded as well, bisecting Singapore at Jurong East
  * The Iron Union has massive water pumps working 24/7 to keep their homes free of the water, but have been working on clearing the natural dams that have formed to help divert the water into the central water catchment.
  * This could become a late game mission if the players wish to gain access to Tengah West and the Technological University

## End Game
* Players end goal eventually could be to find a way to escape Singapore and to find a better home away from the metro.
* Tengah West could be the location of the remaining Singapore Military which have been awaiting the all clear from a foreign power
  * EYE?
* Nanyang Technological University could be the final showdown with the Scientologists and whatever they've been scheming
  * This was the location of Archon and The Messenger, so could be the source of supranatural phenomenon for the metro and greater Singapore
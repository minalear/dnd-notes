---
title: "Setup"
---

## Cydonia Redux
* Cydonia is the second version of the original Knights of Cydonia setting
* Many elements are carried over, including the crashed colonists trying to survive in an inhospitable environment
* Many aspects are changing to make the setting a bit more serious and to add more variety to the environments
* More inspiration will be pulled from Kenshi and Alien to setup more intrigue for the setting as well

## Premise
* Centuries ago, a colony ship from Earth crash landed on a moon that orbits the planet known as Paradise
* The destination of the colony ship was to be Paradise but due to some complications, the ship was forced to emergency crash onto the desolate and inhospitable moon instead
* The moon was later dubbed Cydonia, a reference now long forgotten by the descendants of the original survivors
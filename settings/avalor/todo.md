---
title: "To-do"
---

* Create cultural groups and languages
  * The successor kingdoms to the old empire share the same alphabet but their spoken languages differ
  * They all descend from the same parent language; Dantalid
* Determine timeline and ages
  * Update age references on the Dantalid Empire page
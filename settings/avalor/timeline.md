---
title: "Timeline"
---

* Dantalid Empire ruled north western Avistan
  * Dantalis Knights conquer the northern lands of Avistan
  * Warriors of Nen, ancient tribes that lived in the hills of Pryd before the empire, fail to hold back the Imperial invasion
  * Many of their notable warriors are buried within the Barrowdun
  * Ancient wall demarcating the border of Nen still exists within the Witchwood
* Wraithwood appears, destroying the central empire
  * Dryads of the Eternal Circle summoned an ancient, primeval being that summoned the woods from the Rift
  * The Rift is a natural realm where nature spirits reside
  * The Wraithwood now acts as a physical border between the material world and the Rift
* Empire provinces fight to fill the power vacuum
  * Border conflicts occur between Pryd and Dravania
  * Pryd establishes a militarized border to help maintain order that is maintained to the modern day
* Valta invades Pryd in the first Valtan War
  * Recruits Dravanian mercenaries that capture the northern counties and the capital city of Pryd
  * Dravanian War Chief is assassinated, leading to a collapse of force organization of the Dravanian
  * Prydish soldiers are able to repel the invaders
* Son of John II allies with Valta to trigger a rebellion and subsequent invasion of Valta, causing the Second Valtan War
  * Lord Protector defends against the peasant armies and Valtan invaders
  * John II abdicates the throne after victory to become a wandering saint
  * Frederick Cromwell becomes the new Lord Protector whose line still controls the title
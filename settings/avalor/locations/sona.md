---
title: "Sona"
---

* Sona is a city built on the white cliffs of Galadia
* City is broken between up between three major districts;
  * The Summit - built on top of the cliffs and the location of most of the important government buildings and the wealthy elite
  * The Shore - the majority of the city lives beneath the cliffs along the shoreline.  Notably the location of the docks and industrial districts along with the majority of residential neighborhoods
  * The Undercity - located beneath the large bridge that leads into the Defiance Bay* and is home to the seedy underbelly of the city

## History
* Sona is a city built by adventurers of the Iron Isles seeking their own fortune on the main land
* These adventurers were lead by a man named Alcaydian Indros
* Built their settlement near the old bridge, which was a significant part of Iron Isles mythos
  * Claiming that the bridge was an old portal to the creators home that has since fallen to decay
* The settlement was built at the top of the massive cliff near the bridge span to protect it from marauding pirates from the south
* The original fort still stands today, expanded since the days of Idros, and named Stromgarde Keep
* Since the founding, the descendants of these original adventurers have become noble houses that hold considerable sway within the city
* Merchant clans and lords have also established major stakes in the city and make up the other half of the wealthy elite that live atop the cliffs of Galadia.

## Culture
* The culture of Sona is a distinct blend of the Iron Isles and mainland cultures
* Major focus on the sea and sea based cuisine, sea based industries, and sea based religions
* 

## Government
* Sona is governed by a number of bodies, most notably the Council of Ushers, the Office of the Lord-Mayor, and the Justice Court.

### The Council of Ushers
* The Council of Ushers is composed of mostly hereditary seats of nobles and rotating seats of various merchant families and guilds, along with a smaller number of electoral seats that represent the common people.
* The Council of Ushers is a typical mess of political infighting and corruption.  
* Many members tend to join a political party to ensure that their goals are achieved.

### The Office of the Lord-Mayor
* The Office of the Lord-Mayor is a democratically elected positions elected by the people of Sona.
* The Lord-Mayor is elected for life and can only be removed by the highest Justice in the Justice Court.
* Typically seen as having near limitless executive power, the Lord-Mayor is a king but in name.
* Officially the Lord-Mayor is there to see the execution of the laws passed by the Council, but due to the inefficient nature of the Ushers, the Lord-Mayor typically rules through executive orders and through the city guard.
* As long as the Lord-Mayor doesn't overstep their bounds and offends the council, they typically do not care as long as they are able to benefit from the Lord-Mayor's actions.

### The Justice Court
* The Justice Court handles the legal arbitration, civil, and criminal cases for the city.
* Composed of Thirteen Justices, as well as numerous lower judges and courts, the Justice Court is seen as the highest law within the city.
* Due to the prestigious nature of the Court, many other city-states refer to the Justice Court in cases that needs a third party arbitrator; like in cases between two other city-states.

### The Hells
* The Hells is the large, prison complex located in the Shore
* Modeled after the mythological representation of Hell, with seven layers constructed deep beneath ground
* Each lower level houses the more deranged and dangerous of criminals

## Locations  
### The Summit
* The Summit is home to the most affluent and influential people within Sona, in addition to the majority of government buildings such as the Lord-Mayor's estate and the Hall of Ushers.
* **Alabaster District** - the richest and most prestigious district in all of Sona.
  * Home to the elite and cut off from the rest of the city by a large marble wall, echoing the motif of the white cliffs themselves.
  * The district's most prominent feature is **Stromgarde Keep** which looks out over Defiance Bay.
  * In addition, there is the **Serpent's Run**, a massive hippodrome that can seat up to 5,000 people at once.
  * House of Lords
    * Not necessarily an official government body, but the seven major noble families keep offices here where the public can come to air grievances, beg favors, or otherwise interact with representatives of the city's nobility.
    * Many of the more minor families and merchant clans do not keep offices here, but can for a small fee (which none bother).
* **Bridgeward** - located near the bridge, it is home to many high end artisans such as jewelers, sculptors, and clothiers.
  * In addition, it is home to the Golemworks, a workshop that specializes in bespoke golems for the wealthy elite of Sona
    * These golems fetch an incredibly high price and are one of the city's most prized exports
  * The **Cenotaph** is located here as well, a standing monument dedicated to the city's first Lord-Mayor and folk hero Indros Sona
    * Has become a local cemetery to those families with deep connections to the hero and the city in general
* **Capital District** - seat of Sona's government
  * Political heart of the entire city
  * Usher's Hall, home to the Council of Ushers
  * Pediment Building, home to the Justice Court
* **Naos** - the "new money district" and home to many playhouses, concert halls, and art houses
  * Lord-Mayor's Menagerie, the Lord-Mayor's private collection of exotic animals he puts on display for the public
  * Starsilver Plaza, large public space that is a gathering of many of the city's denizens and hosts many events throughout the year.  Has a spectacular view of the city due to its proximity near the cliffs.
    * Location of Sona'a most famous theater, the **Triodea**.
  * Heidmarch Manor - private estate used by a secretive organization of "adventurers"
* **Vista** - home to various top-tier shops, restaurants, and other businesses.
  * Current Lord-Mayor's eight story castle/mansion 
* **Grand Arch** - largest district in the Summit and home to many middle class and shopkeepers, and various affluent foreigners.
  * Named after two Watchers, known as the Guardians, facing one another with weapons held aloft, forming a gigantic arch that can be viewed from miles around.

### The Shore

### The Undercity

### The Bridge
* There is a partially collapsed bridge of gargantuan proportions that extends from the Galadian cliff face into Defiance Bay
* The bridge is held aloft by several Watchers and several others can still be seen partially destroyed where the bridge used to stretch further into the ocean
  * The Watchers that hold the bridge aloft are some of the largest ones found, being ten times larger than many other Watchers found throughout Sona and the Iron Isles
* Many believe the bridge was constructed by the elder race as a portal to their home world and that when the bridge collapsed, the portal's connection was severed (or vice versa).
  * This is many scholar's beliefs on why the elder race is no longer present.

### Stromgarde Keep
* Stromgarde Keep sits at the best position along the Galadian cliffs to oversee the bay and the city
* Has historically acted as a deterrent to any pirates that might be thinking of attacking the city
* The fort bristles with siege weapons and bowmen

### Serpent's Run
* Massive hippodrome located in the Alabaster District of the city
* Can seat up to 5,000 people
* Used to host various races and other non-bloodsports
  * Blood sports have been effectively banned due to some incident in the past resulting in the death of a young noble lord and an escaped minotaur

## Ideas
* Contingent of Hellknights help maintain order in the city
  * Requested by the Justice Court after a smattering of political unrest that the city guard failed to properly maintain
  * They help protect the districts that the city watch largely ignore due to logistical issues
  * These Hellknights are few but incredibly skilled in martial combat
  * Their headquarters are within the Bastion of the Nail, a minor fortification located in the Shore
* Arvensoar
  * 400 ft tower that connects the Shore with the Summit, boasting a large elevator that is used by various organizations throughout the city
  * The tower boasts a number of trebuchet that gives the tower the ability to protect the city from land and sea based assaults
* Djikstra's Castle
  * A shantytown of sorts in the Shore, more than 200 buildings all owned by Djikstra and fortified
  * Acts as a capital of his minor criminal organization
  * Lives a very wealthy life, kept protected by the castle's makeshift fortifications
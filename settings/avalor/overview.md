---
title: "Overview"
---

* Tembley located within the Barrodun within the county whose capital is Chadwick
* Pryd is a kingdom and former grand duchy that is lead by a lord protector
* To the east is the dryad controlled forest of the Witchwood that is very dangerous
  * Dryads keep to themselves as long as their woods remain untouched
  * Dryads are constantly at war with Caladoria to the south due to this
* Beyond the Witchwood is the land of Mythralis, begotten by time and overrun by creatures of the dark
  * Ruled by a caste of vampires that subjugate the remaining mortal populations and uses them like livestock
* To the south is Caladoria, a nation of merchant ran city-states that have a strong alliance of mutual defense
* Caladoria is the border between the temperate north and the arid south
  * Grand Desert of Kharadia ensures that adventuring to the south is treacherous
* Storm riddled coast to the west ensures that boat travel remains relatively local
* To the north is the frigid lands of Dravania
  * Free peoples, pirates, and others
  * Border land between Dravania and Pryd are heavily fortified and is known as the Black Wall
* 
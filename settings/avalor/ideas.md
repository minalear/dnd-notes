---
title: "Ideas"
---

## Seven Cities
* The northern provinces are mostly comprised of ten large city states found north of the Vistula River
* Sona
  * White castle with many brightly colored roofs
  * Notable for the many noble houses and their form of governance
  * Built into the side of some alabaster cliffs
  * Big fuckin' bridge
    * Ancient connection to another world that was severed
    * Big pillars are big dudes, similar to those from the Iron Isles
  * Similar culture to the Iron Isles, but more mainland
* Iron Isles
  * One minor nation located on an archipelago
  * Colossus of Rhodes
    * Titans of water that have become dormant
  * Major naval power and trading hub
  * Extreme neutrality and hostile towards piracy
* Novigrad*  
  * Mountainous and remote city in the far north
  * Notable for its mages guild  
  * Home of the Eternal Flame*
  * EVIL?
  * Built on top of the Lost City of Imcando*
  * Pretty racist
* Carda
  * Southern most city state of the northern provinces
  * Borders the mouth the Vistula river
  * South of Carda lies the great Andalusian desert 
  * Many of the towns and villages along the Vistula rely on Carda for protection
  * Major source of drugs
* Noxus*
  * Militarized city located in the north that is constantly at war with Dravania
  * Is supported by the other free states in exchange for not killing him
* Winelands*
  * Located in a rich river valley
  * Moderate temperatures makes this a bread basket region and notable for its vast vineyards
  * Focused on the arts; bard college
* Greywater*
  * City of trees near a dryad controlled forest
  * Perpetually at war with them due to the city's timber industry

## Other Ideas
* Southern empire founded by half-elves
  * Full blooded elves tend to hate half-elves as they believe they have diluted their divine blood and cost them their immortal lives
  * Due to this, the half-elves also hate full blooded elves
* Dravanian horde lives north of the mountains that come down to raid the towns in the south
* Vaalkerian, bird-like people from the aeries of the Mithral Peaks
* Watchers
  * Looked too alien and over the centuries, people have modified them to be more human like
  * The massive watchers holding up the Irespan still look foreign and weird
* Rogue Traders are massive ships and fleets that don't ally with any single nationality 
  * Moving from port to port, they don't call any single port home
  * Various cities make deals with these traders since they are entire moving economies by themselves
  * Many have private islands within the Iron Isles where they create fortresses to help safeguard their wealth
* Milaxia - feared witch of the woods



## Pryd
* Pryd is a small kingdom on the continent of Avistan.
* It is bordered by Dravania to the north, Caladoria to the south, Mythralis to the east, and the Serelis Sea to the west.
* Ruled by the Cromwell Family who earned the title by inheriting the Earldom that was Pryd
  * The family never fully inherited the title of king, preferring Lord or Lord Protector
* Pryd was mostly rural farmlands during the empire's reign, but a number of urban centers have cropped up in the ~1300 years since
* The typical demographics of Pryd is;
  * 91% Human
  * 5% Halfling
  * 3% Dwarf
  * 1% other

### Utrecht
* Capital is Utrecht which lies upriver on the River Vryn, at the junction of Vryn and Lesser Vryn
* Utrecht itself is a fortified citadel that was a major bastion defending the River Vryn from invaders in the old empire days
* Greater Utrecht refers to the surrounding villages and farmlands of Utrecht
  * Greater Utrecht is the single most populated center within Pryd boasting nearly 72,000 population
  * Utrecht itself is populated by 28,000

## Wraithwood
* The Wraithwood is a large marshy forest that forms the border between Pryd and Mythralis
* The forest is said to have been conjured by dryads who were seeking revenge for the destruction of their home by the old empire
* The old capital of the empire, Dantalis, was swallowed by the forest along with the empire's nobility and armies
* The surviving regions of the empire effectively became independent overnight

## Dravania
* Militaristic kingdom north of Pryd
* Used to be a region under the old empire that traded political independence for military service
* With the old empire gone, their militaristic nature tends to end with border conflicts with their neighbors 

## Mythralis
* Mysterious kingdom found across the Wraithwood
* Said to be full of evil witches and demons

## Misc
* Lost Dwarven City of Imcando
  * Many sections of the city have fallen to ruin or have been completely cut off due to cave-ins
* Trolls are named after the first thing they try to eat (ex. Dad, My Arm, Small Girl, Drunken Dwarf)
* Dragons were made by Dragonkin, servants of the dark lord
  * Different colors exemplify different personalities
  * Gold dragons are kind hearted and disowned their creators, now living among humans in disguise
* Avalon, island at the center of a great lake where heroes are said to go when they perish, whether in battle or of old age
  * only grand heroes with a story worth telling are beckoned to the island
  * at the center of the island is a church to mithra and from there the heroes live forever in peace
  * souls of these heroes find their way through a grand forest to the shores of the lake where they meet the boat maiden and her boat with a black cloth awaiting them
* The god of the elves granted them long life by fragmenting parts of herself and giving them those pieces
  * Realized that the elves are now cursed to be near her at all times
  * Now in the modern era with the elven god gone, the elves no longer have immortal life
* Sorcerer rejects (who have been kicked out of school) are often recruited by secret service groups to become trained assassins

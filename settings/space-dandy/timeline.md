---
title: "Timeline"
---

## 2100s
* Earth governments and private companies begin establishing offworld colonies as FTL travel becomes reliable and affordable
  * These colonies still take many years to reach and require substantial support during their founding
  * Colony ships are massive and require a minimum of a decade to construct before being launched
  * Each colony ship seeds 100,000 people onto new worlds and provides the basic needs of a burgeoning civilization to establish itself
* Space colonization reaches its peak when the Marlowe Corporation launches 10 colony ships simultaneously into the stars
* The Messenger arrives on Earth and is captured by the US military for study

### 2197 - The Gray Haze
* Due to the experimentations, a clone of the Messenger brings about a nuclear winter referred to as the Gray Haze upon the world and cuts it off from the rest of the galaxy for 200 years.
* During this time of silence, several colonies collapse, but others struggle through and establish permanent footholds within their new solar systems.
* New governments are established and many colonies band together to form small federations.
* Over the 200 years of silence, they become fully autonomous, unsure of what happened to their parent planet.
  * A few attempts to send ships back to Earth failed due to the Gray Haze, leaving the colonies completely unsure as to what happened

## 2300s
* Various multinational corporations and governments work together to break through the Gray Haze, allowing for Earth to reestablish communications with the stars
* The local governments are not happy to have lost control over their colonies and work to reestablish control.
* Due to the sheer distance and time required to travel to the colonies, the endeavour falls flat
* Scientists working for SolarStar discover Warpspace and Hyperlane travel
  * The first experiment into improved FTL engines fails, resulting in the ship Event Horizon to enter "The Warp"
  * Though the entire crew perishes on the Event Horizon, scientists discover a creature on the ship they dubbed the "scuttlebug"
  * These bugs are used to tear a hole into the Warp that ships can use to enter/leave freely
* Early excursions into the Warp are incredibly dangerous, but eventually scientists discover the Hyperlanes, a stable, permanent network of passageways through the Warp that can be used to travel great distances
  * The Hyperlane is theorized to have been constructed by some ancient aliens

### Interplanetary Wars
* With the advent of Hyperlanes, Earth ships can travel to and from the human colonies at a fraction of the time
* This leads to hostilities as the Earth governments again try to reestablish control over these offworld societies
* Wars break out that last 20 years with many colonist and terran native perishing
  * Several devastating terrorist attacks on Earth result in the collapse of several governments
  * These attacks utilized warp engine explosions to wipe out entire countries
* Fearing the collapse of the human race, several remaining colonial powers and the United Nations convene and establish the UAP; the United Authority of Planets, to act as a federated government over all colonies and Earth itself
  * The United Nations is dissolved with the UAP taking a more direct control over Earth politics
  * The UAP leadership are elected by every major power, resulting in a balanced government that attempts to benefit all of humanity

## 2500s
* Humanity is exploring the stars much further than they have ever done before, expanding well beyond Orion's Belt into the local galactic cluster
* It is here where humanity begins to meet and interact with different xeno civilizations

* **2553** - The Damascus Incident
  * An alien species known as the Egregorians are found, the first non-human species native to the Orion Arm
  * It was discovered that the Smith-Shimano Corporation were trying to hide the incident, but due to the involvement of a LANCER wing
  * The planet were host to a number of ancient machines and AI that were trying to eradicate the native population
  * UAP has taken direct control of the colony and has dissolved Landmark Colonial, a subsidiary of SSC
  * It seems the war machines date back to the Interplanetary Wars of the 2400s

## Timeline
* **2107** - First human colony ship is launched; destined for Angora
* **2142** - Marlowe Corporation launches 10 ships simultaneously
  * **2148** - Cydonia is founded as a secret experiment with the colony ship being reported as destroyed by unknown phenomena
* **2177** - The Messenger arrives to Earth and is captured by the US Military
* **2179** - Clone of the Messenger brings about a nuclear winter, causing the **Gray Haze**, leading to 259 years of isolation
* **2438** - Earth breaks through the **Gray Haze** and reestablishes contact with colonies
* **2450 - 2552** - **Interplanetary Wars**
  * **2552** - UAP is founded in the closing hours of the war to reunite the human colonies
* **2901** - Humanity makes first contact with aliens from the Persus Arm
* **3118** - **The Damascus Incident**



## Timeline
### First Expansion - 2107&mdash;2282
* **2107** - the first of twelve colony ships are launched into space from Earth.
  * Using traditional booster technology, these ships were expected to take hundreds of years to reach their destination.
  * These colony ships contained primitive androids as well as human embryos destined to establish new enclaves of humanity in the stars.
  * Over the intervening years, many of these ships would lose contact with Earth and were never recovered.
  * 
### Second Expansion - 2283&mdash;2349
* Alcubierre Drive is invented allowing for FTL travel, reducing travel time from centuries to years.
* First Ring expansion followed by massive colonization of the first human colonies.
* **2348** - The Messenger arrives to Earth and is captured by the US military.

### The Fall - 2350&mdash;2732
* **2350** - Military clones The Messenger and inadvertently creates The Hubris who brings about a nuclear winter.
* The Gray Haze encompasses the Earth, cutting it off from the burgeoning human colonies for centuries.
* Humanity nearly collapses with many colonies withering and minor feudal wars breaking out.

### Interplanetary Wars - 2733&mdash;2849
* The Enigmatic teaches humanity about hyperspace, allowing them to break through the Gray Haze.
* Over the following years, Earth tries to reestablish dominance over her lost colonies, many of which have become independent.
* War breaks out through the first ring resulting in even more human death.
* The Earth Alliance is dissolved and replaced with the United Authority of Planets (Union), a federal government that unites all of humanity's planets.

### Third Expansion - 2850&mdash;3012
* Golden Age of the UAP where colonial efforts are renewed, allowing for colonization of the second and third rings.
* Hyperlanes are discovered, allowing for rapid transit between fixed points within the solar system, establishing interstellar highways.

### Fourth Expansion and First Contact - 3013&mdash;3213
* **3013**-**3014** - First Contact war with the Valtorian aliens
  * Caused by a misunderstanding and aggressive posturing resulting in a brief yet bloody conflict between the UAP and the new, militarized xenos.
  * Avaraxians mediate the conflict bringing the war to an end and setting up the UAP to sign a non-aggression pact with the Avaraxian Alliance.
* **3014** - Avaraxians show humanity how to build jump gates to create artificial hyperlanes
  * These gates are less reliable than naturally occurring hyperlanes, but allows civilizations to fill in gaps in their hyperlane networks and to better support remote colony clusters.
* Fourth Expansion leads humanity to expand into the fourth and fifth rings
  * These outer rings begins the intermingling of xeno species with humanity
  * **3084** - first xeno planet officially joins the UAP; exiled sect of Dravar
* **3118** - The Damascus Incident
  * First 
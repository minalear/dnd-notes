---
title: "Factions"
---

## Ferelian Swarm
* Native to the Lav system, the Ferelians are a hiveminded, plantoid race that preys on other organic species for sustenance.  They have been known to venture the stars in search of prey to satiate their eternal hunger.
* Intelligent species that coordinate through a hivemind.  The great mother is located on Lavis Prime and has the ability to share her consciousness from lightyears away.
* They use specialized drones to achieve different tasks; military drones perform the fighting, artisan drones perform craftsmanship.
  * They have drones that act as psychic beacons, extending the range of the great mother's will
* Although intelligent, the Ferelians do not interact with alien species outside of consuming them.

## Galactic Alliance
* An alliance of various intergalactic, spacefaring races that have united for a common good
* Though has no direct authority over the member governments, the judgments and laws passed by the Alliance are typically respected by the participating governments (for the most part)
* Humanity is a recent addition to the alliance, though many people of Humanity's upper government are xenophobic to say the least
* The Alliance maintains a massive space station, known as the Citadel, near the galactic core and is often seen as the center of intergalactic society
* They maintain their own covert military force known as the Seraphs

## United Authority of Planets
* Humanity's reach into the stars is managed by the federated government known as the United Authority of Planets, commonly abbreviated UAP.
* The UAP was formed after the several decades of warfare that erupted between human space colonies after the discovery of the Hyperlane Network
  * Before the discovery of hyperlanes, regular FTL travel still resulting in very isolated colonies that basically ran independent of the Earth governments that sent them out
  * After several centuries, each of these colonies developed independent cultures, governments, and practices and basically ran fully autonomously due to the sheer amount of time to communicate and travel
  * After Hyperlanes allowed much quickly travel between the homeworld and colonies, the Earth governments tried to reestablish full control on these remote worlds
  * This resulted in the single most bloodiest point in human history as the various worlds erupted into wars
* The UAP was formed in the bloody aftermath of these Unification Wars, with most of the history predating this period being lost

### Organization
* UAP has several branches that exist to help maintain peace and security throughout their territory
* USC (United Space Command) are the naval forces and primary military force used by UAP to act as the first line of defense for UAP allied systems
* OGR (Office of Galactic Reconnaissance), often referred to as "Ogre", is the xeno intelligence branch of the military and have many secret dealings throughout UAP space
  * They keep tight control on fringe colonies, often using underhanded tactics to keep systems in line
  * Highly xenophobic, seeing any non-human races as a danger to the human race and actively work to keep UAP ahead of its enemies
* Lancer, a subwing of the USC that recruits and trains skilled mechanized infantry suits, referred to as Lancers.
  * These soldiers are highly specialized and act as the special operatives within the military
  * They usually act as reconnaissance units or act as the spearhead for various military operations
* HIGHCOM, often referred to as the Admirality, are the highest operational command of the USC and in charge of all personnel, equipment, and facilities used by the USC.
  * Ran by the USC Security Council, these generals and admirals are some of the most influential people within all of the UAP
---
title: "Races"
---

## Yokians
* Anthropomorphic eggs
* Commonly referred to as "yolkians" or "eggmen" as a misnomer or slur
* Die when they hatch or hatch when they die, their biology is poorly understood
* Require constant warmth
* Shell becomes harder as they age
* The older they get without hatching, the stronger the creature gestating inside becomes
* Ancient yokians are nigh indestructible and are rumored to harbor catastrophic entities
* Not commonly adventurers, but often on the run due to poaching and the potential nuisance or danger they represent when they hatch
* Became spacefaring alarmingly fast, citing inspiration from petroglyph-like inscriptions across the surface of their homeworld inspiring them to return to their winged creators
* Dishes incorporating Yokians are delicacies among the criminal elite, particularly aged
* They inhabit a moon of a massive ringed gaseous planet named Baal-Yok
* It is said that the moon itself is the largest and most ancient Yokian
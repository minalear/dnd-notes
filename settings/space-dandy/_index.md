---
title: "Space Dandy"
---

## Concept
* Galaxy spanning game with various space faring civilizations
* Concept from the Culture series where the progenitor race has ascended to a higher dimension, leaving the galaxy space for new civilizations to flourish
* An ancient race of machines, created by the first ascended race, resurrects every few millennia to judge the worthiness of the civilizations; exterminating them if they find them wanting or enlightening them if they find them worthy
* These ancient machiens can be found across the galaxy and can represent significant threats if awakened prematurely
* Advanced AI similar to Cortana are used for advanced navigation and combat, but many ships still use organic pilots; either due to arrogance or genuine skill of the pilot
  * AI should be compatible with Lancer to allow games in that system to exist
* Game can pull from every space opera and incorporate elements to create a diverse setting with many stories to tell
  * Star Wars, Star Trek, Firefly, Mass Effect, Riddick Alien, Warhammer, etc.
* Travel between systems are achieved via Hyperlanes, a series of extra dimensional pathways that connect most known star systems
  * No one knows who constructed these Hyperlanes, but they allow for galactic spanning empires to function at large scales
  * To enter the Hyperlane, a ship needs to be equipped with a Warp Core; a fully self contained pod that contains a creature known as a "scuttlebug"
  * These bugs are native inhabitants of the network and can open rifts allowing to the Hyperlane

## Warpspace and Hyperlanes
* Hyperlanes are one of the few reliable ways of interstellar travel over vast distances
* A hyperlane is a semi-stable region within Warpspace that allows ships to travel between two points relatively fast; much faster than traditional FTL methods
* Warpspace is incredibly dangerous though, teeming with monstrous entities referred to as "demons"
* The hyperlanes are fairly safe for most traffic, though the occassional incident occurs
* Advanced AIs are required to plot the course through the complicated Warpspace, as no human pilot could possibly comprehend the journey to successfully navigate Warpspace

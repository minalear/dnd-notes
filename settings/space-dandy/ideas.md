---
title: "Ideas"
---

## Setting Ideas

* *Minerva's Den* is a location in the Minverva system that is home to a number of gangs and other criminal elements.
* AI have been banned by the Galactic Council from using most weapon systems, known as the Gix Accords, due to a war which powerful AI caused tremendous amounts of damage
* Clones have unique bio-QR codes as thumb prints to uniquely identify them
* General Systems Assembly Ship, GSAs, turn asteroids into space stations and orbitals
* Nilgobs are prominent through Orion Arm setting up various small stations known as "gas stations"
  * They typically house a few apartments, hotels, restaurants, and other rest stops and house various workers or migrants
* Jovians are the group of humans native to the moon system of Jupiter
  * Known for being a religious cult that worships an entity from Warpspace
* Intelligence, especially in regards towards machine intelligence, is measured on a scale relative to the intellectual capacity of a standard Homo sapien.
  * The base score is 1.0
  * Most "human-like" machine intelligences have a score ranging from 0.8 to 1.2
  * Advanced minds, such as super intelligence machines, can have scores in the 10s
  * Mentats, a pan-human species, is notable for having a score of 4.0, the highest recorded for an organic species
* Van der Meer family's ancestral home was destroyed during the Interplanetary Wars
  * Was named Decimas
  * After the war and during the Third Expansion, the family created the Golden Saucer in the Alpha Canis system

## Mission Ideas

* Software that can be used to decrypt anything
* Space cult with leader looking to ascend into the Shroud and take his people with him

## Other Shit

* Mass sensors
* "Space Dwarves" use "Star Forges" found within dense nebulas to create some of the galaxy's biggest constructs

## Names for Corporations
* Orion Corporation
* Novak Enterprises
* AlphaTech Solutions
* Delta Group

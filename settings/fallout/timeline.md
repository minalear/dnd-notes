---
title: "Timeline"
---

## The Messenger
* A messenger from The Enigmatic arrives on Earth as a diplomat from the greater galactic community.
  * The messenger arrives 
* US Government captures the messenger and take them to Area 51 for study, giving the contract to new biotechnical firm Gilead.
* The Enigmatic are a pacifist race and do not retaliate for their messenger being captured.
* Gilead begins experimentation upon The Messenger, using their DNA to create advanced cybernetics, nanotechnology, and various medications that rapidly advance many different scientific fields.
  * With this technology, Gilead becomes a dominant power and uses their vast capital to purchase various firms, cementing them as a multinational conglomerate.
* Part of their experimentation involved cloning The Messenger, a subject dubbed Archon, that became raving mad; bent on the destruction of everything around them.
  * Archon was imprisoned in a lead coffin and stored beneath Area 51 in hopes of containing the powerful alien being.

## Cyber Age
* Cybernetics become commonplace, being mostly manufactured by Gilead and their subsidiaries
* EYE is founded officially as a private military company, but acts as a secret police force for the Illuminati.
  * Agents are referred to as Templars and are biologically enhanced with DNA from the Messenger in addition to advanced cybernetics, armors, and weaponry.

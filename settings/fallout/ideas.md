---
title: "Ideas"
---

## Concept
* Post-Apocalptic world with most of the world sent into chaos, much like Wasteland or Fallout universe
* Story begins with aliens visiting Earth and the US government capturing the messenger and forcibly studying it
* The research group tasked with studying the alien was controlled by the Illuminati
* DNA from the messenger was used to create advanced warriors along with cybernetic enhancements
* During this time, a nuclear war broke out which caused the systematic fall of various nations
* Many years later, various nations were founded and banded together to found the Federated States of Surviving Nations, or simply The Federation
* The Federation is one of the major factions of the world, but many groups resist their attempts to establish a new world order
* The Illuminati puppet some factions of the Federation, but other factions, such as the Secretorum, oppose them via clandestine operations

## Ideas
* Gilead's primary research moved from Area 51 to Nanyang Technological University in Singapore, bringing along The Messenger and Archon for further study
  * Archon broke free not long after and summoned the Gray Haze which resulted in the Collapse
* Federated Union of Survivor States, or simply the Federation, is a new world power that controls most of the geopolitics
  * With long distance communication no longer reliable due to the Collapse, the world is mostly in chaos, with small communities banding together for survival
  * The Federation is one of the few major powers that has any semblance of control, but even within their borders is rampant crime and anarchism
* Various cities such as New York and Chicago live under "light domes" that try to mimic natural daylight.
  * These cities are surrounded by dark slums of poor people who work in these cities
  * VR is a common escape for these downtrodden individuals who cannot afford to live within the light
* Outer Heaven - the largest city left on Earth and the heart of the Federation
  * Protected by Scions, large AI warmachines that protect the last bastion of humanity
* The Collapse was a world wide apocalytpic event resulting in the sun being permanently blocked out and rampant mutations causing a collapse of modern society
  * Several decades pass before any semblance of society forms again with every major power being brought down
* The Marlowe Corporation experimenting with advanced communication devices to manage their space colonies.
  * They are now the sole proprietor of devices of long distance communication/transmissions on Earth
  * The devices are known as Ansibles and are capable of instantaneous transmissions, regardless of distance
  * They use these to communicate with their colonies.  They are incredibly expensive and very rare.
  * They are actively working on technology to dispatch the Gray Haze or at least allow traversal with air/space craft.
* 40 years pass between the Collapse and the founding of Outer Heaven/The Federation with another 200 years passing before the dome cities are constructed.
  * Outer Heaven is the world's largest city and was considered the last bastion of humanity for over a century.
  * It is an incredibly large dome built near Prague, Czechia
  * The top level is a recreation of the old way of life, complete with parks, plazas, apartments, and artificial sunlight.
  * The lower layers are reserved for the lower classes and are rife with corruption and crime.
* Gray Haze has dropped the world's temperatures by a significant amount, resulting in a collapse of most ecosystems.
  * The only organisms remaining are the ones that have been mutated by the Haze and now thrive in the perpetual darkness
EYE's headquarters are located in Tselinoyarsk, a small country located near former Pakistan and the Soviet Union
  * The main Temple are where the Templars train and is one of the most heavily defended fortresses in the world
  * They use advanced military equipment, including power armor, flying troop transports, and heavy laser weaponry
  * They are also genetically modified with The Messenger's DNA granting them increased strength, reaction, speed, and various psionic abilities.
* There are two main factions within EYE and they are at odds with one another; Jian Shang Di and the Culter Dei